# This class reads a file with a list of lists that contain integers;
# each number means a type of block. 1 is for solid (indestructible)
# blocks. 2 to 5 are normal blocks in different colors.
# Levels can be any number of rows high and any number of columns wide.

(class +Level)

(dm T (File)                                                                     # Pass a file path and the dimensions of the area to be filled with the blocks
   (let (D (in File (read))
         Unith (/ *GameH 2 (length D))                                           # Calculate the dimensions of a single block
         Unitw (/ *GameW (length (car D))) )
      (=: n 0)
      (=: blocks (make                                                           # Each block is an executable expression
         (for (I . R) D
            (for (J . E) R
               (let (X (* Unitw (dec J)) Y (* Unith (dec I))                     # Calculate the position of the block
                     S '(draw> *Sprite *Solid)                                   # Expression template for solid blocks. *Sprite is a +GLSprite. *Solid is a +GLTexture
                     B '(draw> *Sprite *Block) )                                 # Expression template for normal blocks. *Block is a +GLTexture
                  (case E                                                        # Complete the templates with the position, size, color, and optional T (= solid block)
                     (1 (link (append S (list X Y Unitw Unith 0.8 0.8 0.7 1.0 T))))
                     (2 (link (append B (list X Y Unitw Unith 0.2 0.6 1.0 1.0)))
                        (inc (:: n)) )
                     (3 (link (append B (list X Y Unitw Unith 0 0.7 0 1.0)))
                        (inc (:: n)) )
                     (4 (link (append B (list X Y Unitw Unith 0.8 0.8 0.4 1.0)))
                        (inc (:: n)) )
                     (5 (link (append B (list X Y Unitw Unith 1.0 0.5 0 1.0)))
                        (inc (:: n)) ) ) ) ) ) ) ) ) )


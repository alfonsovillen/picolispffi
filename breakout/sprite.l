# This class draws a rectangle using a +GLTexture
# (= a 2D sprite)

(class +GLSprite)

(dm T (Shader MatName)                                                           # Pass a +Shader and a name
   (let (VAO (glGenVertexArrays 1)
         VBO (glGenBuffers 1) )
      (=: shader Shader)
      (=: mat MatName)                                                           # We'll allocate a matrix
      (=: vao VAO)
      (=: vbo VBO)
      (=: rot 0)                                                                 # Rotation angle
      (glBindVertexArray VAO)
      (glBindBuffer GL_ARRAY_BUFFER VBO)
      (glBufferData GL_ARRAY_BUFFER                                              # Vertex and texture data
         (floats* 1.0 (0 1.0 0 1.0 1.0 0 1.0 0 0 0 0 0
            0 1.0 0 1.0 1.0 1.0 1.0 1.0 1.0 0 1.0 0) ) GL_STATIC_DRAW )
      (glVertexAttribPointer 0 4 GL_FLOAT 0 16 0)
      (glEnableVertexAttribArray 0)
      (glBindBuffer GL_ARRAY_BUFFER 0)
      (glBindVertexArray 0)
      (newmat> Shader 4 MatName)                                                 # Allocate the matrix in the shader object
      (uniform> Shader "spriteColor")                                            # Get two uniforms by name
      (uniform> Shader "model") ) )

# Prepare the shader for use with its vertex array and a texture unit

(dm activate> (TexUnit)
   (use> (: shader))
   (glActiveTexture TexUnit)
   (glBindVertexArray (: vao)) )

# Unbind the shader's vertex array

(dm deactivate> ()
   (glBindVertexArray 0) )

# Draw the sprite using texture Tex, coordinates X Y W H and color R G B A

(dm draw> (Tex X Y W H R G B A)                                                  # Pass a +GLTexture, some coordinates and a color
   (and Tex (bindtex> Tex))
   (setmat> (: shader) 4 "model" (: mat)                                         # Set the matrix in the shader object
      (multm
         (scalem W H 1.0)
         (transm (*/ -0.5 W 1.0) (*/ -0.5 H 1.0) 0)
         (rotzm (: rot))
         (transm (*/ 0.5 W 1.0) (*/ 0.5 H 1.0) 0)
         (transm X Y 0) ) )
   (setvec> (: shader) "spriteColor" R G B A)                                    # Set the uniform with an RGBA color value
   (glDrawArrays `GL_TRIANGLES 0 6) )


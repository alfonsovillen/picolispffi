# The shader program needs the uniforms spriteColor, model, projection,
# charOffset and image

(class +GLText)

(dm T (TexFile Rows Cols FirstAscii)
   (=: cols Cols)
   (=: rows Rows)
   (=: scalex (/ 1.0 Cols))
   (=: scaley (/ 1.0 Rows))
   (=: first FirstAscii)
   (=: shader (new '(+Shader)
      "#version 330 core
       layout (location = 0) in vec4 vertex;
       out vec2 TexCoords;
       uniform mat4 model;
       uniform mat4 projection;
       uniform vec2 charOffset;
       void main()
       {
         TexCoords = vertex.zw + charOffset;
         gl_Position = projection * model * vec4(vertex.xy, 0.0, 1.0);
       }"
       
      "#version 330 core
       in vec2 TexCoords;
       out vec4 color;
       uniform sampler2D image;
       uniform vec3 spriteColor;
       void main()
       {
         color = vec4(spriteColor, 1.0) * texture(image, TexCoords);
       }" ) )
   (=: tex (new '(+GLTexture) TexFile GL_RGBA))
   (let (VAO (glGenVertexArrays 1)
         VBO (glGenBuffers 1) )
      (=: vao VAO)
      (=: vbo VBO)
      (=: rot 0)                                                                 # Rotation angle
      (glBindVertexArray VAO)
      (glBindBuffer GL_ARRAY_BUFFER VBO)
      (glBufferData GL_ARRAY_BUFFER                                              # Vertex and texture data
         (floats* 1.0
            (list 0   1.0 0 (: scaley)
                  1.0 0   (: scalex) 0
                  0   0   0 0                                                    # We'll modify the texture coordinates to point
                  0 1.0   0 (: scaley)
                  1.0 1.0 (: scalex) (: scaley)
                  1.0 0   (: scalex) 0 ) )
         GL_STATIC_DRAW )          # to the single characters when rendering them
      (glVertexAttribPointer 0 4 GL_FLOAT 0 16 0)
      (glEnableVertexAttribArray 0)
      (glBindBuffer GL_ARRAY_BUFFER 0)
      (glBindVertexArray 0) )
   (use> (: shader))
   (uniform> (: shader) "model")
   (uniform> (: shader) "projection")
   (uniform> (: shader) "charOffset")
   (uniform> (: shader) "image")
   (uniform> (: shader) "spriteColor")
   (newmat> (: shader) 4 'model)
   (newmat> (: shader) 4 'proj)
   (setmat> (: shader) 4 "projection" 'proj                                      # Set the "projection" uniform with an
      (orthom 0 0 *GameW *GameH -1.0 1.0) )                                      # orthographic projection matrix
   (setint> (: shader) "image" 0) )

(dm draw> (Str X Y CharW CharH R G B)
   (use> (: shader))
   (glActiveTexture 0)
   (glBindVertexArray (: vao))
   (bindtex> (: tex))
   (setvec> (: shader) "spriteColor" R G B)  
   (for C (mapcar '((U) (- (char U) (: first))) (chop Str))
      (setmat> (: shader) 4 "model" 'model                                          # Set the matrix in the shader object
         (multm
            (scalem CharW CharH 1.0)
            (transm (*/ -0.5 CharW 1.0) (*/ -0.5 CharH 1.0) 0)
            (rotzm (: rot))
            (transm (*/ 0.5 CharW 1.0) (*/ 0.5 CharH 1.0) 0)
            (transm X Y 0) ) )
      (setvec> (: shader) "charOffset"
         (* (% C (: cols)) (: scalex))
         (* (/ C (: cols)) (: scaley)) )
      (glDrawArrays `GL_TRIANGLES 0 6)
      (inc 'X CharW) )
   (glBindVertexArray 0) )

(dm drawCentered> (Str TotW TotH CharW CharH R G B)
   (let (X (- (/ TotW 2) (* CharW (/ (length Str) 2)))
         Y (- (/ TotH 2) (/ CharH 2)) )
      (draw> This Str X Y CharW CharH R G B) ) )
      
(dm destroy> ()
   (destroy> (: shader))
   (destroy> (: tex)) )


# This file needs sdl.l, sdlutil.l, sdlimage.l, sdlmixer.l, sdlopengl.l,
# opengl3.l, shader.l and transform.l from the sdl/ folder

# Some all-uppercase symbols whose val is a number are backticked, but the
# backticks are optional and can be removed.

(scl 6)                                                                          # This means that 1.0 = 1000000

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/sdlmixer.l"
 "@sdl/sdlopengl.l" "@sdl/opengl3.l" "@sdl/shader.l" "@sdl/transform.l" )

# All global symbols (starting with *) are defined in this file and used in
# the files loaded below.

(load "sprite.l" "texture.l" "effect.l" "player.l" "ball.l" "powerup.l"
   "particle.l" "level.l" "text.l" )

(de err? (F) (prinl F ": " (hex (glGetError))))                                  # We can remove this when ready for release

# Width and height of the game window

(setq *GameW 800.0 *GameH 600.0)

# Clamp a value

(de clamp (N Min Max) (if (< N Min) Min (if (> N Max) Max N)))

# Gives A the same sign as B

(de samesig (A B)
   (if (=0 B) A
       (* (abs A) (if (lt0 B) -1 1)) ) )

# Algorithm for collision detection: rectangular object-rectangle.
# Returns a direction (bouncing) vector if both rectangles collide,
# basing on the colliding sides.
# -1 means left/up, 1 means right/down, 0 means the direction doesn't change.

(de hitbb (Ax Ay Aw Ah Bx By Bw Bh)
   (and (>= (+ Ax Aw) Bx)
      (>= (+ Bx Bw) Ax)
      (>= (+ Ay Ah) By)
      (>= (+ By Bh) Ay)
      (cddr (mini '((L) (- (car L) (cadr L)))
         (list
            (list (+ Ax Aw) Bx -1 0)
            (list (+ Bx Bw) Ax 1 0)
            (list (+ Ay Ah) By 0 -1)
            (list (+ By Bh) Ay 0 1) ) ) ) ) )
                  
# Initialize the state of the game

(de start ()
   (sdlInitApp SDL_INIT_VIDEO)
   (imgInit (| IMG_INIT_JPG IMG_INIT_PNG))
   (mixInit MIX_INIT_MP3)
   (mixOpenAudio)
   
   (setq *Win (new '(+OpenGLWindow) "Picolisp Breakout"
            (/ *GameW 1.0) (/ *GameH 1.0) 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DOUBLEBUFFER . 1) ) )
              
         *SprShd (new '(+Shader)
            "#version 330 core
             layout (location = 0) in vec4 vertex;
             out vec2 TexCoords;
             uniform mat4 model;
             uniform mat4 projection;
             void main()
             {
               TexCoords = vertex.zw;
               gl_Position = projection * model * vec4(vertex.xy, 0.0, 1.0);
             }"
             
            "#version 330 core
             in vec2 TexCoords;
             out vec4 color;
             uniform sampler2D image;
             uniform vec4 spriteColor;
             void main()
             {
               color = spriteColor * texture(image, TexCoords);
             }" )
         
         *Effect (new '(+Effect))
                
         *Block (new '(+GLTexture) "gfx/block.png")
         *Solid (new '(+GLTexture) "gfx/block_solid.png")
         *Paddle (new '(+GLTexture) "gfx/paddle.png" GL_RGBA)
         *Face (new '(+GLTexture) "gfx/awesomeface.png" GL_RGBA)
         *Bkgnd (new '(+GLTexture) "gfx/background.jpg")
         *PartSpr (new '(+GLTexture) "gfx/particle.png" GL_RGBA)
         *PupSpeed (new '(+GLTexture) "gfx/powerup_speed.png" GL_RGBA)
         *PupSticky (new '(+GLTexture) "gfx/powerup_sticky.png" GL_RGBA)
         *PupPassThrough (new '(+GLTexture)
            "gfx/powerup_passthrough.png" GL_RGBA )
         *PupIncrease (new '(+GLTexture) "gfx/powerup_increase.png" GL_RGBA)
         *PupConfuse (new '(+GLTexture) "gfx/powerup_confuse.png" GL_RGBA)
         *PupChaos (new '(+GLTexture) "gfx/powerup_chaos.png" GL_RGBA)
         
         *Sprite (new '(+GLSprite) *SprShd 'sprmat)

         *LNum 1
         
         *Music (new '(+Music) "music/breakout.mp3")
         
         *CNormal (new '(+Chunk) "music/normal.wav")
         *CBleep (new '(+Chunk) "music/bleep.wav")
         *CPup (new '(+Chunk) "music/powerup.wav")
         *CSolid (new '(+Chunk) "music/solid.wav")
         
         *Text (new '(+GLText) "gfx/letters512x256.png" 6 16 32)
         
         *Lives 3
         
         *FTime (0 . 0) )

   (with *SprShd
      (use> This)                                                                # Use the shader program
      (uniform> This "image")                                                    # Get two uniforms by name
      (uniform> This "projection")
      (newmat> This 4 'proj)                                                     # Allocate a new matrix
      (setmat> This 4 "projection" 'proj                                         # Set the "projection" uniform with an
         (orthom 0 0 *GameW *GameH -1.0 1.0) )                                   # orthographic projection matrix
      (setint> This "image" 0) )                                                 # Set the "image" uniform with the texture unit to be used
   
   (glClearColor 1.0 1.0 1.0 1.0)
   (glEnable GL_BLEND)
   (glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA) )

# This loads a level file and presents the new level, then
# changes the game mode to "play"

(de newLvl ()
   (ifn (info (pack "levels/" *LNum ".lvl"))
      (setq *State won)
      (activate> *Sprite 0)
      (draw> *Sprite *Bkgnd 0 0 *GameW *GameH 1.0 1.0 1.0 1.0)
      (drawCentered> *Text (pack "LEVEL " *LNum) *GameW *GameH
         50.0 90.0 0 1.0 1.0)
      (setq *Player (new '(+Player) 100.0 20.0 600.0)
            *Ball (new '(+Ball) 12.5 250.0 -600.0 *Player) )
      (with *Player
         (=: stuck *Ball)
         (=: stuck p This)
         (pos> This)
         (pos> (: stuck)) )
      (setq *Sparkles (mkPart 10 *Ball)
            *Pups NIL
            *Level (new '(+Level) (pack "levels/" *LNum ".lvl")
               *GameW (/ *GameH 2) )
            *State play )
      (with *Effect
         (=: confuse 0)
         (=: chaos 0)
         (=: shake 0)
         (=: dthru 0)
         (=: dconfuse 0)
         (=: dchaos 0)
         (=: dshake 0)
         (=: dthru 0) )
      (swap> *Win)
      (wait 3000)
      (play> *Music -1) ) )

# This shows the "You won" message because the player finished
# all levels

(de won ()
   (activate> *Sprite 0)
   (draw> *Sprite *Bkgnd 0 0 *GameW *GameH 1.0 1.0 1.0 1.0)
   (drawCentered> *Text "YOU WON!" *GameW *GameH
      50.0 90.0 1.0 1.0 1.0)
   (swap> *Win)
   (wait 3000)
   (on *SDLStop) )

# This shows a "Level Completed" message for 3 secs and
# lets the next level load

(de lvlDone ()
   (activate> *Sprite 0)
   (draw> *Sprite *Bkgnd 0 0 *GameW *GameH 1.0 1.0 1.0 1.0)
   (drawCentered> *Text "Level Completed!" *GameW *GameH
      50.0 90.0 1.0 1.0 0 )
   (swap> *Win)
   (mixFadeOutMusic 3000)
   (wait 5000)
   (mixRewindMusic)
   (inc '*LNum)
   (setq *State newLvl) )

# This renders a frame while playing the game.
# Change (on *SDLStop) to (setq *State menu) when menu loop ready.

(de play ()
   (let Delta (/ (- (car *FTime) (cdr *FTime)) 2)
      (con *FTime (car *FTime))
      (set *FTime (usec))
      (with *Effect
         (updfx> This Delta *Player *Ball)
         (begin> This) )
      (activate> *Sprite 0)
      (draw> *Sprite *Bkgnd 0 0 *GameW *GameH 1.0 1.0 1.0 1.0)
      (movepup Delta *Player *Ball)
      (drawpup)
      (run (; *Level blocks))                                                    # Run the block drawing expressions
      (move> *Player Delta)                                                      # Draw the player's paddle
      (glBlendFunc `GL_SRC_ALPHA `GL_ONE)
      (particles Delta)                                                          # Draw the sparkles
      (glBlendFunc `GL_SRC_ALPHA `GL_ONE_MINUS_SRC_ALPHA)
      (move> *Ball Delta)                                                        # Move and render the ball
      (and (le0 (; *Level n))                                                    # If all normal blocks were destroyed
           (setq *State lvlDone) )                                               # Change the game mode
      (end> *Effect)
      (render> *Effect Delta) )
      (draw> *Text (pack "Lives " *Lives) 0 0 24.0 48.0 1.0 1.0 1.0)             # Render the info text
      (swap> *Win)
      (and (keyDown? *Keys 41) (on *SDLStop)) )                                  # ESC key exits play mode

# This takes a life and checks if all lives are gone

(de die ()
   (dec '*Lives)
   (if (=0 *Lives)
      (on *SDLStop)
      (with *Ball
         (=: p *Player)
         (=: p stuck This)
         (pos> This) ) ) )

# This is run before finishing the program

(de cleanup ()
   (mixHaltMusic)
   (mapc '((K) (destroy> K))  (list *SprShd *Solid *Block *Paddle *Win
      *Face *Bkgnd *PartSpr *PupSpeed *PupSticky *PupPassThrough *PupIncrease
      *PupConfuse *PupChaos *Effect *Music *CNormal *CBleep *CPup *CSolid
      *Text ) )
   (mixCloseAudio)
   (mixQuit)
   (imgQuit)
   (sdlQuitApp) )

# This is run at every frame. It processes the SDL event queue
# (currently discarding all events) and renders a frame using the
# rendering function stored in *State.

(de sdlEventLoop ()
   (until *SDLStop                                                               # Main application loop
      (until (=0 (sdlPollEvent)))                                                # Take events from the event queue, until it is empty (i.e. sdlPollEvent returns 0)
         (run *State) ) )                                                        # Animate a frame


# This is the main function

(de main ()
   (start)
   (setq *State newLvl
         *SDLActions NIL
         *Keys (sdlGetKeyboardState) )
   (sdlEventLoop)
   (mixFadeOutMusic 3000)
   (activate> *Sprite 0)
   (draw> *Sprite *Bkgnd 0 0 *GameW *GameH 1.0 1.0 1.0 1.0)
   (drawCentered> *Text "GAME OVER!" *GameW *GameH 50.0 90.0 1.0 0 0)
   (swap> *Win)
   (wait 5000)
   (cleanup) )


# This implements a powerup pill. It uses *Sprite to render

(class +Powerup)

(dm T (Typ Dur X Y R G B Tex)
   (=: typ Typ)
   (=: dur Dur)
   (=: x X)
   (=: y Y)
   (=: r R)
   (=: g G)
   (=: b B)
   (=: tex Tex)
   (=: w 60.0)
   (=: h 20.0)
   (=: vx 0.0)
   (=: vy 150.0) )

# Activate the effect of a powerup pill

(dm activate> (Player Ball)
   (play> *CPup)                                                                 # Play a sound
   (let D (: dur)
      (case (: typ)
         ("speed" (with Ball (=: vx (*/ (: vx) 1.2 1.0))
                             (=: vy (*/ (: vy) 1.2 1.0)) ) )
         ("sticky" (put Ball 'p Player)
                   (with Player (=: r 1.0) (=: g 0.5) (=: b 1.0) ) )
         ("thru" (with *Effect
                     (=: thru 1)
                     (inc (:: dthru) D) )
                 (with Ball (=: r 1.0) (=: g 0.5) (=: b 0.5)) )
         ("bigger" (with Player (inc (:: w) 50.0) ) )
         ("confuse" (with *Effect
                        (unless (=1 (: chaos))
                           (=: confuse 1)
                           (inc (:: dconfuse) D) ) ) )
         ("chaos" (with *Effect
                     (unless (=1 (: confuse))
                        (=: chaos 1)
                        (inc (:: dchaos) D) ) ) ) ) ) )
      
# Create a new powerup pill based on a probability

(de spawnpup (X Y)
   (let Prob (rand 0 100)
      (cond
         ((> 4 Prob) (case (rand 1 4)
            (1 (push '*Pups (new '(+Powerup) "speed" 0 X Y 0.5 0.5 1.0 *PupSpeed)))
            (2 (push '*Pups (new '(+Powerup) "sticky" 20.0 X Y 1.0 0.5 1.0 *PupSticky)))
            (3 (push '*Pups (new '(+Powerup) "thru" 10.0 X Y 0.5 1.0 0.5 *PupPassThrough)))
            (4 (push '*Pups (new '(+Powerup) "bigger" 0 X Y 1.0 0.6 0.4 *PupIncrease))) ) )
         ((> 14 Prob) (case (rand 1 2)
            (1 (push '*Pups (new '(+Powerup) "confuse" 15.0 X Y 1.0 0.3 0.3 *PupConfuse)))
            (2 (push '*Pups (new '(+Powerup) "chaos" 15.0 X Y 0.9 0.25 0.25 *PupChaos))) ) ) ) ) )

# Check collisions of some powerup pills *Pups with a paddle and a ball and move them

(de movepup (Delta Player Ball)
   (let (Px (; Player x) Py (; Player y) Pw (; Player w) Ph (; Player h))
      (setq *Pups (make
         (for This *Pups
            (cond
               ((< *GameH (: y)))                                                # skip pill if out of screen
               ((hitbb Px Py Pw Ph (: x) (: y) (: w) (: h))                      # if pill collides with paddle
                  (activate> This Player Ball) )                                 # activate the pill but skip (=kill) it
               (T (inc (:: y) (*/ Delta (: vy) 1.0))                             # move the pill and keep it
                  (link This) ) ) ) ) ) ) )

# Draw the powerup pills

(de drawpup ()
   (for This *Pups
      (draw> *Sprite (: tex) (: x) (: y) (: w) (: h) (: r) (: g) (: b) 1.0) ) )


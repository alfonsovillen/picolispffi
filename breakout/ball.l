# This represents a game ball

(class +Ball)

(dm T (R Vx Vy P)                                                                # Take the initial radius and velocity of the ball and a +Player
   (=: r R)                                                                      # r = radius
   (=: s (* R 2))                                                                # s = ball size (diameter)
   (=: rsq (*/ R R 1.0))                                                         # R = radius ^ 2
   (=: vx Vx)                                                                    # vx = current x velocity
   (=: ivx Vx)                                                                   # Initial ball velocity
   (=: vy Vy)                                                                    # vy = current y velocity
   (=: p P)                                                                      # p = +Player (paddle) this ball is stuck to
   (=: p stuck This) )                                                           # Set the paddle status to stuck to the ball

(dm pos> ()                                                                      # Set the position of the ball centered on top of the +Player P
   (=: x (+ (: p x) (- (/ (: p w) 2) (: r))))
   (=: y (- (: p y) (: s))) )

# Check for collisions with the blocks of the level until a collision
# is found, delete the block if it isn't solid or pass-through effect activated

(dm coll> ()
   (let (Hit NIL Bx (: x) By (: y) Bs (: s) Dir NIL)
      (for B (; *Level blocks)                                                   # Check for collisions
         (and  (setq Dir (hitbb Bx By Bs Bs (; B 4) (; B 5)
                  (; B 6) (; B 7) ) )
               (setq Hit B) )
         (T Hit                                                                  # Exit immediately if there was a collision
            (and (nT (last Hit))
                 (put *Level 'n (dec (; *Level n))) )                            # Decrement if the ball hit a normal block
            (if (=1 (; *Effect thru))                                            # If pass-through powerpill activated
               (prog
                  (put *Level 'blocks (delq Hit (; *Level blocks)))              # Delete the block
                  (spawnpup (; Hit 4) (; Hit 5)) )                               # Randomly throw a powerup
               (if (=T (last Hit))                                               # If block is marked as solid
                  (prog
                     (put *Effect 'shake 1)                                      # Set the shaking effect
                     (put *Effect 'dshake 0.05)
                     (play> *CSolid) )                                           # Play a sound
                  (put *Level 'blocks (delq Hit (; *Level blocks)))              # Delete the block
                  (spawnpup (; Hit 4) (; Hit 5))                                 # Randomly throw a powerup
                  (play> *CNormal) )                                             # Play a sound
               (=: vx (samesig (: vx) (car Dir)))                                # Update the balls velocity, i.e. make it bounce
               (=: vy (samesig (: vy) (cadr Dir))) ) ) ) ) )

# Check for collision of the ball with +Player (paddle) P and
# update the velocities of the ball

(dm paddle> (P)
   (when (hitbb (: x) (: y) (: s) (: s) (; P x) (; P y) (; P w) (; P h))
      (play> *CBleep)                                                            # Play a sound
      (let (Cb (+ (; P x) (/ (; P w) 2))                                         # Center of the paddle
            Di (- (+ (: x) (: r)) Cb)                                            # Distance
            Pc (*/ 1.0 Di (/ (; P w) 2))                                         # Percentage
            St 2.0                                                               # Strength
            Nx (*/ (*/ (: ivx) Pc 1.0) St 1.0)                                   # New x velocity
            Ln (lenv (list (: vx) (: vy)))                                       # Length of old velocity vector
            No (normv (list Nx (: vy))) )                                        # Normalize new x velocity and y old velocity
         (if (and (: p) (not (: p stuck)))                                       # This is T when sticky powerup pill activated
            (=: p stuck This)                                                    # Stick the ball to the paddle
            (=: vx (*/ (car No) Ln 1.0))                                         # Set new x velocity
            (=: vy (* -1 (abs (*/ (cadr No) Ln 1.0)))) ) ) ) )                   # Set new y velocity
   
(dm move> (Delta)
   (if (: p stuck)
      (pos> This)
      (inc (:: x) (*/ (: vx) Delta 1.0))
      (inc (:: y) (*/ (: vy) Delta 1.0))
      (=: cx (+ (: x) (: r)))                                                    # Store the x and y position of the ball's center
      (=: cy (+ (: y) (: r)))
      (cond
         ((le0 (: x)) (=: vx (- (: vx))) (=: x 0))                               # Check if ball touches the left border
         ((>= (+ (: x) (: s)) *GameW)                                            # Check if ball touches the right border
            (=: vx (- (: vx))) (=: x (- *GameW (: s))))
         ((le0 (: y)) (=: vy (- (: vy))) (=: y 0))                               # Check if ball touches the top border
         ((>= (: y) *GameH) (die))                                               # Check if ball flies off the bottom of the screen
         ((>= (+ (: y) (: s)) (; *Player y)) (paddle> This *Player))             # Check if ball collides with a paddle
         ((<= (: y) (/ *GameH 2)) (coll> This)) ) )                              # Check if ball touches a block
   (draw> *Sprite *Face (: x) (: y) (: s) (: s) 1.0 1.0 1.0 1.0) )               # Draw the ball. *Sprite is a +GLSprite, *Face is a +GLTexture


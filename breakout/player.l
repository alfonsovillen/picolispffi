# This represents a paddle

(class +Player)

(dm T (W H V) (=: w W) (=: h H) (=: v V))                                        # Take the initial width, height and velocity of the paddle

(dm pos> (X Y)                                                                   # Set the initial position of the paddle or center it at the bottom of the window
   (if X (=: x X) (=: x (- (/ *GameW 2) (/ (: w) 2))))
   (if Y (=: y Y) (=: y (- *GameH (: h)))) )

(dm move> (Delta)
   (cond
      ((keyDown? *Keys 4)                                                        # 4 = scan code for key "A": move the paddle to the left
         (and (ge0 (: x))
              (dec (:: x) (*/ (: v) Delta 1.0)) ) )
      ((keyDown? *Keys 7)                                                        # 7 = scan code for key "D": move the paddle to the right
         (and (<= (: x) (- *GameW (: w)))
              (inc (:: x) (*/ (: v) Delta 1.0)) ) )
      ((keyDown? *Keys 44)                                                       # 44 = scan code for SPACE key: release the ball
         (with (: stuck)
            (=: r 1.0) (=: g 1.0) (=: b 1.0) )                                   # Restore the color of the ball for the case the sticky effect was activated before
         (=: stuck p NIL)
         (=: stuck NIL) ) )
   (draw> *Sprite *Paddle (: x) (: y) (: w) (: h) 1.0 1.0 1.0 1.0) )             # Draw the paddle. *Sprite is a +GLSprite. *Paddle is a +GLTexture


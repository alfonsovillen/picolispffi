# Make a new particle

(class +Particle)

(dm setup> (Ball)
   (let (Pos (rand 0 10.0)
         Ofs (/ (; Ball r) 2) )
      (=: x (+ (; Ball x) Pos Ofs))
      (=: y (+ (; Ball y) Pos Ofs))
      (=: col 1.0)
      (=: l (rand 0 1.0))
      (=: vx (*/ (; Ball vx) 0.1 1.0))
      (=: vy (*/ (; Ball vy) 0.1 1.0)) ) )

# Make some particles

(de mkPart (N Ball)
   (let P (make (do N (link (new '(+Particle)))))
      (mapc '((X) (setup> X Ball)) P)
      P ) )
      
# Update particles, making dead particles alive again

(de updPart (Delta Parts Ball)
   (for This Parts
      (dec (:: x) (*/ (: vx) Delta 1.0))
      (dec (:: y) (*/ (: vy) Delta 1.0))
      (dec (:: col) (*/ Delta 2.5 1.0))
      (dec (:: l) Delta)
      (or (gt0 (: l)) (setup> This Ball) ) ) )

# Draw sparkles

(de particles (Delta)
   (bindtex> *PartSpr)
   (updPart Delta *Sparkles *Ball)
   (for This *Sparkles
      (let C (: col)
      (draw> *Sprite NIL (: x)
         (: y) 10.0 10.0 C C C 1.0) ) ) )


# Image post processing

(class +Effect +Shader)

(dm T ()
   (super
     "#version 330 core
      layout (location = 0) in vec4 vertex; // <vec2 position, vec2 texCoords>

      out vec2 TexCoords;

      uniform bool  chaos;
      uniform bool  confuse;
      uniform bool  shake;
      uniform float time;

      void main()
      {
          gl_Position = vec4(vertex.xy, 0.0f, 1.0f); 
          vec2 texture = vertex.zw;
          if(chaos)
          {
              float strength = 0.3;
              vec2 pos = vec2(texture.x + sin(time) * strength, texture.y + cos(time) * strength);        
              TexCoords = pos;
          }
          else if(confuse)
          {
              TexCoords = vec2(1.0 - texture.x, 1.0 - texture.y);
          }
          else
          {
              TexCoords = texture;
          }
          if (shake)
          {
              float strength = 0.01;
              gl_Position.x += cos(time * 10) * strength;        
              gl_Position.y += cos(time * 15) * strength;        
          }
      }"
      
     "#version 330 core
      in  vec2  TexCoords;
      out vec4  color;
        
      uniform sampler2D scene;
      uniform vec2      offsets[9];
      uniform float     edge_kernel[9];
      uniform float     blur_kernel[9];

      uniform bool chaos;
      uniform bool confuse;
      uniform bool shake;

      void main()
      {
          color = vec4(0.0f);
          vec3 sample[9];
          // sample from texture offsets if using convolution matrix
          if(chaos || shake)
              for(int i = 0; i < 9; i++)
                  sample[i] = vec3(texture(scene, TexCoords.st + offsets[i]));

          // process effects
          if(chaos)
          {           
              for(int i = 0; i < 9; i++)
                  color += vec4(sample[i] * edge_kernel[i], 0.0f);
              color.a = 1.0f;
          }
          else if(confuse)
          {
              color = vec4(1.0 - texture(scene, TexCoords).rgb, 1.0);
          }
          else if(shake)
          {
              for(int i = 0; i < 9; i++)
                  color += vec4(sample[i] * blur_kernel[i], 0.0f);
              color.a = 1.0f;
          }
          else
          {
              color =  texture(scene, TexCoords);
          }
      }" )
   (=: w (/ *GameW 1.0))
   (=: h (/ *GameH 1.0))
   (=: msfbo (glGenFramebuffers 1))
   (=: fbo (glGenFramebuffers 1))
   (=: rbo (glGenRenderbuffers 1))
   (glBindFramebuffer GL_FRAMEBUFFER (: msfbo))
   (glBindRenderbuffer GL_RENDERBUFFER (: rbo))
   (glRenderbufferStorageMultisample GL_RENDERBUFFER 8 GL_RGB (: w) (: h))
   (glFramebufferRenderbuffer GL_FRAMEBUFFER GL_COLOR_ATTACHMENT0 GL_RENDERBUFFER (: rbo))
   (if (<> GL_FRAMEBUFFER_COMPLETE (glCheckFramebufferStatus GL_FRAMEBUFFER))
      (quit "Error in +Effect: Failed to initialize property msfbo")
      (glBindFramebuffer GL_FRAMEBUFFER (: fbo))
      (=: tex (glGenTextures 1))
      (glBindTexture GL_TEXTURE_2D (: tex))
      (glTexImage2D GL_TEXTURE_2D 0 GL_RGB (: w) (: h) 0 GL_RGB GL_UNSIGNED_BYTE 0)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glFramebufferTexture2D GL_FRAMEBUFFER GL_COLOR_ATTACHMENT0 GL_TEXTURE_2D (: tex) 0)
      (if (<> GL_FRAMEBUFFER_COMPLETE (glCheckFramebufferStatus GL_FRAMEBUFFER))
         (quit "Error in +Effect: Failed to initialize property fbo")
         (glBindFramebuffer GL_FRAMEBUFFER 0)
         (=: vao (glGenVertexArrays 1))
         (=: vbo (glGenBuffers 1))
         (glBindBuffer GL_ARRAY_BUFFER (: vbo))
         (glBufferData GL_ARRAY_BUFFER (floats* 1 (-1 -1 0 0 1 1 1 1 -1 1 0 1
            -1 -1 0 0 1 -1 1 0 1 1 1 1 ) ) GL_STATIC_DRAW )
         (glBindVertexArray (: vao))
         (glEnableVertexAttribArray 0)
         (glVertexAttribPointer 0 4 GL_FLOAT GL_FALSE 16 0)
         (glBindBuffer GL_ARRAY_BUFFER 0)
         (glBindVertexArray 0)
         (use> This)
         (uniform> This "scene")
         (setint> This "scene" 0)
         (let (F (/ 1.0 300) -F (- F))
            (uniform> This "offsets")
            (glUniform2fv (: "offsets") 9
               (floats* 1.0 (list -F F 0 F F F -F 0 0 0 F 0 -F -F 0 -F F -F)) ) )
         (uniform> This "edge_kernel")
         (glUniform1fv (: "edge_kernel") 9
               (floats* 1.0 (-1.0 -1.0 -1.0 -1.0 8.0 -1.0 -1.0 -1.0 -1.0)) )
         (uniform> This "blur_kernel")
         (glUniform1fv (: "blur_kernel") 9
               (floats* 1.0 (list
                  (/ 1.0 16) (/ 2.0 16) (/ 1.0 16)
                  (/ 2.0 16) (/ 4.0 16) (/ 2.0 16)
                  (/ 1.0 16) (/ 2.0 16) (/ 1.0 16) ) ) )
         (uniform> This "time")
         (uniform> This "confuse")
         (uniform> This "chaos")
         (uniform> This "shake")
         (=: confuse 0)
         (=: chaos 0)
         (=: shake 0)
         (=: dthru 0)
         (=: dconfuse 0)
         (=: dchaos 0)
         (=: dshake 0)
         (=: dthru 0) ) ) )

(dm begin> ()
   (glBindFramebuffer `GL_FRAMEBUFFER (: msfbo))
   (glClearColor 0 0 0 1.0)
   (glClear `GL_COLOR_BUFFER_BIT) )

(dm end> ()
   (glBindFramebuffer `GL_READ_FRAMEBUFFER (: msfbo))
   (glBindFramebuffer `GL_DRAW_FRAMEBUFFER (: fbo))
   (glBlitFramebuffer 0 0 (: w) (: h) 0 0 (: w) (: h)
      `GL_COLOR_BUFFER_BIT `GL_NEAREST )
   (glBindFramebuffer `GL_FRAMEBUFFER 0) )

(dm render> (Time)
   (use> This)
   (setfloat> This "time" Time)
   (setint> This "confuse" (: confuse))
   (setint> This "chaos" (: chaos))
   (setint> This "shake" (: shake))
   (glActiveTexture 0)
   (glBindTexture `GL_TEXTURE_2D (: tex))
   (glBindVertexArray (: vao))
   (glDrawArrays `GL_TRIANGLES 0 6)
   (glBindVertexArray 0) )

(dm destroy> ()
   (glDeleteTextures (: tex))
   (glDeleteRenderbuffers (: rbo))
   (glDeleteFramebuffers (list (: fbo) (: msfbo)))
   (super) )

(dm updfx> (Delta Player Ball)
   (mapc '((E ED)
      (when (gt0 (get This ED))
         (dec (prop This ED) Delta)
         (when (le0 (get This ED))
            (put This ED 0)
            (put This E 0)
            (case E
               ('bigger (with Player (=: r 1.0) (=: g 1.0) (=: b 1.0)
                           (dec (:: w) 50.0) ) )
               ('thru (with Ball (=: r 1.0) (=: g 1.0) (=: b 1.0))) ) ) ) )
      '(shake confuse chaos thru)
      '(dshake dconfuse dchaos dthru) ) )


# This file depends on sdl.l, sdlutil.l, sdlimage.l and opengl3.l

# This class loads an image file and creates a new texture

(class +GLTexture)

(dm T (File Format)                                                              # Pass a file path and an optional image format
   (default Format GL_RGB)
   (let (Ntex (glGenTextures 1)
         Tex (imgLoad File)                                                      # imgLoad creates an SDL_Surface
         Str (surfstruct Tex) )                                                  # get data of the surface
      (=: id Ntex)
      (glBindTexture GL_TEXTURE_2D Ntex)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_BASE_LEVEL 0)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAX_LEVEL 0)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_REPEAT)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_REPEAT)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glTexStorage2D GL_TEXTURE_2D 1 GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
         (; Str w) (; Str h) )
      (glTexSubImage2D GL_TEXTURE_2D 0 0 0 (; Str w) (; Str h)
         Format GL_UNSIGNED_BYTE (; Str pixels) )
      (glBindTexture GL_TEXTURE_2D 0)
      (sdlFreeSurface Tex) ) )                                                   # After creating the texture, we don't need the surface anymore

(dm bindtex> () (glBindTexture `GL_TEXTURE_2D (: id)))                           # Use the texture

(dm destroy> () (glDeleteTextures (: id)))


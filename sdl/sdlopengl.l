# Always load "sdl.l" before this file!!

(let C 0
    (mapc '((K) (def K C) (inc 'C))
        (quote
            # GL attributes (for sdlGlSetAttribute etc.)
            SDL_GL_RED_SIZE
            SDL_GL_GREEN_SIZE
            SDL_GL_BLUE_SIZE
            SDL_GL_ALPHA_SIZE
            SDL_GL_BUFFER_SIZE
            SDL_GL_DOUBLEBUFFER
            SDL_GL_DEPTH_SIZE
            SDL_GL_STENCIL_SIZE
            SDL_GL_ACCUM_RED_SIZE
            SDL_GL_ACCUM_GREEN_SIZE
            SDL_GL_ACCUM_BLUE_SIZE
            SDL_GL_ACCUM_ALPHA_SIZE
            SDL_GL_STEREO
            SDL_GL_MULTISAMPLEBUFFERS
            SDL_GL_MULTISAMPLESAMPLES
            SDL_GL_ACCELERATED_VISUAL
            SDL_GL_RETAINED_BACKING
            SDL_GL_CONTEXT_MAJOR_VERSION
            SDL_GL_CONTEXT_MINOR_VERSION
            SDL_GL_CONTEXT_EGL
            SDL_GL_CONTEXT_FLAGS
            SDL_GL_CONTEXT_PROFILE_MASK
            SDL_GL_SHARE_WITH_CURRENT_CONTEXT
            SDL_GL_FRAMEBUFFER_SRGB_CAPABLE
            SDL_GL_CONTEXT_RELEASE_BEHAVIOR
            SDL_GL_CONTEXT_RESET_NOTIFICATION
            SDL_GL_CONTEXT_NO_ERROR ) ) )

(mapc '((K) (def (car K) (hex (cadr K))))
    (quote
        (SDL_GL_CONTEXT_PROFILE_CORE            "0001")
        (SDL_GL_CONTEXT_PROFILE_COMPATIBILITY   "0002")
        (SDL_GL_CONTEXT_PROFILE_ES              "0004")
        (SDL_GL_CONTEXT_DEBUG_FLAG              "0001")
        (SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG "0002")
        (SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG      "0004")
        (SDL_GL_CONTEXT_RESET_ISOLATION_FLAG    "0008")
        (SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE   "0000")
        (SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH  "0001")
        (SDL_GL_CONTEXT_RESET_NO_NOTIFICATION   "0000")
        (SDL_GL_CONTEXT_RESET_LOSE_CONTEXT      "0001") ) )

(de sdlGlLoadLibrary (SPath)
    (native `*SDLlib "SDL_GL_LoadLibrary" 'I SPath) )
    # returns 0 (= success) or -1 (= fail)

(de sdlGlGetProcAddress (SProc)
    (native `*SDLlib "SDL_GL_GetProcAddress" 'P SProc) )
    
(de sdlGlUnloadLibrary ()
    (native `*SDLlib "SDL_GL_UnloadLibrary" NIL) )

(de sdlGlExtensionSupported (SExt)
    (native `*SDLlib "SDL_GL_ExtensionSupported" 'I SExt) )
    # returns 1 (= true) or 0 (= false)

(de sdlGlResetAttributes ()
    (native `*SDLlib "SDL_GL_ResetAttributes" NIL) )

(de sdlGlSetAttribute (IAttr IVal)
    (native `*SDLlib "SDL_GL_SetAttribute" 'I IAttr IVal) )
    # returns 0 (= success) or -1 (= fail)

(de sdlGlGetAttribute (IAttr)
    (use (R V)
      (setq R (native `*SDLlib "SDL_GL_GetAttribute" 'I IAttr '(V (4 . I))) )
         (list R V) ) )
    # returns 0 (= success) or -1 (= fail) and the value of the attribute

(de sdlGlCreateContext (PWin)
    (native `*SDLlib "SDL_GL_CreateContext" 'P PWin) )

(de sdlGlMakeCurrent (PWin PContext)
    (native `*SDLlib "SDL_GL_MakeCurrent" 'I PWin PContext) )

(de sdlGlGetCurrentWindow ()
    (native `*SDLlib "SDL_GL_GetCurrentWindow" 'P) )

(de sdlGlGetCurrentContext ()
    (native `*SDLlib "SDL_GL_GetCurrentContext" 'P) )

(de sdlGlGetDrawableSize (PWin)
    (use (IW IH)
       (native `*SDLlib "SDL_GL_GetDrawableSize" PWin '(IW (4 . I))
           '(IH (4 . I)) )
       (list IW IH) ) )

# IInterval can be 0 = immediate updates, 1 = vertical sync,
# -1 = late swaps immediate
(de sdlGlSetSwapInterval (IInterval)
    (native `*SDLlib "SDL_GL_SetSwapInterval" 'I IInterval) )
    # returns 0 (= success) or -1 (= fail)

(de sdlGlGetSwapInterval ()
    (native `*SDLlib "SDL_GL_GetSwapInterval" 'I) )

(de sdlGlSwapWindow (PWin)
    (native `*SDLlib "SDL_GL_SwapWindow" NIL PWin) )

(de sdlGlDeleteContext (PContext)
    (native `*SDLlib "SDL_GL_DeleteContext" NIL PContext) )

# Convenient class for OpenGL windows 

(class +OpenGLWindow)

    # in LAttrib you can pass some OpenGL attributes
    # to be handled by SDL upon window creation
    # LAttrib is a list of cons pairs: '((SDL_GL_DOUBLEBUFFER . 1) ...)
    # Flags must be or'ed together and are passed to sdlCreateSimpleWindow
    # The SDL_WINDOW_OPENGL flag is always set
    
    (dm T (STitle IW IH SVersionMajor SVersionMinor LAttrib IFlag)
        (default SVersionMajor 2 SVersionMinor 1 IFlag 0)
        (let R
            (if (>= SVersionMajor 3)
                (= '(0 0 0)
                    (mapcar '((K V) (sdlGlSetAttribute K V))
                        (list `SDL_GL_CONTEXT_MAJOR_VERSION
                              `SDL_GL_CONTEXT_MINOR_VERSION
                              `SDL_GL_CONTEXT_PROFILE_MASK )
                        (list SVersionMajor
                              SVersionMinor
                              `SDL_GL_CONTEXT_PROFILE_CORE) ) )
                (= '(0 0)
                    (mapcar '((K V) (sdlGlSetAttribute K V))
                        (list `SDL_GL_CONTEXT_MAJOR_VERSION
                              `SDL_GL_CONTEXT_MINOR_VERSION )
                        (list SVersionMajor SVersionMinor) ) ) )
            (when R
                (mapc '((A) (sdlGlSetAttribute (val (car A)) (cdr A))) LAttrib)
                (=: win (sdlCreateSimpleWindow STitle IW IH
                    (| IFlag `SDL_WINDOW_OPENGL))) ) )
        (ifn (: win)
            (quit (sdlGetError) "Error creating window")            
            (=: size (sdlGetWindowSize (: win)))
            (=: ticks (sdlGetTicks))
            (=: rate 0)
            (=: context (sdlGlCreateContext (: win)))
            (if (=0 (: context))
               (quit (sdlGetError) "Error creating OpenGL context") )
               (sdlGlMakeCurrent (: win) (: context))
               (when (sdlGlSetSwapInterval 1)
                   (=: vsync T) ) ) )

    (dm destroy> ()
        (glFinish)
        (sdlGlDeleteContext (: context))
        (sdlDestroyWindow (: win)) )

    # redraws the window immediately
    
    (dm swap> () (sdlGlSwapWindow (: win)))
    
    # redraws the window; if "rate" is configured (using "framerate>")
    # the window will wait until "rate" ticks have elapsed since the
    # last redraw
    
    (dm refresh> ()
        (let (Ti (sdlGetTicks) La (- Ti (: ticks)))
            (when (< La (: rate)) (sdlDelay (- (: rate) La)))
            (=: ticks Ti) )
        (sdlGlSwapWindow (: win)) )

# collision? checks for a bounding axis collision. Rect1 and Rect2 are
# lists of (X Y Width Height)

(de collision? (Rect1 Rect2)
    (and (>= (+ (car Rect1) (caddr Rect1)) (car Rect2))
         (>= (+ (car Rect2) (caddr Rect2)) (car Rect1))
         (>= (+ (cadr Rect1) (cadddr Rect1)) (cadr Rect2))
         (>= (+ (cadr Rect2) (cadddr Rect2)) (cadr Rect1)) ) )

(de collisions (Rect1 Rects) (filter '((X) (collision? Rect1 X)) Rects))

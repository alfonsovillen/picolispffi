# Functions for manipulating 4x4 matrices and 4 row vectors.
# They use the shared library "libtransform.so" in the "lib/" subdirectory.

(setq
    *Trans "@sdl/lib/libtransform.so"    
    "Flt1" (0 . -1.0)
    "Flt2" (0 . -1.0)
    "Flt3" (0 . -1.0)
    "Flt4" (0 . -1.0)
    "Flt5" (0 . -1.0)
    "Flt6" (0 . -1.0)
    "Flt7" (0 . -1.0)
    "Flt8" (0 . -1.0)
    "Flt9" (0 . -1.0) )

# allocate a buffer of I matrices, returning the pointer to the first of them

(de newm (I) (native `*Trans "newm" 'P I))

# take the pointer to a matrix buffer and free the matrices

(de freem (P) (%@ "free" NIL P))

# take the pointer to a matrix buffer and set matrix nr. I to identity

(de identm (P I) (native `*Trans "identm" NIL P I))

# take the pointer to a matrix buffer, set up a translation matrix
# and store it in matrix nr. I

(de transm (P I X Y Z)
    (set "Flt1" X "Flt2" Y "Flt3" Z)
    (native `*Trans "transm" NIL P I "Flt1" "Flt2" "Flt3") )

# take the pointer to a matrix buffer, set up a scaling matrix
# and store it in matrix nr. I

(de scalem (P I X Y Z)
    (set "Flt1" X "Flt2" Y "Flt" Z)
    (native `*Trans "scalem" NIL P I "Flt1" "Flt2" "Flt3") )

# take the pointer to a matrix buffer, set up a rotation matrix
# and store it in matrix nr. I

(de rotzm (P I R)
    (set "Flt1" R)
    (native `*Trans "rotzm" NIL P I "Flt1") )

(de rotym (P I R)
    (set "Flt1" R)
    (native `*Trans "rotym" NIL P I "Flt1") )

(de rotxm (P I R)
    (set "Flt1" R)
    (native `*Trans "rotxm" NIL P I "Flt1") )

# take the pointer to a matrix buffer, multiply matrices from nr. A
# to nr. B and store the result in matrix nr. I. The pointer to
# matrix nr. I is returned

(de multm (P I A B) (native `*Trans "multm" 'P P I A B))

# take the pointer to a matrix buffer, create a look-at matrix and
# store it in matrix nr. I

(de lookatm (P I EyeX EyeY EyeZ TargetX TargetY TargetZ UpX UpY UpZ)
    (set "Flt1" EyeX "Flt2" EyeY "Flt3" EyeZ
         "Flt4" TargetX "Flt5" TargetY "Flt6" TargetZ
         "Flt7" UpX "Flt8" UpY "Flt9" UpZ )
    (native `*Trans "lookatm" NIL P I "Flt1" "Flt2" "Flt3" "Flt4"
        "Flt5" "Flt6" "Flt7" "Flt8" "Flt9" ) )

# take the pointer to a matrix buffer, create a perspective
# projection matrix and store it in matrix nr. I

(de perspm (P I Fov Ratio Near Far)
    (set "Flt1" Fov "Flt2" Ratio "Flt3" Near "Flt4" Far)
    (native `*Trans "perspm" NIL P I "Flt1" "Flt2" "Flt3" "Flt4") )
    
# take the pointer to a matrix buffer, create a model-view-projection
# matrix (Model, View and Proj are matrix indices), store the result
# in matrix nr. I and return the pointer to the result matrix,
# so it can e.g. be passed to glUniform

(de mvpm (P I Model View Proj) (native `*Trans "mvpm" 'P P I Model View Proj))

# take the pointer to a matrix buffer and display the contents
# of matrix nr. I

(de disp (P I) (native `*Trans "disp" NIL P I))

# Implement a score board with SDL. It creates a textured sprite
# from a PNG file and renders it to the renderer of a +Window object.

# Always load "sdl.l" and "sdlimage.l" before this file!!

(class +Score +SprImg)

    # IW/IH are the width and height of one digit.
    # In order to resize the board to IW/IH (they're not NIL),
    # you can use a +Window or specify a pixel depth and an integer
    # corresponding to a pixel format.
    
    (dm T (File IW IH OWin IBpp IPxf)
    
        # digitmap.png is a PNG image with a transparent background
        # and the digits 0-9 in it. Each digit is 32px wide and
        # 64px high.
        
        (super File)
        (when (and IW IH (or (not (= IW 32)) (not (= IH 64))))
            (resize> This (* IW 10) IH (sdlGetSurfaceBpp (: surf))
                (sdlGetSurfacePixelFormatVal (: surf)) ) )
        
        (totexture> This OWin)
        (setsrc> This 0 0 IW IH)
        (setdest> This 0 0 IW IH)
        (=: width IW)
        (=: renderer (get OWin 'renderer)) )
    
    # Renders a number on a +RenderWindow
    
    (dm number> (N IX IY)
        (for D (mapcar format (chop (format N)))
            (srcxy> This (* (: width) D) 0)
            (xy> This IX IY)
            (copyto> This (: renderer))
            (inc 'IX (: width)) ) )

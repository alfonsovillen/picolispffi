(load "@sdl/nativearg.l")

(de "words" (L) (mapcar pack (split (chop L) " ")))

(de "nums" (L FlipY)
      # if FlipY is T, we want to parse 2 UV coordinates and return
      # the second one subtracted from 1, so the UV coordinates will be
      # flipped vertically. OpenGL needs that
      (if FlipY
         (let V (cdr ("words" L))
            (list (format (car V) *Scl) (- 1.0 (format (cadr V) *Scl))) )
         # otherwise, get the coordinates (any number of them, normally 3)
         (mapcar '((X) (format X *Scl)) (cdr ("words" L))) ) )

(de "indi" (L) (mapcar '((X) (format X)) (split (chop L) "/")))

(de "indices" (L) (mapcar "indi" (cdr ("words" L))))

# Load a simple Blender .OBJ file (only vertices, texture coordinates
# and normals, plus the information of the faces).
# All data is returned in a way that it can be passed to OpenGL functions
# such as glBufferData directly: vertex coordinates + UV coordinates +
# normal coordinates in a buffer (8 floats per vertex = 32 bytes) and
# elements in another buffer (unsigned integers)
# The functions floats* and uints* are used to pass
# the data to the 'native' function that calls the OpenGL function. See
# opengl3.l for details.
# Try with (loadobj "demos/meshes/XXXXX.obj")

(de loadobj ("File")
   (let (V NIL UV NIL N NIL F NIL DU NIL ArrBuf NIL ElemBuf NIL)
   
      # parse file
      (in "File"
         (while (line T)
            (cond
               # parse a vertex coordinate
               ((pre? "v " @) (queue 'V ("nums" @)))
               # parse a UV coordinate. Note that UV coordinates
               # are flipped on the Y axis, so they will be mapped correctly
               # only if the texture image is flipped vertically
               ((pre? "vt " @) (queue 'UV ("nums" @ T)))
               # parse a normal coordinate
               ((pre? "vn " @) (queue 'N ("nums" @)))
               # parse a triangle
               ((pre? "f " @) (queue 'F ("indices" @))) ) ) )
               
      # convert the data into a format OpenGL can work with
      # step 1
      (setq DU (make (for (I . E)   # store in DU a list of
         (uniq (apply append F))    # unique triangle vertex descriptors
         (link (list E              # store the element
                     I              # with its index
                     (append
                        (get V (car E)) # and the V, UV and N elements
                        (get UV (cadr E)) # pointed by E as a single list
                        (get N (caddr E)) ) ) ) ) ) )
                        
      # step 2: store the array buffer data. floats* is in nativearg.l
      (setq ArrBuf (floats* 1.0 (apply append (mapcar caddr DU))))
      
      # step 3: go through every triangle, look up their vertices in DU
      # and store in the element buffer the corresponding index
      # uints* is in nativearg.l
      (setq ElemBuf (uints* (make (for Tr F # Tr = a triangle
         (for E Tr # E = a vertex with three indices (V UV N)
            # store de index, but beware: OpenGL counts from 0, so decrement
            (link (dec (cadr (assoc E DU)))) ) ) ) ) )
            
   # return the data: a native argument specification with the list
   # of vertices (12 bytes each) + texture coordinates (8 bytes each) +
   # normal coordinates
   # (12 bytes each), the number of elements that make up the mesh,
   # and a native argument specification with the list of elements
   # (unsigned integers, 4 bytes each)
   (list (cons 'vertices ArrBuf)
         (cons 'numelems (* (length F) 3))
         (cons 'elems ElemBuf) ) ) )

# obj2bin parses a .OBJ file and, after processing it, saves it in a form
# that can be "read" directly by Picolisp and assigned to a symbol

(de obj2bin ("ObjFile" "BinFile")
   (let B (loadobj "ObjFile") (out "BinFile" (println B))) )

# loadbin reads a file produced by obj2bin

(de loadbin ("BinFile") (in "BinFile" (read)))

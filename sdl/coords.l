# Convenient class for working with rects and bounding boxes. The coordinates
# can be in a different scale than their real representation. That allows
# working with floating point coordinates that can be translated to screen
# coordinates. The "scale" property stores the internal scale.

(class +Rect)

    (dm T (X Y W H S)
        (default S 1)
        (=: left X)
        (=: top Y)
        (=: width W)
        (=: height H)
        (=: right (+ X W))
        (=: bottom (+ Y H))
        (=: scale S) )
    
    (dm set> (X Y)
        (=: left X)
        (=: top Y)
        (=: right (+ X (: width)))
        (=: bottom (+ Y (: height))) )
    
    # move the rect positions a given amount of horizontal and
    # vertical units
    
    (dm delta> (DX DY)
        (set> This (+ (: left) DX) (+ (: top) DY)) )
    
    # set a random X or Y position within the bounds of the defined width/height
    # and a maximum allowed X or Y coordinate.
    # randomX,Y take the rect's width/height into account
    
    (dm randomX> (MaxX Y)
        (let X (rand (: width) MaxX)
            (=: left (- X (: width)))
            (=: right X) )
        (when Y
            (=: top Y)
            (=: bottom (+ Y (: height))) ) )
    
    (dm randomY> (MaxY X)
        (let Y (rand (: height) MaxY)
            (=: top (- Y (: height)))
            (=: bottom Y) )
        (when X
            (=: left X)
            (=: right (+ X (: width))) ) )
    
    # positive N values zoom in, negative ones zoom out
    (dm zoom> (N)
        (=: width (+ N N (: width)))
        (=: height (+ N N (: height)))
        (dec (:: top) N)
        (dec (:: left) N)
        (inc (:: bottom) N)
        (inc (:: right) N) )
    
    # Scales down the X and Y coordinates, as well as the width and the height,
    # so that they can be passed to a SDL_Rect struct.
    
    (dm scalexywh> ()
        (mapcar '((X) (/ (get This X) (: scale))) '(left top width height)) )
    
    # See if a point is inside the rectangle
    
    (dm inside?> (X Y)
        (and (<= (: left) X (: right)) (<= (: top) Y (: bottom))) )

    # Adapt the rectangle position and size so that it
    # encloses a set of points

    (dm enclose> (L)
        (let (MinX (mini car L)
              MaxX (maxi car L)
              MinY (mini cadr L)
              MaxY (maxi cadr L) )
            (=: width (- MaxX MinX))
            (=: height (- MaxY MinY))
            (set> This MinX MinY) ) )

    # Return the position and dimensions (x y width height) of the portion
    # of this rect that overlaps another rect, or NIL if both rects
    # don't overlap

    (dm intersect> (R)
        (let (MinX (min (: left) (; R left))
              MaxX (max (: right) (; R right))
              MinY (min (: top) (; R top))
              MaxY (max (: bottom) (; R bottom)) )
            (and
                (or (< (- MaxX MinX) (+ (: width) (; R width)))
                    (< (- MaxY MinY) (+ (: height) (; R height))) )
                (list (max (: left) (; R left))
                      (max (: top) (; R top))
                      (- (+ (: width) (; R width)) (- MaxX MinX))
                      (- (+ (: height) (; R height)) (- MaxY MinY) ) ) ) ) )

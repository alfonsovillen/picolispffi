(setq
   TTF_STYLE_NORMAL            0
   TTF_STYLE_BOLD              1
   TTF_STYLE_ITALIC            2
   TTF_STYLE_UNDERLINE         4
   TTF_STYLE_STRIKETHROUGH     8
   TTF_HINTING_NORMAL          0
   TTF_HINTING_LIGHT           1
   TTF_HINTING_MONO            2
   TTF_HINTING_NONE            3 )

(def 'SDLttf "libSDL2_ttf.so")

(de ttfInit () (native `SDLttf "TTF_Init" 'I))
(de ttfQuit () (native `SDLttf "TTF_Quit"))
(de ttfOpenFont @ (pass native `SDLttf "TTF_OpenFont" 'P))
(de ttfCloseFont @ (pass native `SDLttf "TTF_CloseFont" NIL))
(de ttfGetFontStyle @
   (let F (pass native `SDLttf "TTF_GetFontStyle" 'I)
      (make
         (for S '((0 . normal) (1 . bold) (2 . italic) (4 . underline)
                  (8 . strikethrough) )
            (when (= (car S) (& F (car S)))
               (link (cdr S)) ) ) ) ) )
(de ttfSetFontStyle @ (pass native `SDLttf "TTF_SetFontStyle" NIL))
(de ttfGetFontHinting @ (pass native `SDLttf "TTF_GetFontHinting" 'I))
(de ttfSetFontHinting @ (pass native `SDLttf "TTF_SetFontHinting" NIL))
(de ttfFontFaceIsFixedWidth @
   (gt0 (pass native `SDLttf "TTF_FontFaceIsFixedWidth" 'I)) )
(de ttfGlyphMetrics (PFont Ch)
   (let (B (%@ "malloc" 'P 20)
         M NIL )
      (when (=0 (native `SDLttf "TTF_GlyphMetrics" 'I PFont Ch
         B (+ B 4) (+ B 8) (+ B 12) (+ B 16) ) )
         (setq M (struct B '(I I I I I)))
         (%@ "free" NIL B)
         M ) ) )
(de ttfSizeUTF8 (PFont SText)
   (use (W H)
      (when (=0 (native `SDLttf "TTF_SizeUTF8" 'I PFont SText
         '(W (4 . I)) '(H (4 . I)) ) )
         (list W H) ) ) )
# don't forget to call sdlFreeSurface when you're done using the text!
(de ttfRenderUTF8Blended (Font Text Color4)
   (let C 0
      # Convert the Color4 list into an SDL_Color (a 4-byte number)
      (mapc '((V F) (inc 'C (* V F))) Color4 (1 256 65536 16777216))
      (native `SDLttf "TTF_RenderUTF8_Blended" 'P Font Text C) ) )


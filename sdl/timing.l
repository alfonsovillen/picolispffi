# This function is like (bench), but returns microseconds

(de mbench Prg
    (let U (usec)
        (prog1
            (run Prg)
            (prinl (format (- (usec) U) 3) "ms") ) ) )

# This function returns the timing results in microseconds
# once every second, useful for animation loops

(de secbench Prg
    (default *U1 (usec))
    (setq *U2 (usec))
    (run Prg)
    (when (<= 1000000 (- *U2 *U1))
        (setq *U1 (usec))
        (prinl (format (- *U1 *U2) 3) "ms/frame") ) )

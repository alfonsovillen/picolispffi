(load "@sdl/sdl.l" "@sdl/sdlimage.l" "@sdl/sdlopengl.l" "@sdl/openGl.l"
   "@sdl/sdlttf.l" )

(push1 '*Bye '(and *Window (endturtle)))

# define things not at all or not well defined in @lib/openGl.l

(setq GL_TEXTURE_2D_MULTISAMPLE 36949
      GL_RGBA8 32856 )

(de glFinish () (native `*GLlib "glFinish"))

(setq glTexImage2D '(@ (pass native *GlutLib "glTexImage2D" NIL))
      glBindTexture '(@ (pass native *GlutLib "glBindTexture" NIL)) )

(de glTexParameteri @ (pass native *GlutLib "glTexParameteri" NIL))

(de glGenTextures @ (pass native *GlutLib "glGenTextures" NIL))

(de glDeleteTextures @ (pass native *GlutLib "glDeleteTextures" NIL))

(de glReadPixels @ (pass native *GlutLib "glReadPixels" NIL))

(de glDrawPixels @ (pass native *GlutLib "glDrawPixels" NIL))

(de glVertex2i @ (pass native *GlutLib "glVertex2i" NIL))

(de glTexEnvi @ (pass native *GlutLib "glTexEnvi" NIL))

(de glGenFramebuffers @ (pass native *GlutLib "glGenFramebuffersEXT" NIL))

# 36160 = GL_FRAMEBUFFER_EXT
(de glBindFramebuffer @ (pass native *GlutLib "glBindFramebufferEXT" NIL 36160))

(de glGenRenderbuffers @ (pass native *GlutLib "glGenRenderbuffersEXT" NIL))

# 36161 = GL_RENDERBUFFER_EXT
(de glBindRenderbuffer @ (pass native *GlutLib "glBindRenderbufferEXT" NIL 36161))

(de glRenderbufferStorageMultisample @ (pass native *GlutLib 
   "glRenderbufferStorageMultisampleEXT" NIL 36161 ) )

# 36064 = GL_COLOR_ATTACHMENT0_EXT
(de glFramebufferRenderbuffer @ (pass native *GlutLib "glFramebufferRenderbufferEXT"
   NIL 36160 36064 36161 ) )

(de glDeleteRenderbuffers @ (pass native *GlutLib "glDeleteRenderbuffersEXT" NIL))

(de glDeleteFramebuffers @ (pass native *GlutLib "glDeleteFramebuffersEXT" NIL))

# return value 36053 = GL_FRAMEBUFFER_COMPLETE_EXT
(de glCheckFramebufferStatus () (native *GlutLib "glCheckFramebufferStatusEXT" 'I 36160))

# 36009 = GL_DRAW_FRAMEBUFFER_EXT, 36008 = GL_READ_FRAMEBUFFER_EXT
(de glBindDrawFramebuffer (N) (native *GlutLib "glBindFramebufferEXT" NIL 36009 N))

(de glBindReadFramebuffer (N) (native *GlutLib "glBindFramebufferEXT" NIL 36008 N))

(de glBlitFramebuffer @ (pass native *GlutLib "glBlitFramebufferEXT" NIL))

# color definitions. default colors are opaque - same colors as FMS Logo

(unless *Colors
   (let N 0
      (setq *Colors
         (make
            (for D
               (quote
                  (black      0        0        0        1.0)
                  (blue       0        0        1.0      1.0)
                  (green      0        1.0      0        1.0)
                  (cyan       0        1.0      1.0      1.0)
                  (red        1.0      0        0        1.0)
                  (magenta    1.0      0        1.0      1.0)
                  (yellow     1.0      1.0      0        1.0)
                  (white      1.0      1.0      1.0      1.0)
                  (brown      0.607843 0.376471 0.231373 1.0)
                  (ocre       0.772549 0.533333 0.070588 1.0)
                  (dark-green 0.392157 0.635294 0.250980 1.0)
                  (turquoise  0.470588 0.733333 0.733333 1.0)
                  (tan-brown  1.0      0.584314 0.466667 1.0)
                  (plum       0.564706 0.443137 0.815686 1.0)
                  (orange     1.0      0.639216 0        1.0)
                  (gray       0.717647 0.717647 0.717647 1.0) )
               (def (car D) (cdr D))
               (link (cdr D))
               (inc 'N) ) ) ) ) )

# turtle graphics definitions

# convert color format 0..1.0 into 0..255

(de rgba (L) (mapcar '((V) (*/ 255 V 1.0)) L))

# the pen color can be one of 0..15, a color symbol, or a list of R G B A
# fixed point values between 0 and 1.0

(de pencolor () (; *Window fore))

(de setpencolor (RGBA)
   (cond
      ((lst? RGBA)
         (put *Window 'fore RGBA) )
      ((<= 0 RGBA 15)
         (put *Window 'fore (get *Colors (inc RGBA))) ) )
   (apply glColor4f (; *Window fore)) )

# the screen color can be one of 0..15, a color symbol or an RGBA value

(de background () (; *Window bkgnd))

(de setbackground (RGBA)
   (cond
      ((lst? RGBA)
         (put *Window 'bkgnd RGBA) )
      ((<= 0 RGBA 15)
         (put *Window 'bkgnd (get *Colors (inc RGBA))) ) )
   (apply glClearColor (; *Window bkgnd)) )

# tint a color from 0 (completely black) to 1.0 (completely white)

(de tint (RGBA F)
   (use N
      (make
         (for C (cut 3 'RGBA)
            (setq N (+ C F))
            (link (if (le0 N) 0 (if (> N 1.0) 1.0 N))) )
         (link (pop 'RGBA)) ) ) )

# set transparency of the current pen color

(de penalpha () (last (; *Window fore)))

(de setpenalpha (A)
   (setpencolor (append (head 3 (; *Window fore)) (list A))) )
   
# create the turtle graphics window

# the graphics window is a single-buffered OpenGL 2.1 window created with SDL2,
# it includes an offscreen multisampled framebuffer and a texture for the 
# turtle icon.
# the back framebuffer (*Window -> fb) will be the active framebuffer by 
# default, only the turtle will be drawn onto the main framebuffer (0) directly.

(de startturtle (W H Title)
   (default Title "Turtle Graphics")
   (or *Window
      (prog
         (default W 512 H 512)
         (sdlInit `SDL_INIT_VIDEO)
         (ttfInit)
         (imgInit `IMG_INIT_PNG)
         (setq *Window (new '(+OpenGLWindow) Title W H 2 1
            '((SDL_GL_DOUBLEBUFFER . 0) (SDL_GL_DEPTH_SIZE . 0)
              (SDL_GL_RED_SIZE . 8) (SDL_GL_BLUE_SIZE . 8)
              (SDL_GL_GREEN_SIZE . 8) (SDL_GL_ALPHA_SIZE . 8)
              (SDL_GL_MULTISAMPLEBUFFERS . 0) (SDL_GL_MULTISAMPLESAMPLES . 0) ) ) )
         (setq *WinW W *WinH H)
         (ifn ("create-fb")
            (quit "Could not set up the graphics window properly!")
            ("create-turtle")
            (glViewport 0 0 W H)
            (glMatrixMode GL_PROJECTION)
            (glLoadIdentity)
            (glOrtho 0 (* 1.0 W) (* 1.0 H) 0 -1.0 1.0)
            (glEnable GL_BLEND)
            (glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA)
            (reset) ) ) ) )

# create an extra, multisampled framebuffer - the drawing will be rendered there

(de "create-fb" ()
   (use (R CA)
      (glGenFramebuffers 1 '(R (4 . I)))
      (put *Window 'fb R)
      (glBindFramebuffer R)
      (glGenRenderbuffers 1 '(CA (4 . I)))
      (put *Window 'fbrb CA)
      (glBindRenderbuffer CA)
      (glRenderbufferStorageMultisample 4 GL_RGBA8 *WinW *WinH )
      (glFramebufferRenderbuffer CA)
      (= 36053 (glCheckFramebufferStatus)) ) )

(de "destroy-fb" ()
   (and (; *Window fb) (glDeleteFramebuffers 1 (cons NIL (4) (- @))))
   (and (; *Window fbrb) (glDeleteRenderbuffers 1 (cons NIL (4) (- @)))) )

# blit the extra framebuffer to the window

(de drawfb ()
   (glFlush)
   (glBindDrawFramebuffer 0)
   (glBindReadFramebuffer (; *Window fb))
   (use E
      (glBlitFramebuffer 0 0 *WinW *WinH 0 0 *WinW *WinH
         GL_COLOR_BUFFER_BIT GL_NEAREST )
      (glFlush)
      (setq E (glGetError))
      (glBindFramebuffer (; *Window fb))
      (when (<> "0" E) (prinl "drawfb error " E)) ) )
   
# close the turtle graphics window

(de endturtle ()
   (while *Icons (destroyicon (car @)))
   ("destroy-font")
   ("destroy-fb")
   (destroy> *Window)
   (wipe '(*Window *WinW *WinH))
   (imgQuit)
   (sdlQuit) )

# display surface info
# this isn't needed, unless for debugging iconfromscreen and savefromscreen
#{
(de surface-info (Text Inf Dump)
   (prinl Text)
   (println 'Surface Inf)
   (let P (pxfmtstruct (get Inf 'pxfmt))
      (println 'PixelFormat P)
      (println 'PixelFormatName (sdlGetPixelFormatName (get P 'fmtval))) )
   (when Dump
      (println 'Dump (struct (get Inf 'pixels)
         (cons 'B (* 4 (get Inf 'w) (get Inf 'h))) ) ) ) )
}#

(de "create-texture" (W H Pixels)
   (use R
      (glGenTextures 1 '(R (4 . I)))
      (glBindTexture GL_TEXTURE_2D R)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      # 10242 = 0x2802 = GL_TEXTURE_WRAP_S
      # 33071 = 0x812f = GL_CLAMP_TO_EDGE
      (glTexParameteri GL_TEXTURE_2D 10242 33071)
      # 10243 = 0x2803 = GL_TEXTURE_WRAP_T
      (glTexParameteri GL_TEXTURE_2D 10243 33071)
      # 8960 = 0x2300 = GL_TEXTURE_ENV
      # 8704 = 0x2200 = GL_TEXTURE_ENV_MODE
      # 7681 = 0X1e01 = GL_REPLACE
      (glTexEnvi 8960 8704 7681)
      (glTexImage2D GL_TEXTURE_2D 0 GL_RGBA8 W H 0 GL_RGBA
         GL_UNSIGNED_BYTE Pixels )
      R ) )
      
(de iconfromfile (File)
   # create a texture
   (let (Sur (imgLoad File) # Use SDL2_image to load the image
         Inf (surfstruct Sur)             # Get the SDL_SURFACE structure
         Icon ("create-texture" (get Inf 'w) (get Inf 'h) (get Inf 'pixels)) )
      (sdlFreeSurface Sur)
      Icon ) )

# create the turtle icon (a texture) - the turtle can be 8.0, 16.0, 32.0,
# 48.0 or 64.0 pixels big

(de "create-turtle" ()
   (unless (; *Window icon)
      (put *Window 'icon
         (iconfromfile
            `(pack (car (file)) "res/tortoise.png")) )
      (put *Window 'tsize 32.0) ) )

(de iconfit (X Y W H)
   (and
      (< -1 X *WinW)
      (< -1 Y *WinH)
      (<= (+ X W) *WinW)
      (<= (+ Y H) *WinH) ) )

# create an icon (texture) from a region of the screen

(de iconfromscreen (X Y W H)
   (when (iconfit X Y W H)
      (drawfb)
      (let (Buf (%@ "malloc" 'P (* W H 4))
            Icon NIL )
         (glBindFramebuffer 0)
         (glReadPixels X (- *WinH (+ Y H))
            W H GL_RGBA GL_UNSIGNED_BYTE Buf)
         (setq Icon ("create-texture" W H Buf))
         (glBindFramebuffer (; *Window fb))
         (%@ "free" NIL Buf)
         (when (showturtle?) (drawturtle))
         (push '*Icons Icon) ) ) )

# save a region of the screen as a PNG image file

(de savefromscreen (X Y W H SFile)
   (when (iconfit X Y W H)
      (drawfb)
      (let (Buf (%@ "malloc" 'P (* W H 4))
            Showt (showturtle?)
            Surf NIL )
         (hideturtle)
         (glBindFramebuffer 0)
         (glReadPixels X (- *WinH (+ Y H))
            W H GL_RGBA GL_UNSIGNED_BYTE Buf)
         (setq Surf (sdlCreateRGBSurfaceFrom Buf W H 32 (* W 4)
            255 65280 16711680 4278190080 ) )
         (sdlFlipSurfaceVert Surf)
         (imgSavePNG Surf SFile)
         (sdlFreeSurface Surf)
         (%@ "free" NIL Buf)
         (put *Window 'showt Showt)
         (repaint) ) ) )
         
# free an icon texture

(de destroyicon (Icon)
   (glDeleteTextures 1 (cons NIL (4) (- Icon)))
   (del Icon '*Icons) )

# draw the turtle icon - the texture is always rendered in the main window,
# so it appears on top of the drawing, but the drawing (without the turtle icon)
# is rendered in the extra framebuffer

(de "draw-icon" (Icon Sx Sy)
   (glBindTexture GL_TEXTURE_2D Icon)
   (glMatrixMode GL_MODELVIEW)
   (glLoadIdentity)
   (let (Nx (/ Sx 2) Ny (/ Sy 2))
      (glTranslatef (xcor) (ycor) 0)
      (glRotatef (degrees (heading)) 0 0 1.0)
      (glEnable GL_TEXTURE_2D)
      (glBegin GL_QUADS)
      (glTexCoord2f 0 1.0)
      (glVertex2f (- Nx) (- Ny))
      (glTexCoord2f 0 0)
      (glVertex2f (- Nx) Ny)
      (glTexCoord2f 1.0 0)
      (glVertex2f Nx Ny)
      (glTexCoord2f 1.0 1.0)
      (glVertex2f Nx (- Ny))
      (glEnd)
      (glLoadIdentity) )
      (glDisable GL_TEXTURE_2D) )

(de drawturtle ()
   (glBindFramebuffer 0)
   (let S (; *Window tsize)
      ("draw-icon" (; *Window icon) S S) )
   (glFlush)
   (glBindFramebuffer (; *Window fb)) )
      
# create the font for text rendering

(de setlabelheight (Size)
   (when (; *Window font) (ttfCloseFont @))
   (put *Window 'font (ttfOpenFont (pack (; *Window fname) ".ttf") Size))
   (put *Window 'fsize Size)
   (unless (; *Window ftex)
      (use R
         (glGenTextures 1 '(R (4 . I)))
         (put *Window 'ftex R) ) ) )

(de setlabelfont (Name Size)
   (put *Window 'fname Name)
   (setlabelheight Size) )

# destroy the font object and close SDL2_ttf

(de "destroy-font" ()
   (glDeleteTextures 1 (cons NIL (4) (; *Window ftex)))
   (ttfCloseFont (; *Window font))
   (ttfQuit) )

# resize the turtle graphics window

(de resizewindow (W H)
   ("destroy-fb")
   (sdlSetWindowSize (; *Window win) W H )
   (put *Window 'size (list W H))
   (setq *WinW W *WinH H)
   (glViewport 0 0 W H)
   (glMatrixMode GL_PROJECTION)
   (glLoadIdentity)
   (glOrtho 0 (* 1.0 W) (* 1.0 H) 0 -1.0 1.0)
   ("create-fb")
   (reset) )
   
# antialiased lines and polygons

(de smoothon ()
   # 32925 = GL_MULTISAMPLE
   (glEnable 32925) )

# non-antialiased lines and polygons

(de smoothoff ()
   (glDisable 32925) )

# default state

(de reset ()
   (setbackground (1.0 1.0 1.0 1.0))
   (setpencolor (0 0 0 1.0))
   (setpenwidth 1.0)
   (setturtlesize 3)
   (setheading 0)
   (smoothon)
   (pendown)
   (clearscreen)
   (showturtle)
   (setlabelfont `(pack (car (file)) "res/DejaVuSans") 16)
   (loop0) )

(de penup () (put *Window 'down NIL))

(de pendown () (put *Window 'down T))

(de setpenwidth (N)
   (put '*Window 'pw N)
   (glLineWidth N) )

# current angle of the turtle - in radians

(de heading () (; *Window angle))

(de degrees (Rad) (*/ 180.0 Rad pi))

# if the pen of the turtle is down

(de down? () (; *Window down))

# the "loop" property avoids unnecessary repainting - "repaint" only draws
# if loop = 0
# the "to" and "rep" functions increase the value of the "loop" property,
# so that the render is done only after the last of those commands has returned

(de loop0 ()
   (put *Window 'loop 0)
   (repaint) )

(de loop+ () (inc (prop *Window 'loop)))

(de loop- ()
   (and (=0 (dec (prop *Window 'loop)))
        (repaint) ) )

(de loop? () (gt0 (; *Window loop)))

(de repaint ()
   (drawfb)
   (when (showturtle?)
      (drawturtle) ) )
         
# turtle coordinates are scaled

(de xcor () (; *Window x))

(de ycor () (; *Window y))

(de pos () (list (xcor) (ycor)))

# check if a filled polygon is being drawn

(de poly? () (; *Window poly))

# define new drawing functions (see examples below) - "to" increases the loop #
# property before the commands, so the drawing is updated after the function
# returns (and loop = 0, meaning that there are no more commands to be executed
# and the turtle is in "immediate mode")

(de to L
   (def (++ L)
      (make
         (link (++ L) '(loop+))
         (chain L)
         (link '(loop-)) ) ) )
         
(to showturtle () (put *Window 'showt T))

(to hideturtle () (put *Window 'showt))

(de showturtle? () (; *Window showt))

(de turtlesize () (; *Window tsize))

(to setturtlesize (N) # must be 1 (8x8 px), 2 (16x16 px), 3 (32x32 px),
                      # 4 (48x48 px) or 5 (64x64 px)
   (when (< 0 N 6)
      (put *Window 'tsize (get (8.0 16.0 32.0 48.0 64.0) N)) ) )

# draw an icon (a texture) on the screen (the back framebuffer)

(to icon (Icon W H) ("draw-icon" Icon W H))

# place the turtle at the center of the screen, and repaint if it is visible

(to home (Color)
   (goto (*/ 1.0 *WinW 2) (*/ 1.0 *WinH 2) Color) )

(to left (N)
   (dec (prop *Window 'angle) (*/ N pi 180.0)) )

(to right (N)
   (inc (prop *Window 'angle) (*/ N pi 180.0)) )

(to setheading (A)
   (put *Window 'angle (*/ A pi 180.0)) )

# make the turtle point towards a target point

(to towards (X Y)
   (put *Window 'angle (atan2 (- Y (ycor)) (- X (xcor)))) )
         
# goto checks if a filled polygon is being drawn and does different things

(to goto (X Y Color)
   (when (down?)
      (if (poly?)
         (prog
            (and Color (setpencolor Color))
            (glVertex2f X Y) ) # when drawing a filled polygon
         (glBegin GL_LINES) # when drawing single lines
         (glVertex2f (xcor) (ycor))
         (and Color (setpencolor Color))
         (glVertex2f X Y)
         (glEnd) ) )
   (put *Window 'x X)
   (put *Window 'y Y) )

# jumpto is like goto, but it never draws a line

(to jumpto (X Y)
   (put *Window 'x X)
   (put *Window 'y Y) )

(de forward (N Color)
   (goto (+ (xcor) (*/ N (cos (heading)) 1.0))
         (+ (ycor) (*/ N (sin (heading)) 1.0))
         Color ) )
         
(de back (N Color) (forward (- N) Color))

(de setx (X Color) (goto X (ycor) Color))

(de sety (Y Color) (goto (xcor) Y Color))

# clears the screen

(to clean ()
   (glClear GL_COLOR_BUFFER_BIT) )

(to clearscreen ()
   (jumpto (*/ 1.0 *WinW 2) (*/ 1.0 *WinH 2))
   (clean) )

# pget and pset are used for flood filling
# screen coordinates (pget / pset) are not scaled

(de pget (P XY)
   (glReadPixels (car XY) (cdr XY) 1 1 GL_RGB GL_UNSIGNED_BYTE P)
   (struct P '(B . 3)) )

(de pset (L)
   (glBegin GL_POINTS)
   (for XY L (glVertex2i (car XY) (cdr XY)))
   (glEnd) )
   
(de visible? (XY)
   (and
      (< -1 (car XY) *WinW)
      (< -1 (cdr XY) *WinH)
      XY ) )

# start a flood fill at the current position of the turtle using the current color;
# pixels are read from the main window and drawn in the extra framebuffer

# two stacks are used: Check stores the pixels to be checked, Paint holds the
# pixels to be painted

(to floodfill ()
   (drawfb)
   (glBindFramebuffer 0)
   (let (B (%@ "malloc" 'P 8) # off 0 = src, 4 = get
         Src NIL
         Get (+ B 4)
         Spos (cons (/ (xcor) 1.0)
              (- *WinH (/ (ycor) 1.0)) )
         Check (and (visible? Spos) (list @)) # start only if XY inside the screen
         Paint NIL
         N NIL )
      (when Check
         (setq Src (pget B Spos)) # store source color
         (while (setq N (++ Check)) # check pixels
            (when
               (and
                  (not (member N Paint))
                  (= Src (pget Get N)) )
               (push 'Paint N)
               (and (visible? (cons (inc (car N)) (cdr N))) (push 'Check @))
               (and (visible? (cons (dec (car N)) (cdr N))) (push 'Check @))
               (and (visible? (cons (car N) (inc (cdr N)))) (push 'Check @))
               (and (visible? (cons (car N) (dec (cdr N)))) (push 'Check @)) ) )
         (glBindFramebuffer (; *Window fb))
         (glMatrixMode GL_PROJECTION)
         (glPushMatrix)
         (glLoadIdentity)
         (glOrtho 0 (* 1.0 *WinW)
            0 (* 1.0 *WinH) -1.0 1.0 )
         (pset Paint)
         (glPopMatrix)
         (glMatrixMode GL_MODELVIEW)
         (%@ "free" NIL B) ) ) )

(to rep (N . Prg)
   (for Repcount N
      (run Prg) ) )

# draw a filled polygon

(to filled Prg
   (put *Window 'poly T)
   (glBegin GL_POLYGON)
   (apply glVertex2f (pos))
   (run Prg)
   (glEnd)
   (put *Window 'poly) )

# render some text with the current font and the current font size at the
# current position and angle of the turtle

(to label @
   (use (Color Sur Inf)
      (setq Color (rgba (; *Window fore)))
      (setq Sur (ttfRenderUTF8Blended (; *Window font)
         (pack (rest))
         Color ) )
      (setq Inf (surfstruct Sur))             # Get the SDL_SURFACE structure
      (glBindTexture GL_TEXTURE_2D (; *Window ftex))
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glTexEnvi 8960 8704 7681)
      (glTexImage2D GL_TEXTURE_2D 0 GL_RGBA8 (get Inf 'w) (get Inf 'h)
         0 GL_BGRA GL_UNSIGNED_BYTE (get Inf 'pixels) )
      (sdlFreeSurface Sur)
      (glMatrixMode GL_MODELVIEW)
      (glLoadIdentity)
      (glTranslatef (xcor) (ycor) 0)
      (glRotatef (degrees (heading)) 0 0 1.0)
      (glEnable GL_TEXTURE_2D)
      (glBegin GL_QUADS)
      (glTexCoord2f 0 0)
      (glVertex2f 0 0)
      (glTexCoord2f 0 1.0)
      (glVertex2f 0 (* 1.0 (get Inf 'h)))
      (glTexCoord2f 1.0 1.0)
      (glVertex2f (* 1.0 (get Inf 'w)) (* 1.0 (get Inf 'h)))
      (glTexCoord2f 1.0 0)
      (glVertex2f (* 1.0 (get Inf 'w)) 0)
      (glEnd)
      (glDisable GL_TEXTURE_2D)
      (glLoadIdentity)
      (let Down (down?)
         (right 90.0)
         (penup)
         (forward (* 1.0 (get Inf 'h)))
         (left 90.0)
         (and Down (pendown)) ) ) )

# --------------------------------

# Utilities

# Size, SizeX, SizeY, Width = scaled numbers
# LR = left | right (can be omitted)
# Colors = list of color indices (0..15), named colors or RGBA values (0-1.0)
# Gradient = a color index, a named color or a list of RGBA values (0-1.0)
# D = degrees (not scaled)
# R, MinR, MaxR = radius (scaled number)
# Loops = non-scaled number

(to rectangle (SizeX SizeY LR Colors) # LR = turn direction, optional: Colors, one per vertex
   (default LR right)
   (rep 2
      (forward SizeX (++ Colors))
      (LR 90.0)
      (forward SizeY (++ Colors))
      (LR 90.0) ) )

(to square (Size) (rectangle Size Size))

(to solidrectangle (SizeX SizeY)
   (filled (rectangle SizeX SizeY)) )

(to solidsquare (Size) (filled (rectangle Size Size)))

(to gradientrectangle (SizeX SizeY Gradient)
   (filled (rectangle SizeX SizeY NIL
      (list Gradient Gradient (; *Window fore)) ) ) )

(to gradientsquare (Size Gradient)
   (gradientrectangle Size Size Gradient) )
   
(to ngon (Size Sides LR Colors) # LR = turn direction, optional: Colors, one per vertex
   (let A (/ 360.0 Sides)
      (default LR right)
      (rep Sides
         (forward Size (++ Colors))
         (LR A) ) ) )

(to solidngon (Size Sides LR) (filled (ngon Size Sides LR)))

(to triangle (Size LR) (ngon Size 3 LR))

(to solidtriangle (Size LR) (solidngon Size 3 LR))

(to arc (D R LR) # D = degrees (not scaled), R = radius (scaled), LR = turn direction
   (default LR left)
   (let S (/ (*/ 2 pi R 1.0) 360)
      (rep D
         (forward S)
         (LR 1.0) ) ) )

(to centerarc (D R LR)
   (default LR left)
   (penup)
   (forward R)
   (LR 90.0)
   (pendown)
   (forward 0)
   (arc D R LR)
   (LR 270.0)
   (penup)
   (back R) )

(to solidcenterarc (D R LR) (filled (centerarc D R LR)))

(to circle (R LR) (arc 360 R LR))

(to solidcircle (R LR) (filled (circle R LR)))

(to centercircle (R)
   (penup)
   (forward R)
   (left 90.0)
   (pendown)
   (circle R left)
   (penup)
   (right 90.0)
   (back R) )

(to solidcentercircle (R)
   (penup)
   (forward R)
   (left 90.0)
   (pendown)
   (filled (circle R left))
   (penup)
   (right 90.0)
   (back R) )
   
(to spiral (MinR MaxR Loops LR)
   (let D (/ (- MaxR MinR) (* Loops 2))
      (until (> MinR MaxR)
         (arc 180 MinR LR)
         (inc 'MinR D) ) ) )
         
(to dash (N Width)
   (rep N
      (pendown)
      (forward Width)
      (penup)
      (forward Width) ) )

# Define short equivalents for some of the functions

(def 'fw forward)
(def 'bk back)
(def 'rt right)
(def 'lt left)
(def 'pu penup)
(def 'pd pendown)
(def 'st showturtle)
(def 'ht hideturtle)
(def 'pc setpencolor)
(def 'sc setbackground)
(def 'cs clearscreen)

# Debug info
`*Dbg

(mapc '((S) (put S 'doc `(pack (car (file)) "res/commands.html")))
   '(fw bk rt lt pu pd st ht pc sc cs showturtle hideturtle setturtlesize
     icon home left right setheading towards goto jumpto clean clearscreen
     floodfill rep filled label rectangle square solidrectangle
     gradientrectangle gradientsquare ngon solidngon triangle solidtriangle
     arc centerarc solidcenterarc circle solidcircle centercircle
     solidcentercircle spiral dash
     rgba pencolor setpencolor background setbackground tint setpenalpha
     startturtle drawfb endturtle iconfromfile iconfit
     iconfromscreen savefromscreen destroyicon drawturtle setlabelheight
     setlabelfont resizewindow smoothon smoothoff reset penup pendown
     setpenwidth heading degrees down? loop0 loop+ loop- loop? repaint
     xcor ycor pos poly? to showturtle? turtlesize forward back setx sety
     pget pset visible? *Colors penalpha ) )

# In the following "native" calls, according to the Picolisp docs, the 
# argument after the C function name is the return type:
# 'P (long int) is used to return a pointer, 'I returns an
# int, 'S returns a char*, and NIL means "void".

# In the arguments and variables, L- means list, I- integer, S- string,
# P- pointer, B- bool (0=false, 1=true)
# So: "IW"=width (integer), "IH"=height, "IX"=x coord (integer),
# "IY"=y coord (integer), "IFlag"=flags (integer),
# "PRect"=pointer to a SDL_Rect, "PWin"=pointer to a SDL_Window,
# "PSurf"=pointer to a SDL_Surface, "PPxf"=pointer to a SDL_PixelFormat,
# "IPxf"=pixel format (integer),
# "PRen"=pointer to a SDL_Renderer, PTex=pointer to a SDL_Texture

# *SDLEvent is a pointer to a 56 byte struct holding SDL event info.
# It is allocated when calling sdlInitApp... and freed when
# calling sdlQuitApp, so that it can be reused without the need
# to allocate and free it everytime.
# *SDLEventInfo is a mapping of *SDLEvent to a specific event type,
# using "struct". See sdlMap... below to see how it works.
# A basic animation main loop is implemented in sdlEventLoop and
# sdlHandleEvents, which relies on a global symbol *SDLActions to be
# defined by the user, as well as on the symbol *SDLStop.
# *SDLStop set to T means that sdlEventLoop must stop.
# *SDLActions is a quoted list containing one or more lists. The CAR
# of a sublist is a constant identifier for an SDL event type and the
# CDR contains the expressions to be evaluated as a response to the
# event. See below for an example.

(load "@sdl/sdlconst.l" "@sdl/nativearg.l")

# This works under Arch Linux
(def '*SDLlib "libSDL2.so")

#{
   SDL_Surface is:                               Offset from surface address
   Uint32 flags                                 0 (but it is padded to 64bit!)
   SDL_PixelFormat *format                      8
   int w                                        16
   int h                                        20
   int pitch (but it seems to be an Uint16)     24 (but it is padded to 64bit!)
   void *pixels                                 32
   void *userdata                               40
   int locked                                   48
   void *lock_data                              ??
   ...
}#

# surfstruct returns a list of tuples with some information of a surface

(de surfstruct ("PSurf")
   (mapcar '((Name Offset Type)
      (cons Name (struct (+ "PSurf" Offset) Type)) )
      '(flags pxfmt w h pitch pixels)
      (0 8 16 20 24 32)
      '(I P I I I P) ) )

#{
   SDL_PixelFormat is:                          Offset from pixel format address
   Uint32 format                                0 (but padded to 64bit!)
   SDL_Palette *palette                         8
   Uint8 BitsPerPixel                           16
   Uint8 BytesPerPixel                          17
   Uint8 padding[2]                             18 (we ignore it)
   Uint32 Rmask                                 20
   Uint32 Gmask                                 24
   Uint32 Bmask                                 28
   Uint32 Amask                                 32
}#

# pxfmtstruct returns a list of tuples with some information of a pixel format

(de pxfmtstruct ("Pxfmt")
   (mapcar '((Name Offset Type IsUInt)
      (cons Name
            (if IsUInt
               (unsigned (struct (+ "Pxfmt" Offset) Type))
               (struct (+ "Pxfmt" Offset) Type) ) ) )
      '(fmtval palette bpp bypp rmask gmask bmask amask)
      (0 8 16 17 20 24 28 32)
      '(I P B B I I I I)
      '(T NIL NIL NIL T T T T) ) )

# sdlInit returns 0 (success) or x<0 (failure)

(de sdlInit ("IFlag")
    (native `*SDLlib "SDL_Init" 'I "IFlag") )

(de sdlInitSubSystem ("IFlag")
    (native `*SDLlib "SDL_InitSubSystem" 'I "IFlag") )
    
# Alias, it also sets up *SDLEvent, which allows to work with events
# more comfortably, and *SDLStop, for the event loop

(de sdlInitApp ("IFlag")
    (default "IFlag" `SDL_INIT_EVERYTHING)
    (cond
        ((=0 (sdlInit "IFlag"))
            (setq *SDLEvent (%@ "malloc" 'P `SDL_EVENT_STRUCT_SIZE))
            (off *SDLStop) )
        (T
            (quit (sdlGetError) 'sdlInitApp)
            (sdlQuitApp)
            (bye) ) ) )

# Before calling sdlQuit you must destroy every surface that
# you may have created manually (use sdlFreeSurface for that), and/or
# send the destroy> method to all +Surface and +Sprite objects you may have created.

(de sdlQuit () (native `*SDLlib "SDL_Quit"))

(de sdlQuitSubSystem ("IFlag")
    (native `*SDLlib "SDL_QuitSubSystem" NIL "IFlag") )

# Alias: Call this if you started SDL with sdlInitApp

(de sdlQuitApp ()
    (sdlQuit)
    (when *SDLEvent (%@ "free" NIL *SDLEvent))
    (on *SDLStop) )

(de sdlWasInit ("IFlag")
    (native `*SDLlib "SDL_WasInit" 'I "IFlag") )
    
# Hint arguments and their values are strings

(de sdlSetHintWithPriority ("SName" "SValue" "IPrior")
    (native `*SDLlib "SDL_SetHintWithPriority" 'I "SName" "SValue" "IPrior") )
    
(de sdlSetHint ("SName" "SValue")
    (native `*SDLlib "SDL_SetHint" 'I "SName" "SValue") )
    
(de sdlGetHint ("SName") (native `*SDLlib "SDL_GetHint" 'S "SName"))

(de sdlGetHintBoolean ("SName" "IDeflt")
    (native `*SDLlib "SDL_GetHintBoolean" 'I "SName" "IDeflt") )

# To install a hint callback, use:
# (sdlAddHintCallback '(("PData" "SName" SOldValue SNewValue) ...))
# Most probably, you'll want to use Picolisp data within your
# callback. Therefore you mustn't pass a pointer to any user data,
# but you are free to do so if you wish.
# The pointer to the callback will be returned, so you can use
# it with sdlDelHintCallback if you need it.

(de sdlAddHintCallback ("SName" "Fun" "PData")
    (default "PData" 0)
    (let R (lisp 'sdlAddHintCallback "Fun")
        (native `*SDLlib "SDL_AddHintCallback" NIL R "PData") ) )
    
(de sdlDelHintCallback ("SName" "PFun" "PData")
    (default "PData" 0)
    (native `*SDLlib "SDL_DelHintCallback" NIL "PFun" "PData") )
    
(de sdlClearHints () (native `*SDLlib "SDL_ClearHints"))

# Get version info

(de sdlGetVersion ()
    (use V
       (native `*SDLlib "SDL_GetVersion" NIL '(V (3 B . 3)))
       V ) )

(de sdlGetRevision () (native `*SDLlib "SDL_GetRevision" 'S))

(de sdlGetRevisionNumber () (native `*SDLlib "SDL_GetRevisionNumber" 'I))

# Video functions

# sdlGetDesktopDisplayMode returns a list of key-value pairs that corresponds
# to an SDL_DisplayMode structure

(de sdlGetDesktopDisplayMode (IIndex)
    (default IIndex 0)
    (use M
        (and
            (=0 (native `*SDLlib "SDL_GetDesktopDisplayMode" 'I IIndex
                    '(M (28 I I I I P) . 0) ) )
            (mapcar cons '(fmtval w h rate data) M) ) ) )

(de sdlCreateWindow ("STitle" "IX" "IY" "IW" "IH" "IFlag")
    (native `*SDLlib "SDL_CreateWindow" 'P "STitle" "IX" "IY" "IW" "IH" "IFlag") )

# Alias for creating a window with some sane defaults, but
# you can also add flags, such as SDL_WINDOW_OPENGL (in sdlconst.l)

(de sdlCreateSimpleWindow ("STitle" "IW" "IH" "IFlag")
    (default "IFlag" 0)
    (let (Pos_undefined (hex "1fff0000")
          "PWin" (sdlCreateWindow "STitle" Pos_undefined Pos_undefined
             "IW" "IH" (| SDL_WINDOW_SHOWN "IFlag") ) )
        "PWin" ) ) # Returns a pointer to a new window

(de sdlGetWindowSize ("PWin")
    (use (IW IH)
       (native `*SDLlib "SDL_GetWindowSize" NIL "PWin"
           '(IW (4 . I)) '(IH (4 . I)) )
       (list IW IH) ) ) # Returns the width and height of the window in a list

(de sdlSetWindowSize @
    (pass native `*SDLlib "SDL_SetWindowSize" NIL) )

(de sdlGetWindowBordersSize ("PWin")
    (use (IT IL IB IR)
       (when (n0 (native `*SDLlib "SDL_GetWindowBordersSize"
           'I "PWin" '(IT (4 . I)) '(IL (4 . I)) '(IB (4 . I))
           '(IR (4 . I)) ) )
           (list IT IL IB IR) ) ) ) # Returns the dimensions (top, left, bottom, 
                                    # right) of its border in a list

# All these return nothing

(de sdlSetWindowMinimumSize ("PWin" "IW" "IH")
    (native `*SDLlib "SDL_SetWindowMinimumSize" NIL "PWin" "IW" "IH") )

(de sdlSetWindowMaximumSize ("PWin" "IW" "IH")
    (native `*SDLlib "SDL_SetWindowMaximumSize" NIL "PWin" "IW" "IH") )

# Bool values can be 1 or SDL_TRUE (true) or 0 or SDL_FALSE (false)

(de sdlSetWindowBordered ("PWin" "B")
    (native `*SDLlib "SDL_SetWindowBordered" NIL "PWin" "B") )

(de sdlSetWindowResizable ("PWin" "B")
    (native `*SDLlib "SDL_SetWindowResizable" NIL "PWin" "B") )

(de sdlShowWindow ("PWin")
    (native `*SDLlib "SDL_ShowWindow" NIL "PWin") )

(de sdlHideWindow ("PWin")
    (native `*SDLlib "SDL_HideWindow" NIL "PWin") )

(de sdlRaiseWindow ("PWin")
    (native `*SDLlib "SDL_RaiseWindow" NIL "PWin") )

(de sdlMaximizeWindow ("PWin")
    (native `*SDLlib "SDL_MaximizeWindow" NIL "PWin") )

(de sdlMinimizeWindow ("PWin")
    (native `*SDLlib "SDL_MinimizeWindow" NIL "PWin") )

(de sdlRestoreWindow ("PWin")
    (native `*SDLlib "SDL_RestoreWindow" NIL "PWin") )

(de sdlSetWindowFullscreen ("PWin" "IFlag")
    (native `*SDLlib "SDL_SetWindowFullscreen" 'I "PWin" "IFlag") )
    # returns 0 (success) or x<0 (failure)

(de sdlGetWindowSurface ("PWin")
    (native `*SDLlib "SDL_GetWindowSurface" 'P "PWin" ) )
    # returns a pointer to a SDL_Surface

(de sdlUpdateWindowSurface ("PWin")
    (native `*SDLlib "SDL_UpdateWindowSurface" 'I "PWin") )
    # returns 0 (success) or x<0 (failure)

(de sdlDestroyWindow ("PWin")
    (native `*SDLlib "SDL_DestroyWindow" NIL "PWin") )

(de sdlDelay (I)    # I=milliseconds
    (native `*SDLlib "SDL_Delay" NIL I) )

(de sdlGetError ()
    (native `*SDLlib "SDL_GetError" 'S ) )
    # returns a string

# Error functions

(de sdlSetError "Args"
    (apply native "Args" `*SDLlib "SDL_SetError" 'I) )
    
(de sdlClearError () (native `*SDLlib "SDL_ClearError"))

# Surface functions

(de sdlGetWindowPixelFormat ("PWin")
    (native `*SDLlib "SDL_GetWindowPixelFormat" 'I "PWin") )
    # returns an integer representing the pixel format

(de sdlGetPixelFormatName ("IPxf")
    (native `*SDLlib "SDL_GetPixelFormatName" 'S "IPxf") )
    # returns a string

(de sdlGetRGBA ("IPxl" "PPxf")
    (use (R G B A)
       (native `*SDLlib "SDL_GetRGBA" NIL '(R (1 B . 1)) '(G (1 B . 1))
           '(B (1 B . 1)) '(A (1 B . 1)) )
       (list R G B A) ) )
       
(de sdlMapRGB ("PPxf" "IR" "IG" "IB")   # IR=red, IG=green, IB=blue (0-255)
    (native `*SDLlib "SDL_MapRGB" 'I "PPxf" "IR" "IG" "IB") )
    # return a color as an integer
    
(de sdlMapRGBA ("PPxf" "IR" "IG" "IB" "IA")   # IR=red, IG=green, IB=blue (0-255)
    (native `*SDLlib "SDL_MapRGBA" 'I "PPxf" "IR" "IG" "IB" "IA") )
    # return a color as an integer
    
# A workaround for C language "SDL_MapRGB". Use this when Format is a number
# instead of a pointer.

(de sdlMapRGBAlloc ("IPxf" "IR" "IG" "IB")
    (let
        (PPxf (native `*SDLlib "SDL_AllocFormat" 'P "IPxf")
         IC  (native `*SDLlib "SDL_MapRGB" 'I PPxf "IR" "IG" "IB") )
        (native `*SDLlib "SDL_FreeFormat" NIL PPxf)
        IC ) )  # returns the color as an integer

(de sdlFillRect ("PSurf" "PRect" "IColor")
    (native `*SDLlib "SDL_FillRect" 'I "PSurf" "PRect" "IColor") )
    
# Alias: sdlFillSurface passes 0 (NULL) to SDL_FillRect, so that it fills
# the entire surface

(de sdlFillSurface ("PSurf" "IColor")
    (native `*SDLlib "SDL_FillRect" 'I "PSurf" 0 "IColor") )
    # returns 0 (success) or x<0 (failure)

# Alias for passing R G B values to sdlFillSurface

(de sdlFillSurfaceRGB ("PSurf" "IR" "IG" "IB")
    (sdlFillSurface "PSurf"
        (sdlMapRGB (sdlGetSurfacePixelFormat "PSurf") "IR" "IG" "IB") ) )

# Alias for passing R G B A values to sdlFillSurface
(de sdlFillSurfaceRGBA ("PSurf" "IR" "IG" "IB" "IA")
    (sdlFillSurface "PSurf"
        (sdlMapRGBA (sdlGetSurfacePixelFormat "PSurf") "IR" "IG" "IB" "IA") ) )
        
# sdlCreateRGBSurface ignores the first argument (Flags)
# because it is marked as obsolete in the SDL source code.

(de sdlCreateRGBSurface ("IW" "IH" "IBpp" "IRmask" "IGmask" "IBmask" "IAmask")
    (native `*SDLlib "SDL_CreateRGBSurface" 'P 0 "IW" "IH" "IBpp"
        "IRmask" "IGmask" "IBmask" "IAmask" ) )
    
# sdlCreateRGBSurfaceWithFormat ignores the Flags parameter,
# because it is marked as obsolete in the SDL source code.

(de sdlCreateRGBSurfaceWithFormat ("IW" "IH" "IBpp" "IPxf")
    (native `*SDLlib "SDL_CreateRGBSurfaceWithFormat" 'P
        0 "IW" "IH" "IBpp" "IPxf") )
    # returns a pointer to a new surface

# alternatively to sdlCreateRGBSurfaceWithFormat (which needs SDL2 >= 2.0.5),
# you can use this convenient function that uses the pixel format of the
# desktop screen

(de sdlCreateSurfaceWithDesktopFormat ("IW" "IH")
    (when (sdlGetDesktopDisplayMode)
        (apply sdlCreateRGBSurface (sdlPixelFormatEnumToMasks (; @ fmtval))
            "IW" "IH" ) ) )

(de sdlConvertSurfaceFormat ("PSurf" "IPxf" "IFlag")
    (native `*SDLlib "SDL_ConvertSurfaceFormat" 'P "PSurf" "IPxf" "IFlag") )
    # returns a pointer to a new surface

(de sdlMasksToPixelFormatEnum ("IBpp" "IRmask" "IGmask" "IBmask" "IAmask")
    (native `*SDLlib "SDL_MasksToPixelFormatEnum" 'I
        "IBpp" "IRmask" "IGmask" "IBmask" "IAmask" ) )
        # returns a pixel format as an integer

(de sdlPixelFormatEnumToMasks ("IPxf")
    (use (Bpp Rmask Gmask Bmask Amask)
       (when (n0 (native `*SDLlib "SDL_PixelFormatEnumToMasks"
           'I "IPxf" '(Bpp (4 . I)) '(Rmask (4 . I)) '(Gmask (4 . I))
           '(Bmask (4 . I)) '(Amask (4 . I)) ) )
           (list Bpp Rmask Gmask Bmask Amask) ) ) )
           # returns bits per pixel and masks for red, green, blue and alpha
           # a integers in a list

# Alias: convert a surface to the pixel format of a window,
# return the new surface and discard (free) the old one.

(de sdlReplaceSurfaceFormat ("PSurf" "PWin")
    (let (IPxf (sdlGetWindowPixelFormat "PWin")
          PSurf2 (sdlConvertSurfaceFormat "PSurf" IPxf 0) )
        (ifn PSurf2
            "PSurf"
            (sdlFreeSurface "PSurf")
            PSurf2 ) ) )
    # returns a pointer to either a new or the old surface

(de sdlCreateRGBSurfaceFrom ("PPixels" "IW" "IH" "IDepth" "IPitch"
    "IRMask" "IGMask" "IBMask" "IAMask" )
    (native `*SDLlib "SDL_CreateRGBSurfaceFrom" 'P "PPixels" "IW" "IH" "IDepth"
        "IPitch" "IRMask" "IGMask" "IBMask" "IAMask" ) )

# convenient function for flipping an SDL_Surface horizontally

(de sdlFlipSurfaceVert ("PSurf")
    (let (Inf (surfstruct "PSurf")
          RowLength (; Inf pitch)
          Buf (%@ "malloc" 'P RowLength)
          Top (; Inf pixels)
          Bottom (+ Top (* (dec (; Inf h)) RowLength)) )
         (while (< Top Bottom)
             (%@ "memcpy" 'P Buf Top RowLength)
             (%@ "memcpy" 'P Top Bottom RowLength)
             (%@ "memcpy" 'P Bottom Buf RowLength)
             (inc 'Top RowLength)
             (dec 'Bottom RowLength) )
         (%@ "free" NIL Buf) ) )
         
# A workaround for C language macro "SDL_LoadBMP".
# If "PWin" is set, then the surface will be converted to the
# pixel format of the window W, the converted surface returned,
# and the old surface discarded.

(de sdlRWops ("SFile") (native `*SDLlib "SDL_RWFromFile" 'P "SFile" "rb"))

(de sdlLoadBMP ("SFile" "PWin")
    (use (RW PSurf)
        (setq RW (sdlRWops "SFile"))
        (setq PSurf (native `*SDLlib "SDL_LoadBMP_RW" 'P RW 1))
        (if "PWin"
            (sdlReplaceSurfaceFormat PSurf "PWin")
            PSurf ) ) )

# A workaround for C language macro "SDL_SaveBMP".

(de sdlSaveBMP ("PSurf" "SFile")
    (let RW (sdlRWops "SFile")
      (native `*SDLlib "SDL_SaveBMP_RW" 'I "PSurf" RW 1) ) )
      
# Alias for SDL_UpperBlit. Remember that "PRect"1 and "PRect"2 can be
# 0 (null) so that the whole surface will be used

(de sdlBlitSurface ("PSurf1" "PRect1" "PSurf2" "PRect2")
    (native `*SDLlib "SDL_UpperBlit" 'I "PSurf1" "PRect1" "PSurf2" "PRect2") )
    # returns 0 (success) or x<0 (failure)

# Alias for SDL_UpperBlitScaled

(de sdlBlitScaled ("PSurf1" "PRect1" "PSurf2" "PRect2")
    (native `*SDLlib "SDL_UpperBlitScaled" 'I "PSurf1" "PRect1" "PSurf2" "PRect2") )
        # returns 0 (success) or x<0 (failure)

(de sdlFreeSurface ("PSurf")
    (native `*SDLlib "SDL_FreeSurface" NIL "PSurf") )

(de sdlCreateRenderer ("PWin" "IIndex" "IFlag")
    (native `*SDLlib "SDL_CreateRenderer" 'P "PWin" "IIndex" "IFlag") )
    # returns a pointer to a new renderer

(de sdlGetRenderer ("PWin")
    (native `*SDLlib "SDL_GetRenderer" 'P "PWin") )

(de sdlSetRenderTarget ("PRen" "PTex")
    (native `*SDLlib "SDL_SetRenderTarget" 'P "PRen" "PTex") )
    
#{
typedef struct SDL_RendererInfo
{
    const char *name;           /**< The name of the renderer */
    Uint32 flags;               /**< Supported ::SDL_RendererFlags */
    Uint32 num_texture_formats; /**< The number of available texture formats */
    Uint32 texture_formats[16]; /**< The available texture formats */
    int max_texture_width;      /**< The maximum texture width */
    int max_texture_height;     /**< The maximum texture height */
} SDL_RendererInfo;
}#

(de sdlGetRendererInfo ("PRen")
    (use P
       (ifn (=0 (native `*SDLlib "SDL_GetRendererInfo" 'I "PRen"
                   '(P (88 . (S I I (I . 16) I I))) ) )
           (quit (sdlGetError) 'sdlGetRendererInfo)
           (list
               (cons 'name (++ P))
               (cons 'flags (unsigned (++ P)))
               (cons 'texture_formats
                   (let (NumTex (unsigned (++ P)) TexL (++ P))
                       (make (do NumTex
                           (let TexFmt (unsigned (++ TexL))
                               (link (cons TexFmt
                                           (sdlGetPixelFormatName TexFmt))) ) ) ) ) )
               (cons 'max_texture_width (++ P))
               (cons 'max_texture_height (++ P))) ) ) )

(de sdlQueryTexture ("PTex")
    (use (Format Access Width Height)
       (ifn (=0 (native `*SDLlib "SDL_QueryTexture" 'I "PTex"
                   '(Format (4 . I)) '(Access (4 . I)) '(Width (4 . I))
                   '(Height (4 . I)) ) )
           (quit (sdlGetError) 'sdlQueryTexture)
           (mapcar cons
               '(format access width height)
               (list (unsigned Format) Access Width Height) ) ) ) )
            
(de sdlSetRenderDrawColor ("PRen" "IR" "IG" "IB" "IA")
    (native `*SDLlib "SDL_SetRenderDrawColor" 'I "PRen" "IR" "IG" "IB" "IA") )
    # returns 0 (success) or x<0 (failure)

(de sdlRenderDrawPoint ("PRen" "IX" "IY")
    (native `*SDLlib "SDL_RenderDrawPoint" 'I "PRen" "IX" "IY") )

(de sdlRenderDrawRect @ (pass native `*SDLlib "SDL_RenderDrawRect" 'I))

(de sdlRenderFillRect @ (pass native `*SDLlib "SDL_RenderFillRect" 'I))

(de sdlRenderDrawLine @ (pass native `*SDLlib "SDL_RenderDrawLine" 'I))

(de sdlSetRenderDrawBlendMode @
   (pass native `*SDLlib "SDL_SetRenderDrawBlendMode" 'I) )
   
(de sdlDestroyRenderer ("PRen")
    (native `*SDLlib "SDL_DestroyRenderer" NIL "PRen") )
    # returns 0 (success) or x<0 (failure)

(de sdlRenderClear ("PRen")
    (native `*SDLlib "SDL_RenderClear" 'I "PRen") )
    # returns 0 (success) or x<0 (failure)

(de sdlRenderCopy ("PRen" "PTex" "PRSrc" "PRDst")
    (native `*SDLlib "SDL_RenderCopy" 'I "PRen" "PTex" "PRSrc" "PRDst") )
    # returns 0 (success) or x<0 (failure)

(de sdlRenderCopyEx ("PRen" "PTex" "PRSrc" "PRDst" "IAngle" "PPoint" "IFlip")
    (native `*SDLlib "SDL_RenderCopyEx" 'I "PRen" "PTex" "PRSrc" "PRDst"
        "IAngle" "PPoint" "IFlip" ) )
    # returns 0 (success) or x<0 (failure)
    
(de sdlRenderPresent ("PRen")
    (native `*SDLlib "SDL_RenderPresent" NIL "PRen") )

(de sdlCreateTextureFromSurface ("PRen" "PSurf")
    (native `*SDLlib "SDL_CreateTextureFromSurface" 'P "PRen" "PSurf") )
    # returns a pointer to a new texture

# sdlGetSurfacePixelFormat is equivalent to accessing the "format" field of 
# a SDL_Surface structure in C, which is a pointer to a SDL_PixelFormat
# The structure of a surface is: Uint32 (flags),
# SDL_PixelFormat* (format), ...
# So it's the cadr of the structure (the pointer to the pixel format)
# what we need to extract.

(de sdlGetSurfacePixelFormat ("PSurf") # (cadr (struct "PSurf" '((4 . I) P)))
   (get (surfstruct "PSurf") 'pxfmt) )

# Alias: sdlGetSurfacePixelFormatVal gets the pixel format as an int

(de sdlGetSurfacePixelFormatVal ("PSurf")
    (struct (sdlGetSurfacePixelFormat "PSurf") 'I) )

# Alias: sdlGetSurfaceBpp gets the depth of the pixel format

(de sdlGetSurfaceBpp ("PSurf")
    (caddr (struct (sdlGetSurfacePixelFormat "PSurf") '((4 . I) P B))) )

# Alias: sdlGetSurfaceBitmap gets the width, height, pitch and the pointer
# to the pixels of a surface
# Deprecated. Use surfstruct instead, it works well.
#(de sdlGetSurfaceBitmap ("PSurf")
#    (cddr (struct "PSurf" '((4 . I) P I I I P))) )

(de sdlDestroyTexture ("PTex")
    (native `*SDLlib "SDL_DestroyTexture" NIL "PTex") )

# sdlSetColorKey takes plain RGB values and converts them to a Uint32
# using sdlMapRGB (which needs a pointer to a SDL_PixelFormat),
# where SDL_SetColorKey takes a Uint32 directly.

(de sdlSetColorKey ("PSurf" "BEnabled" "IR" "IG" "IB")
    (let (PPxf (sdlGetSurfacePixelFormat "PSurf")
          IC (sdlMapRGB PPxf "IR" "IG" "IB") )
        (when (=0 (native `*SDLlib "SDL_SetColorKey" 'I "PSurf"
                    "BEnabled" IC ) )
            IC ) ) ) # returns a color as an integer

(de sdlSetSurfaceColorMod ("PSurf" "IR" "IG" "IB")
    (native `*SDLlib "SDL_SetSurfaceColorMod" 'I "PSurf" "IR" "IG" "IB") )
    # returns 0 (success) or x<0 (failure)

(de sdlSetTextureColorMod ("PTex" "IR" "IG" "IB")
    (native `*SDLlib "SDL_SetTextureColorMod" 'I "PTex" "IR" "IG" "IB") )
    # returns 0 (success) or x<0 (failure)

(de sdlSetSurfaceAlphaMod ("PSurf" "IA")
    (native `*SDLlib "SDL_SetSurfaceAlphaMod" 'I "PSurf" "IA") )
    # returns 0 (success) or x<0 (failure)

(de sdlSetTextureAlphaMod ("PTex" "IA")
    (native `*SDLlib "SDL_SetTextureAlphaMod" 'I "PTex" "IA") )
    # returns 0 (success) or x<0 (failure)

(de sdlSetSurfaceBlendMode ("PSurf" "IFlag")
    (native `*SDLlib "SDL_SetSurfaceBlendMode" 'I "PSurf" "IFlag") )
    # returns 0 (success) or x<0 (failure)

(de sdlSetTextureBlendMode ("PTex" "IFlag")
    (native `*SDLlib "SDL_SetTextureBlendMode" 'I "PTex" "IFlag") )
    # returns 0 (success) or x<0 (failure)

(de sdlGetTicks () (native `*SDLlib "SDL_GetTicks" 'I) )
# returns the number of ticks as an integer

(de sdlLockSurface ("PSurf")
    (native `*SDLlib "SDL_LockSurface" 'I "PSurf") )

(de sdlUnlockSurface ("PSurf")
    (native `*SDLlib "SDL_UnlockSurface" NIL "PSurf") )

# Other than SDL_GetKeyboardState, sdlGetKeyboardState doesn't take
# any parameters (SDL2 apps written in C pass NULL most of the times
# anyway). The pointer that sdlGetKeyboardState returns is always valid,
# so this function can be called once and its returned value be used
# throughout the program.

(de sdlGetKeyboardState () (native `*SDLlib "SDL_GetKeyboardState" 'P 0))

# Convenient function: Take the pointer returned by sdlGetKeyboardState
# and return T if the key with symcode Code is pressed.
# Scan codes (SDL_SCANCODE_*) are different from key symbols (SDLK_*).

(de keyDown? ("PKeys" "Code") (=1 (byte (+ "PKeys" "Code"))))

(de sdlSetRelativeMouseMode ("BMode")
   (native `*SDLlib "SDL_SetRelativeMouseMode" 'I "BMode") )

(de sdlWarpMouseInWindow ("PWin" "IX" "IY")
   (native `*SDLlib "SDL_WarpMouseInWindow" NIL "PWin" "IX" "IY") )

# Event functions

# SDL_WaitEvent and SDL_PollEvent return 1 on success, 0 on failure.

# sdlWaitEvent can put the first received event into PEvent, which
# can be *SDLEvent. Otherwise, it just waits until an event is in the
# event queue.

(de sdlWaitEvent ("PEvent")
    (default "PEvent" 0)
    (native `*SDLlib "SDL_WaitEvent" 'I "PEvent") )

# sdlPollEvent puts the event into *SDLEvent.

(de sdlPollEvent ()
    (native `*SDLlib "SDL_PollEvent" 'I *SDLEvent) )

# sdlPushEvent uses the current values put in the structure
# *SDLEvent. It returns 1 (success), 0 (event filtered) or a negative
# number (error). The type of the event must be one of SDL's standard
# events.

(de sdlPushEvent ()
    (native `*SDLlib "SDL_PushEvent" 'I *SDLEvent) )

(de sdlRegisterEvents ("IN")
    (let Q (native `*SDLlib "SDL_RegisterEvents" 'I "IN")
        (if (= (abs Q) (hex "FFFFFFFF")) NIL Q) ) )
        
# sdlGetEventType takes the "type" field of the SDL_Event structure
# returned by sdlPollEvent or sdlWaitEvent.
# "struct" allows to retrieve just the first field a
# structure, which is convenient here.

(de sdlGetEventType () (struct *SDLEvent 'I))
# returns the "type" field of the *SDLEvent struct as an integer

# sdlHandleEvents peeks the type of *SDLEvent and checks it against a
# list of pairs (EventType Action1 Action2 ...),
# which you'll have defined in the global *SDLActions.
# If EventType matches *SDLEvent, its action is run.
# EventType is a number (see SDL header files and "sdlconst.l"),
# Action is a Prog (list of executable expressions).
# If no EventType in *SDLActions matches the car of *SDLEvent,
# that event will be ignored.
# If *SDLActions is NIL (undefined), there'll be no response to
# any events.

(de sdlHandleEvents () (run (cdr (assoc (sdlGetEventType) *SDLActions))) )

# A SDL_Event has different structures depending on the event
# type. So it has the structure that *SDLEvent points to.
# The following variables and functions take care of that.
# Depending on the event type, you can call the appropriate
# sdlMapX function to convert the SDL_Event structure to a Lisp list
# which you can use to retrieve event data.
# You can use "struct" over *SDLEvent to make your own events.

(def '*SDLMouseButtonEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          I     # 4. which
          B     # 5. button
          B     # 6. state
          B     # 7. clicks
          B     # 8. padding
          I     # 9. x coordinate
          I     # 10. y coordinate
          ) )
          
(de sdlMapMouseButtonEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLMouseButtonEventStruct)) )

(def '*SDLMouseMotionEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          I     # 4. which
          I     # 5. state
          I     # 6. x coordinate
          I     # 7. y coordinate
          I     # 8. relative motion in x direction
          I     # 9. relative motion in y direction
          ) )
          
(de sdlMapMouseMotionEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLMouseMotionEventStruct)) )

(def '*SDLWindowEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          B     # 4. event (SDL_WindowEventID)
          B     # 5. padding1
          B     # 6. padding2
          B     # 7. padding3
          I     # 8. data1
          I     # 9. data2
          ) )
          
(de sdlMapWindowEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLWindowEventStruct)) )

(def '*SDLKeyboardEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          B     # 4. state
          B     # 5. repeat
          B     # 6. padding2
          B     # 7. padding3
          I     # 8. keysym.scancode
          I     # 9. keysym.sym
          I     # 10. keysym.mod
          ) )
          
(de sdlMapKeyboardEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLKeyboardEventStruct)) )

(def '*SDLTextEditingEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          (B . 32) # 4. text
          I     # 5. start
          I     # 6. length
          ) )

(de sdlMapTextEditingEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLTextEditingEventStruct)) )

(def '*SDLTextInputEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          (B . 32) # 4. text
          ) )

(de sdlMapTextInputEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLTextInputEventStruct)) )

(def '*SDLMouseWheelEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          I     # 4. which
          I     # 5. amount of x scroll
          I     # 6. amount of y scroll
          I     # 7. direction
          ) )

(de sdlMapMouseWheelEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLMouseWheelEventStruct)) )

(def '*SDLAudioDeviceEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. which
          B     # 4. iscapture
          ) )

(de sdlMapAudioDeviceEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLAudioDeviceEventStruct)) )

(def '*SDLTouchFingerEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. touchId
          I     # 4. fingerId
          -1.0  # 5. x
          -1.0  # 6. y
          -1.0  # 7. dx
          -1.0  # 8. dy
          -1.0  # 9. pressure
          ) )

(de sdlMapTouchFingerEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLTouchFingerEventStruct)) )

(def '*SDLUserEventStruct
        '(I     # 1. type
          I     # 2. timestamp
          I     # 3. windowID
          I     # 4. code
          P     # 5. pointer to data1
          P     # 6. pointer to data2
          ) )
          
(de sdlMapUserEvent ()
    (setq *SDLEventInfo (struct *SDLEvent *SDLUserEventStruct)) )

# Implementation of a main event loop, running continuously and
# handling all events occurred between two loops.
# If *SDLStop is not NIL, the loop ends.
# sdlInitApp sets *SDLStop to NIL.
# sdlQuitApp sets *SDLStop to T.

(de sdlEventLoop ("Prog")
    # Main application loop:
    (until *SDLStop
        # Take events from the event queue, until it is empty
        # (i.e. sdlPollEvent returns 0)
        (until (=0 (sdlPollEvent))
            # Respond to an event.
            # sdlHandleEvents depends on the user
            # having defined *SDLActions.
            (sdlHandleEvents) )
        # Animate a frame.
        (run "Prog") ) )

# Implementation of a main event loop that waits for events, so it
# does nothing until an event is pushed into the event queue.
# If *SDLStop is not NIL, the loop ends.
# sdlInitApp sets *SDLStop to NIL.
# sdlQuitApp sets *SDLStop to T.

(de sdlWaitLoop ("Prog")
    # Main application loop:
    (until *SDLStop
        (case (sdlWaitEvent)
            (1
                # Take events from the event queue, until it is empty
                # (i.e. sdlPollEvent returns 0)
                (until (=0 (sdlPollEvent))
                    # Respond to an event.
                    # sdlHandleEvents depends on the user
                    # having defined *SDLActions.
                    (sdlHandleEvents) )
                # Animate a frame.
                (run "Prog") )
            (0
                (quit (sdlGetError) 'sdlWaitLoop)
                (on *SDLStop) ) ) ) )

NIL

Example of *SDLActions (from Picoblocks' game.l):

(def '*SDLActions
    (quote
        (`SDL_QUIT
            (on *SDLStop) )
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            # The 8th field of *SDLEventInfo holds now the key code
            # 41 = Escape key
            (case (get *SDLEventInfo 8)
                (41 (if (= "GMode" start)
                        (on *SDLStop)
                        (mixHaltMusic)
                        (mixRewindMusic)
                        (setq "GMode" start) ) )
                (T  (when (= "GMode" start)
                        (levelDefaults 1) ) ) ) )
        (`SDL_MOUSEBUTTONDOWN
            (sdlMapMouseButtonEvent)
            # the 9th and 10th fields of *SDLEventInfo hold the
            # X and Y position of the mouse
            (let ((X Y) (nth *SDLEventInfo 9))
                (pickDrop X Y) ) )
        (`SDL_MOUSEMOTION
            (sdlMapMouseMotionEvent)
            # the 6th and 7th fields of *SDLEventInfo hold the
            # X and Y position of the mouse
            (let ((X Y) (nth *SDLEventInfo 6))
                (setq "MouseX" X "MouseY" Y) ) ) ) )

# Always load "sdl.l" before this file!!
   
# A basic, convenient class for surfaces. It handles all pointer stuff
# nicely.

(class +Surface)

    # If you provide a +Window ("OWin" argument), the surface will be
    # converted to the pixel format of that window.
    # Otherwise ("OWin" is NIL), you must provide a
    # pixel format (integer, not pointer).

    (dm T ("IW" "IH" "OWin" "IPxf")
        (when "OWin"
            (setq "IPxf" (sdlGetWindowPixelFormat (get "OWin" 'win))) )
        (let (Mask (sdlPixelFormatEnumToMasks "IPxf")
              S (apply sdlCreateRGBSurface Mask "IW" "IH") )
            (if (=0 S)
                (quit (sdlGetError) "+Surface constructor")
                (=: surf S)
                (=: pxf "IPxf")
                # dest and src are malloc'ed SDL_Rect structures.
                # They are allocated once because they can be reused multiple
                # times and their pointers passed to SDL blit functions
                # using "native".
                # By manipulating the memory structures, we reduce the overhead
                # of allocating and freeing them every time we must pass them
                # to C functions using "native".
                # Coordinates (x, y, width and height) to blit onto
                (=: dest (%@ "malloc" 'P 16))
                # Piece of the surface to blit
                (=: src (%@ "malloc" 'P 16)) ) ) )

    # Resizes a surface creating a new surface of the desired
    # size and discarding the old surface.
    # You can provide a +Window (OWin) or a pixel format (integer).
    # If your source surface has an alpha channel and you want
    # to preserve it, be sure to choose the same destination
    # pixel format, e.g. by using
    # (sdlGetSurfacePixelFormatVal (: surf))
    
    (dm resize> ("IW" "IH" "OWin" "IPxf")
        (let S (new '(+Surface) "IW" "IH" "OWin" "IPxf")
            (when (get S 'surf)
                (setdest> S 0 0 "IW" "IH")
                (sdlBlitScaled (: surf) 0 (get S 'surf) (get S 'dest))
                (sdlFreeSurface (: surf))
                (=: surf (get S 'surf))
                (put S 'src (%@ "free" NIL (get S 'src)))
                (put S 'dest (%@ "free" NIL (get S 'dest))) ) ) )

    # Sets the values of the destination surface's rect.
    # Returns the values

    (dm setdest> ("IX" "IY" "IW" "IH")
        (struct (: dest)
            '(I I I I)
            (cons "IX" 4) (cons "IY" 4) (cons "IW" 4) (cons "IH" 4) ) )

    # Sets the values of the source surface's rect.
    # Returns the values

    (dm setsrc> ("IX" "IY" "IW" "IH")
        (struct (: src)
            '(I I I I)
            (cons "IX" 4) (cons "IY" 4) (cons "IW" 4) (cons "IH" 4) ) )
  
    # X and Y position on the destination's surface

    (dm pos> () (struct (: dest) '(I I)))

    # Increments the destination surface's X and Y values (the position
    # the source surface will be blitted or copied to).

    (dm move> ("IX" "IY")
        (let ((X Y) (pos> This))
            (xy> This (inc 'X "IX") (inc 'Y "IY")) ) )
    
    # sets the source surface's position to X and Y.
    
    (dm srcxy> ("IX" "IY")
        (struct (: src) '(I I)
            (cons "IX" 4) (cons "IY" 4) ) )

    # sets the destination surface's position to X and Y.
    
    (dm xy> ("IX" "IY")
        (struct (: dest) '(I I)
            (cons "IX" 4) (cons "IY" 4) ) )
    
    # don't forget to destroy> a +Surface so that src and dest
    # are freed. Otherwise you'll have a memory leak
    
    (dm destroy> ()
        (if (: istexture)
            (sdlDestroyTexture (: surf))
            (sdlFreeSurface (: surf)) )
        (%@ "free" NIL (: src))
        (%@ "free" NIL (: dest)))

    (dm colorkey> ("BEnabled" "IR" "IG" "IB")
        (=: colorkey (sdlSetColorKey (: surf) "BEnabled" "IR" "IG" "IB"))
        (=: hascolorkey "BEnabled") )

    # Converts the "src" and "dest" memory structures to a list of integers.

    (dm getsrc> () (struct (: src) '(I I I I)))

    (dm getdest> () (struct (: dest) '(I I I I)))

    # Alias to sdlBlitSurface.

    (dm blitto> ("PSurf")
        (sdlBlitSurface (: surf) (: src) "PSurf" (: dest)) )
    
    # Alias to sdlBlitSurface, with source rect set to 0.
    
    (dm blitallto> ("PSurf")
        (sdlBlitSurface (: surf) 0 "PSurf" (: dest) ) )

    # Converts the surface into a SDL_Texture. If you need to apply
    # a color key, call colorkey> before totexture> !

    (dm totexture> ("OWin")   # OWin is a +RenderWindow object
        (ifn (isa '+RenderWindow "OWin")
            (quit "+Surface totexture> method: not a +RenderWindow" "OWin")
            (let S (sdlCreateTextureFromSurface
                    (get "OWin" 'renderer) (: surf) )
                (if (=0 S)
                    (quit (sdlGetError) "+Surface totexture> method")
                    (sdlFreeSurface (: surf))
                    (=: surf S)
                    (=: istexture T)
                    (=: pxf NIL) ) ) ) )

    # Alias to sdlRenderCopy.

    (dm copyto> ("PRen") (sdlRenderCopy "PRen" (: surf) (: src) (: dest)))
    
    # Alias to sdlRenderCopy, with source rect set to 0.
    
    (dm copyallto> ("PRen") (sdlRenderCopy "PRen" (: surf) 0 (: dest)))

    # Sets color modulation and uses it to color the texture.

    (dm modulate> ("IR" "IG" "IB")
        (if (: istexture)
            (sdlSetTextureColorMod (: surf) "IR" "IG" "IB")
            (sdlSetSurfaceColorMod (: surf) "IR" "IG" "IB") )
        (=: colormod (list "IR" "IG" "IB")) )

    # Sets alpha modulation.

    (dm alpha> ("IA")
        (if (: istexture)
            (sdlSetTextureAlphaMod (: surf) "IA")
            (sdlSetSurfaceAlphaMod (: surf) "IA") )
        (=: alphamod "IA") )

    # Sets blending mode.

    (dm blend> ("IFlag")
        (if (: istexture)
            (sdlSetTextureBlendMode (: surf) "IFlag")
            (sdlSetSurfaceBlendMode (: surf) "IFlag") )
        (=: blend "IFlag") )

    # We could define a function wrapping SDL_HasIntersection,
    # but since it compares two SDL_Rects, that would make sense
    # only when working with surfaces anyway.

    (dm collision?> ("OSurf")
        (=1 (native `*SDLlib "SDL_HasIntersection" 'I
                (: dest) (get "OSurf" 'dest))) )
          
# Convenient class for 2D sprites. It inherits from +Surface, but has
# its own constructor method, which allows to load a .bmp file.

(class +Sprite +Surface)

    # If you supply a +Window, the loaded bitmap will be converted into the 
    # pixel format of the window and the original bitmap will be freed
    # See sdlLoadBMP.

    (dm T ("SFile" "Sid" "OWin")
        (let S (if (isa '+Window "OWin")
                   (sdlLoadBMP "SFile" (get "OWin" 'win))
                   (sdlLoadBMP "SFile") )
            (if (=0 S)
                (quit (sdlGetError) (pack "SFile" " in +Sprite constructor"))
                (=: surf S)
                (let PPxf (sdlGetSurfacePixelFormat (: surf))
                    (=: pxf (struct PPxf 'I)) )
                (=: sprid "Sid")
                # coordinates to blit onto
                (=: dest (%@ "malloc" 'P 16))
                # piece of the surface to blit
                (=: src (%@ "malloc" 'P 16)) ) ) )

# Convenient class for SDL windows. It handles all pointer stuff
# nicely.

(class +Window)

    (dm T ("STitle" "IW" "IH" "IFlag")
        (let PWin (sdlCreateSimpleWindow "STitle" "IW" "IH" "IFlag")
            (if (=0 PWin)
                (quit (sdlGetError) "+Window constructor")
                (=: surf (sdlGetWindowSurface PWin))
                (=: pxf (sdlGetWindowPixelFormat PWin))
                (=: pxfname (sdlGetPixelFormatName (: pxf)))
                (=: win PWin)
                (=: size (sdlGetWindowSize PWin))
                (=: ticks (sdlGetTicks))
                (=: rate 0) ) ) ) # milliseconds between frames

    (dm destroy> () (sdlDestroyWindow (: win)))

    # updates the window immediately

    (dm update> () (sdlUpdateWindowSurface (: win)))

    # I = frames per second. Use this to set the desired frame rate.
    
    (dm framerate> ("IFps") (=: rate (/ 1000 "IFps")))

    # updates the window at most frequently as stated with the framerate> method.

    (dm refresh> ()
        (let (Ti (sdlGetTicks) La (- Ti (: ticks)))
            (when (< La (: rate)) (sdlDelay (- (: rate) La)))
            (=: ticks Ti) )
        (sdlUpdateWindowSurface (: win)) )

# Convenient class for windows with a renderer.

(class +RenderWindow +Window)

    (dm T ("STitle" "IW" "IH" "WFlag" "RFlag")
        (default "RFlag" (| SDL_RENDERER_ACCELERATED SDL_RENDERER_PRESENTVSYNC))
        (super "STitle" "IW" "IH" "WFlag")
        (when (: win)
	        (=: renderer (sdlGetRenderer (: win)))
            (when (=0 (: renderer))
                (=: renderer (sdlCreateRenderer (: win) -1 "RFlag") )
                (if (=0 (: renderer))
                    (quit (sdlGetError) "+RenderWindow constructor")
                    (=: surf NIL)
                    (=: pxf NIL)
                    (=: pxfname NIL) ) ) ) )

    (dm rendercolor> ("IR" "IG" "IB" "IA")
        (=: color (list "IR" "IG" "IB" "IA"))
        (sdlSetRenderDrawColor (: renderer) "IR" "IG" "IB" "IA") )

    (dm destroy> ()
        (sdlDestroyRenderer (: renderer))
        (super) )

    (dm clear> () (sdlRenderClear (: renderer)))

    (dm present> () (sdlRenderPresent (: renderer)))
    
    (dm refresh> ()
        (let (Ti (sdlGetTicks) La (- Ti (: ticks)))
            (when (< La (: rate)) (sdlDelay (- (: rate) La)))
            (=: ticks Ti) )
        (sdlRenderPresent (: renderer)) )
        
# Collision detection routines, using the method collision?> of +Surface.

# One surface against others

(de detect (OSurf OSurfs) (filter '((X) (collision?> OSurf X)) OSurfs))

# All surfaces against one another

(de detectall ("OSurfs")
    (let O NIL
        (make
            (while (setq O (++ "OSurfs"))
                (when (detect O "OSurfs") (link (cons O @))) ) ) ) )

# This file depends on opengl3.l

# Class for managing uniform buffer objects

(class +UBO)

   # create a new uniform buffer object
   # input:
   #  Name = name of the uniform block in the shader(s)
   #  Size = bytes to allocate
   
   (dm T (Name Size)
      (let (UBO (glGenBuffers 1)
            Buf (%@ "malloc" 'P Size) )
         (if (nand (gt0 UBO) (gt0 Buf))
            (quit (pack "Could not make UBO " Name))
            (glBindBuffer `GL_UNIFORM_BUFFER UBO)
            (glBufferDataP `GL_UNIFORM_BUFFER Size 0 `GL_DYNAMIC_DRAW)
            (=: block Name)
            (=: id UBO)
            (=: size Size)
            (=: buffer Buf) ) ) )

   # bind a UBO to a shader program
   # input:
   #  Shader = a +Shader
   #  IPoint = the binding point
               
   (dm bindprog> (Shader IPoint)
      (let Index (glGetUniformBlockIndex (; Shader shprog) (: block))
         (glUniformBlockBinding (; Shader shprog) Index IPoint)
         (glBindBufferBase `GL_UNIFORM_BUFFER IPoint (: id))
         (push (:: bind) (list (; Shader shprog) Index IPoint)) ) )

   # set values in a (uniform) buffer (object)
   # input:
   #  Std140 = an std140 specification
   #  @ = one or more evaluated arguments in the form
   #     - (field values), for single std140 buffer updates
   #     - (elem field values), for std140 array buffer updates

   (dm update> (Std140 . @)
      (use C
         (while (setq C (next))
            (if (num? (car C))
               # buffer is an array, CAR points to the nth element
               (struct (+ (: buffer) (get (cdr Std140) @ (cadr C))) NIL (caddr C))
               # buffer is not an array, CAR points to the field's name
               (struct (+ (: buffer) (get (cdr Std140) (car C))) NIL (cadr C)) ) )
         # upload the whole buffer
         (glBufferSubData `GL_UNIFORM_BUFFER 0 (: size) (: buffer)) ) )

   (dm activate> () (glBindBuffer `GL_UNIFORM_BUFFER (: id)))
   
    # destroy> takes care of freeing the space allocated for
    # matrices
    
    (dm destroy> () (%@ "free" NIL (: buffer)))

# Convenient functions: std140 and std140-array
# calculate offsets of a uniform block with an std140 layout
# for use in UBOs (uniform buffer objects)
# supports only:
#  scalar types: "scalar" stands for all 4-byte single
#     data types in GLSL: float, int, uint, bool
#  vec2, vec3 and vec4 (elements are floats)
#  arrays (not only the array, but also its elements
#     are 16-byte aligned)
#  matrices (as an array of vec4)
# input: lists of (type name [array size])
#  for matrices, use array and 2, 3, 4 for mat2, mat3, mat4
# output: a list whose CAR is the length of the block
#  and whose CDR are cons pairs of uniform name + offset in bytes

(de std140 L
   (let (P 0 E NIL
      Bases
      '((scalar 4 4)       # type, base offset, base alignment
        (vec2 8 8)
        (vec3 12 16)
        (vec4 16 16)
        (array 16 16) ) )
      (make
         (for X L
            # CAR X = type (scalar, vec2, vec3, vec4, array)
            (setq E (cdr (assoc (car X) Bases)))
            # calculate the offset of the next element
            (or
               # start of the structure
               (=0 P)
               # offset corresponds to the base alignment
               (=0 (% (+ P (cadr E)) (cadr E)))
               # correct alignment
               (inc 'P (- 16 (% P 16))) )
            # CADR X = name
            (link (cons (cadr X) P))
            (case (car X) 
               (array
                  # CADDR X = number of array elements
                  (inc 'P (* (car E) (caddr X))) )
               (T
                  (inc 'P (car E)) ) ) )
         # pad at the end of the structure if necessary
         (or (=0 (% P 16)) (inc 'P (- 16 (% P 16))))
         # put total size in front of the list
         (yoke P) ) ) )

# use this when the uniform block is an array of structures with std140
# layout.
# input: N = number of elements, S = result of std140
# output: CAR = total size of the block, CDR = list of results of std140
# with increasing offsets, so you can access them by index, e.g.
# (car (cdr (std140-array ....))) is the first element of the block

(de std140-array (N S)
   (let P 0
      (make
         (do N
            (chain
               (list
                  (mapcar '((V)
                     (cons (car V) (+ (cdr V) P)) )
                     (cdr S) ) ) )
            (inc 'P (car S)) )
            (yoke P) ) ) )

# example: for the uniform block
#  layout (std140) uniform Data
#  {
#     vec3 vertex;
#     mat4 modelview;
#     float float1;
#     bool bool1;
#  }
#  you run
#     (std140 (vec3 vertex) (array modelview 4) (scalar float1) (scalar bool1))
#  which returns
#     (96 (vertex . 0) (modelview . 16) (float1 . 80) (bool1 . 84))
#  where 96 is the size of the block in bytes (so you can allocate the memory
#  needed), and the cons pairs correspond to the structure members and
#  their offsets in bytes, as OpenGL needs them (so you can access them
#  with "struct" and modify their contents), and that way you don't have
#  to ask OpenGL for the offset of every element of the block

#{
   Another example:
      GLSL declaration
         struct SLightSource {
            vec4 color;
            vec3 position;
            vec3 lightDir;
            float lightDirMixFactor;
            float cosTheta;
            float cosPhi;
            float constantAttenuation;
            float linearAttenuation;
            float quadraticAttenuation;
         }
         
         layout (std140) uniform LightSourcesBlock {
            SLightSource lightSources[32];
         }
      
      translates to
         (setq SLightSource (std140
            (vec4 color)
            (vec3 position)
            (vec3 lightDir)
            (scalar lightDirMixFactor)
            (scalar cosTheta)
            (scalar cosPhi)
            (scalar constantAttenuation)
            (scalar linearAttenuation)
            (scalar quadraticAttenuation) ) )
         
         (setq LightSourcesBlock (std140-array 32 SLightSource))
      
      the VAL of SLightSource is
         (80 (color . 0) (position . 16) (lightDir . 32)
             (lightDirMixFactor . 44) (cosTheta . 48) (cosPhi . 52)
             (constantAttenuation . 56) (linearAttenuation . 60)
             (quadraticAttenuation . 64) )
      
      the VAL of LightSourcesBlock is
         (2560 ((color . 0) (position . 16) (lightDir . 32)
         (lightDirMixFactor . 44) (cosTheta . 48) (cosPhi . 52)
         (constantAttenuation . 56) (linearAttenuation . 60)
         (quadraticAttenuation . 64))
         ((color . 80) (position . 96) (lightDir . 112)
         (lightDirMixFactor . 124) (cosTheta . 128) (cosPhi . 132)
         (constantAttenuation . 136) (linearAttenuation . 140)
         (quadraticAttenuation . 144))
         ((color . 160) (position . 176) (lightDir . 192)
         (lightDirMixFactor . 204) (cosTheta . 208) (cosPhi . 212)
         (constantAttenuation . 216) (linearAttenuation . 220)
         (quadraticAttenuation . 224)) etc. etc. )

      you get the "lightDir" offset of the 3rd light source with
         (; (cdr LightSourcesBlock) 3 lightDir)
         -> 192
      
      you allocate a memory block for LightSourcesBlock within a +Shader with
         (setq UBO (new '(+UBO) "LightSourcesBlock" (car LightSourcesBlock))
      
      you bind the UBO to a shader program's binding point 1 with
         (bindprog> UBO Shader 1)
         
      you fill the "lightDir" of the 3rd light source with the values
      (2.0 2.0 1.0) with the expression
         (update> UBO LightSourcesBlock
            '(3 lightDir (-1.0 2.0 2.0 1.0)) ) or with
         (update> UBO LightSourcesBlock
            (list 3 'lightDir (floats 1.0 (2.0 2.0 1.0))) )
      
      you bind the UBO with
      (activate> UBO)
      
      you destroy the buffer with
      (destroy> UBO)
      
      "floats" can be found in "@sdl/nativearg.l".
}#


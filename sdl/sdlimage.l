# Bindings for libSDL2_image.so
# Always load "sdl.l" and "sdlutil.l" before this file!!

# This works under Arch Linux
(def '*SDLimg "libSDL2_image.so")

(def 'IMG_INIT_JPG 1)
(def 'IMG_INIT_PNG 2)
(def 'IMG_INIT_TIF 4)

(de imgInit (IFlag)
    (native `*SDLimg "IMG_Init" 'I IFlag) )

(de imgQuit () (native `*SDLimg "IMG_Quit"))

(de imgLoad (SFile)
    (let S (native `*SDLimg "IMG_Load" 'P SFile)
        (if (=0 S)
            (quit "imgLoad: Error loading image" SFile)
            S) ) )
    # Returns a pointer to a SDL_Surface

(de imgSavePNG (PSurf SFile) (native `*SDLimg "IMG_SavePNG" 'I PSurf SFile))

# This class loads images using sdlimage.l. It inherits from +Surface.
# See sdl.l and sdlutil.l.

(class +SprImg +Surface)

    (dm T (SFile Sid)
        (let S (imgLoad SFile)
             (=: surf S)
             (=: pxf (sdlGetSurfacePixelFormat (: surf)))
             (=: sprid Sid)
             (=: dest (%@ "malloc" 'P 16))
             (=: src (%@ "malloc" 'P 16)) ) )

# (loadsym sym1 sym2 ... any1 any2 ...) → lst
# Loads the files specified under sym1, sym2 ..., looks in the symbols
# any1 any2 ... for symbols contained in the files loaded, and skims
# all those symbols contained in the files that are not used in the
# val's of the symbols any1 any2 ...
# You can use this to strip unused symbols from large libraries,
# so the global namespace doesn't get too cluttered.

(de "symsof" (D)
    (push1q '"Using" D)
    (when (lst? (val D))
        (mapc "symsof"
            (uniq
                (filter '((S) (and (member S "Loaded")
                    (not (member S "Using")) (not (str? S)) ) )
                    (fish sym? (val D)) ) ) ) ) )

(de loadsym "Args"
    (setq "Orig" (all)
          "Loaded"
            (prog
                (while (str? (car "Args")) (load (pop '"Args")))
                (diff (all) "Orig") )
          "Using" NIL
          "Clear" NIL )
    (mapc "symsof" "Args")
    (setq "Clear" (diff "Loaded" "Using"))
    (mapc zap "Clear")
    (setq "Orig" (length "Orig")
          "Loaded" (length "Loaded")
          "Using" (length "Using")
          "Clear" (length "Clear") )
    # returns four numbers: initial, newly loaded,
    # used and removed symbols
    (list "Orig" "Loaded" "Using" "Clear") )

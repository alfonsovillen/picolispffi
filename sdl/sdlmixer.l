# Bindings for libSDL2_mixer.so
# Always load "sdl.l" before this file!!

# This works under Arch Linux
(def '*SDLmix "libSDL2_mixer.so")

(mapc '((K) (def (car K) (hex (cadr K))))
    (quote
        (MIX_INIT_FLAC      "1")
        (MIX_INIT_MOD       "2")
        (MIX_INIT_MP3       "8")
        (MIX_INIT_OGG       "10")
        (MIX_INIT_MID       "20")
        (AUDIO_S16LSB       "8010") # little endian
        (MIX_DEFAULT_FORMAT "8010") # change to "9010" if your system is big-endian
        (AUDIO_S16MSB       "9010") # big endian
        ) )

(de mixInit ("IFlag") (native `*SDLmix "Mix_Init" 'I "IFlag"))

(de mixQuit () (native `*SDLmix "Mix_Quit"))

(de mixOpenAudio ("Freq" "Format" "Channels" "ChSize")
    (default "Freq" 22050 "Format" `MIX_DEFAULT_FORMAT "Channels" 2
        "ChSize" 4096 )
    (native `*SDLmix "Mix_OpenAudio" 'I "Freq" "Format" "Channels"
        "ChSize" ) )

(de mixCloseAudio () (native `*SDLmix "Mix_CloseAudio"))

(de mixHaltMusic () (native `*SDLmix "Mix_HaltMusic"))

(de mixPauseMusic () (native `*SDLmix "Mix_PauseMusic"))

(de mixResumeMusic () (native `*SDLmix "Mix_ResumeMusic"))

(de mixRewindMusic () (native `*SDLmix "Mix_RewindMusic"))

(de mixPausedMusic () (native `*SDLmix "Mix_PausedMusic" 'I))
# returns 0 = success or another value = failure

(de mixPlayingMusic () (native `*SDLmix "Mix_PlayingMusic" 'I))
# returns 0 = success or another value = failure

(de mixFadeOutMusic (IMs) (native `*SDLmix "Mix_FadeOutMusic" 'I IMs))

# only WAV files for +Chunk

(class +Chunk)

    (dm T ("SFile")
        (let RW (sdlRWops "SFile")
            (=: chunk (native `*SDLmix "Mix_LoadWAV_RW" 'P RW 1))
            (if (=0 (: chunk))
                (quit (pack "Error loading sound file " SFile))
                (mapc '((K V) (put This K V))
                    '(allocated abuf alen volume)
                    (struct (: chunk) '(I B I B)) ) ) ) )

    (dm destroy> () (native `*SDLmix "Mix_FreeChunk" NIL (: chunk)))

    (dm play> ("Loops")
        (default "Loops" 0) # defaults to playing once (no looping)
        (=: channel (native `*SDLmix "Mix_PlayChannelTimed" 'I -1
            (: chunk) "Loops" -1 ) ) )
            
(class +Music)

    (dm T ("SFile")
        (=: music (native `*SDLmix "Mix_LoadMUS" 'P "SFile"))
        (when (=0 (: music))
            (quit (pack "Error loading music file " SFile)) ) )
    
    (dm destroy> () (native `*SDLmix "Mix_FreeMusic" NIL (: music)))

    (dm play> ("Loops")
        (default "Loops" 0) # defaults to playing once (no looping)
        (=: channel (native `*SDLmix "Mix_PlayMusic" 'I
            (: music) "Loops" ) ) )

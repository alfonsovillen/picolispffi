# Alfonso Villén Carrasco (alfonso.villen@gmail.com)
# License: CC0 - http://creativecommons.org/publicdomain/zero/1.0/

(scl 6)

(load "@sdl/transform.l" "@sdl/timing.l")

(setq "trans" "@sdl/lib/libtransform.so")

(de doTest ()
    (mbench
        (setq M (native "trans" "newm" 'P 4))

        (native "trans" "identm" NIL M 0)

        (native "trans" "rotxm" NIL M 0 (1.0 . -1.0))

        (native "trans" "rotym" NIL M 0 (0.3 . -1.0))

        (native "trans" "rotzm" NIL M 0 (0.4 . -1.0))

        (native "trans" "transm" NIL M 0
            (1.0 . -1.0) (1.0 . -1.0) (-3.0 . -1.0) )

        (native "trans" "lookatm" NIL M 1
            (0 . -1.0) (0 . -1.0) (-1.0 . -1.0)
            (0 . -1.0) (0 . -1.0) (3.0 . -1.0)
            (0 . -1.0) (1.0 . -1.0) (0 . -1.0) )

        (native "trans" "perspm" NIL M 2
            (45.0 . -1.0) (cons (*/ 1.0 4.0 3.0) -1.0)
            (0.1 . -1.0) (10.0 . -1.0) )

        (native "trans" "mvpm" NIL M 3 0 1 2)
    )

    (native "trans" "disp" NIL M 3)

    (%@ "free" NIL M)

    (mbench
        (setq M
            (mvpm
                (multm
                    (identm)
                    (rotxm 1.0)
                    (rotym 0.3)
                    (rotzm 0.4)
                    (transm 1.0 1.0 -3.0) )
                (lookatm (0 0 -1.0) (0 0 3.0) (0 1.0 0))
                (perspm 45.0 (*/ 1.0 4.0 3.0) 0.1 10.0) ) )
    )

    (println M)
    
    )

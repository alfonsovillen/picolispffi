// Alfonso Villén Carrasco (alfonso.villen@gmail.com)
// License: CC0 - http://creativecommons.org/publicdomain/zero/1.0/

// Source of the libtransform.so shared library

#define MATH_3D_IMPLEMENTATION

#include <stdlib.h>
#include "math_3d.h"

// allocate an array of 4x4 matrices
// matrices are numbered 0 .. array size - 1
mat4_t *newm(int n) {
    mat4_t *p = malloc(n * sizeof(mat4_t));
    return p;
}

// sets matrix #i to identity
void identm(mat4_t *m, int i) {
    m[i] = m4_identity();
}

// apply a translation to matrix #i
void transm(mat4_t *m, int i, float x, float y, float z) {
    mat4_t a, b;
    a = m4_translation(vec3(x, y, z));
    b = m4_mul(m[i], a);
    m[i] = b;
}

// apply a scaling to matrix #i
void scalem(mat4_t *m, int i, float x, float y, float z) {
    mat4_t a, b;
    a = m4_scaling(vec3(x, y, z));
    b = m4_mul(m[i], a);
    m[i] = b;
}

// apply a rotation around the Z axis to matrix #i
void rotzm(mat4_t *m, int i, float r) {
    mat4_t a, b;
    a = m4_rotation(r, vec3(0.0f, 0.0f, 1.0f));
    b = m4_mul(m[i], a);
    m[i] = b;
}

// apply a rotation around the Y axis to matrix #i
void rotym(mat4_t *m, int i, float r) {
    mat4_t a, b;
    a = m4_rotation(r, vec3(0.0f, 1.0f, 0.0f));
    b = m4_mul(m[i], a);
    m[i] = b;
}

// apply a rotation around the X axis to matrix #i
void rotxm(mat4_t *m, int i, float r) {
    mat4_t a, b;
    a = m4_rotation(r, vec3(1.0f, 0.0f, 0.0f));
    b = m4_mul(m[i], a);
    m[i] = b;
}

// creates a model matrix applying the transformations in the
// matrices indexed from first to last
// the result is stored in matrix #i
// the pointer to matrix #i is returned
mat4_t *multm(mat4_t *m, int i, int first, int last) {
    mat4_t a, b, c;
    int f;
    c = m[first];
    for (f = first + 1; f <= last; f++) {
        a = c;
        b = m[f];
        c = m4_mul(a, b);
    }
    m[i] = c;
    return &m[i];
}

// creates a "look at" matrix (view matrix) and stores it in matrix #i
// px, py, pz = eye
// tx, ty, tz = target
// ux, uy, uz = up
void lookatm(mat4_t *m, int i, float px, float py, float pz, float tx, float ty, float tz, float ux, float uy, float uz) {
    m[i] = m4_look_at(vec3(px, py, pz), vec3(tx, ty, tz), vec3(ux, uy, uz));
}

// creates a perspective projection matrix and stores it in #i
void perspm(mat4_t *m, int i, float fov, float ratio, float near, float far) {
    m[i] = m4_perspective(fov, ratio, near, far);
}

// creates a model-view-projection matrix the indices of three matrices
// and stores the result in #i, returning the pointer to it
mat4_t *mvpm(mat4_t *m, int i, int model, int view, int projection) {
    mat4_t t1, result;
    t1 = m4_mul(m[projection], m[view]);
    result = m4_mul(t1, m[model]);
    m[i] = result;
    return &m[i];
}

// prints out matrix #i
void disp(mat4_t *m, int i) {
    m4_print(m[i]);
}

# Functions for manipulating 4x4 matrices and 4 row vectors

# Matrices in column major order

# Identity matrix

(load "@lib/math.l")

(de identm () (1.0 0 0 0 0 1.0 0 0 0 0 1.0 0 0 0 0 1.0))

# Translation matrix

(de transm (X Y Z) (list 1.0 0 0 0 0 1.0 0 0 0 0 1.0 0 X Y Z 1.0) )

# Remove translation from a matrix

(de remtransm (Mat) (append (head 12 Mat) (0 0 0 1.0)))

# Scaling matrix

(de scalem (X Y Z) (list X 0 0 0 0 Y 0 0 0 0 Z 0 0 0 0 1.0) )

# Rotation matrices, angle in radians

(de rotzm (Rad) # Z rotation
    (let (C (cos Rad)
          S (sin Rad) )
        (list C S 0 0 (- S) C 0 0 0 0 1.0 0 0 0 0 1.0) ) )

(de rotxm (Rad) # X rotation
    (let (C (cos Rad)
          S (sin Rad) )
        (list 1.0 0 0 0 0 C S 0 0 (- S) C 0 0 0 0 1.0) ) )

(de rotym (Rad) # Y rotation
    (let (C (cos Rad)
          S (sin Rad) )
        (list C 0 (- S) 0 0 1.0 0 0 S 0 C 0 0 0 0 1.0) ) )

# Vector addition

(de sumv (V1 V2) (mapcar + V1 V2))

# Vector subtraction

(de subv (V1 V2) (mapcar - V1 V2))

# Vector dot product

(de dotv (V1 V2) (/ (sum * V1 V2) 1.0))

# Vector cross product

(de crossv (V1 V2)
    (let ((Ax Ay Az) V1 (Bx By Bz) V2)
        (list
            (/ (- (* Ay Bz) (* Az By)) 1.0)
            (/ (- (* Az Bx) (* Ax Bz)) 1.0)
            (/ (- (* Ax By) (* Ay Bx)) 1.0) ) ) )

# Vector normalization

(de normv (V)
    (let L (sqrt (sum * V V))
        (mapcar '((X) (*/ 1.0 X L)) V) ) )

# Determine the direction of a 2D vector

(de dirv (Vec)
   (let N (normv Vec)
      (maxi '((V) (dotv N (car V)))
         (list '((0 1.0) . up) '((1.0 0) . right) '((0 -1.0) . down)
            '((-1.0 0) . left) ) ) ) )

# Determine the length of a 2D vector

(de lenv (V) (sqrt (sum * V V)))

# Transposition

(de xy2yx (Mat)
    (mapcan list (cut 4 'Mat) (cut 4 'Mat) (cut 4 'Mat) (cut 4 'Mat) ) )

# Multiplication

# Multiply a vector by a number

(de vecxnum (Vec N) (mapcar '((V) (*/ N V 1.0)) Vec))

# Multiply a matrix by a vector

(de matxvec (Mat Vec)
    (apply mapcar
        (mapcar '((V) (vecxnum (cut 4 'Mat) V)) Vec) + ) )

# Multiply two matrices together, M1 = source matrix, M2 = applied matrix
# The same as in OpenGL M1 * M2

(de matxmat (M1 M2)
          # convert M1 to a list of rows
    (let (A (mapcar list (cut 4 'M1) (cut 4 'M1) (cut 4 'M1) (cut 4 'M1))
          # convert M2 to a list of columns
          B (list (cut 4 'M2) (cut 4 'M2) (cut 4 'M2) (cut 4 'M2))
          # store the dot products in a list of rows
          S (mapcar '((R) (mapcar '((C) (dotv R C)) B)) A) )
        # convert the solution to a single column-major list and return
        (apply mapcan S list) ) )

# Translation of a vector

(de transv (Vec X Y Z) (matxvec (transm X Y Z) Vec))

# Scaling of a vector

(de scalev (Vec X Y Z) (matxvec (scalem X Y Z) Vec))

# Rotation of a vector in radians

(de rotxv (Vec X) (matxvec (rotxm X) Vec))
(de rotyv (Vec Y) (matxvec (rotym Y) Vec))
(de rotzv (Vec Z) (matxvec (rotzm Z) Vec))

# Model matrix: scale, rotate, translate
# Any number of matrices can be passed, but beware to pass them
# in that order
# Matrices are multiplied in sequence, not as OpenGL multiplies them
# So the first matrix must have been applied to a vector, if necessary

(de multm @
    (let Res (next)
        (while (args)
            (setq Res (matxmat (next) Res)) )
        Res ) )

# "Look at" matrix

(de lookatm (Eye Tar Up)
    (let (Axz (normv (subv Eye Tar))
          Axx (normv (crossv Up Axz))
          Axy (crossv Axz Axx) )
      (xy2yx
         (append Axx (cons (- (dotv Axx Eye)))
            Axy (cons (- (dotv Axy Eye)))
            Axz (cons (- (dotv Axz Eye)))
            (0 0 0 1.0) ) ) ) )

# Orthographic projection matrix, aspect corrected

(de orthom (Left Top Right Bottom Near Far)
    (let (R+L (+ Right Left)
          R-L (- Right Left)
          T+B (+ Top Bottom)
          T-B (- Top Bottom)
          F+N (+ Far Near)
          F-N (- Far Near) )
        (list
            (*/ 1.0 2.0 R-L) 0 0 0
            0 (*/ 1.0 2.0 T-B) 0 0
            0 0 (*/ 1.0 -2.0 F-N) 0
            (- (*/ 1.0 R+L R-L)) (- (*/ 1.0 T+B T-B)) (- (*/ 1.0 F+N F-N)) 1.0 ) ) )

(def 'Pi180 (*/ 1.0 3.141593 180.0)) # pi / 180 = 0.017453
(def '_180Pi (*/ 1.0 180.0 3.141593)) # 180 / pi = 57.295773

(de radians (Degrees) (*/ Degrees `Pi180 1.0))

(de degrees (Rad) (*/ Rad `_180Pi 1.0))

# Perspective projection matrix

(de perspm (Fov Aspect Near Far) # Fov in degrees
    (let (Top (*/ Near (tan (*/ `Pi180 Fov 2.0)) 1.0)
          Bottom (- Top)
          Right (*/ Top Aspect 1.0)
          Left (- Right) )
        (list (*/ 2.0 Near (- Right Left)) 0 0 0
              0 (*/ 2.0 Near (- Top Bottom)) 0 0
              (*/ 1.0 (+ Right Left) (- Right Left))
              (*/ 1.0 (+ Top Bottom) (- Top Bottom))
              (*/ 1.0 (- (+ Far Near)) (- Far Near)) -1.0
              0 0 (*/ -2 Far Near (- Far Near)) 0 ) ) )
              
# Model-view-projection matrix

(de mvpm (M V P) (matxmat P (matxmat V M)))

# Matrix inversion

(de invm (Mat)
   (let
      ((M0 M1 M2 M3 M4 M5 M6 M7 M8 M9 M10 M11 M12 M13 M14 M15) Mat
        I0 (+ (* M5 M10 M15)
              (- (* M5 M11 M14))
              (- (* M9 M6 M15))
              (* M9 M7 M14)
              (* M14 M6 M11)
              (- (* M13 M7 M10)) )
        I4 (+ (* (- M4) M10 M15)
              (* M4 M11 M14)
              (* M8 M6 M15)
              (- (* M8 M7 M14))
              (- (* M12 M6 M11))
              (* M12 M7 M10) )
        I8 (+ (* M4 M9 M15)
              (- (* M4 M11 M13))
              (- (* M8 M5 M15))
              (* M8 M7 M13)
              (* M12 M5 M11)
              (- (* M12 M7 M9)) )
        I12 (+ (* (- M4) M9 M14)
               (* M4 M10 M13)
               (* M8 M5 M14)
               (- (* M8 M6 M13))
               (- (* M12 M5 M10))
               (* M12 M6 M9) )
        I1 (+ (* (- M1) M10 M15)
              (* M1 M11 M14)
              (* M9 M2 M15)
              (- (* M9 M3 M14))
              (- (* M13 M2 M11))
              (* M13 M3 M10) )
        I5 (+ (* M0 M10 M15)
              (- (* M0 M11 M14))
              (- (* M8 M2 M15))
              (* M8 M3 M14)
              (* M12 M2 M11)
              (- (* M12 M3 M10)) )
        I9 (+ (* (- M0) M9 M15)
              (* M0 M11 M13)
              (* M8 M1 M15)
              (- (* M8 M3 M13))
              (- (* M12 M1 M11))
              (* M12 M3 M9) )
        I13 (+ (* M0 M9 M14)
               (- (* M0 M10 M13))
               (- (* M8 M1 M14))
               (* M8 M2 M13)
               (* M12 M1 M10)
               (- (* M12 M2 M9)) )
        I2 (+ (* M1 M6 M15)
              (- (* M1 M7 M14))
              (- (* M5 M2 M15))
              (* M5 M3 M14)
              (* M13 M2 7)
              (- (* M13 M3 M6)) )
        I6 (+ (* (- M0) M6 M15)
              (* M0 M7 M14)
              (* M4 M2 M15)
              (- (* M4 M3 M14))
              (- (* M12 M2 M7))
              (* M12 M3 M6) )
        I10 (+ (* M0 M5 M15)
               (- (* M0 M7 13))
               (- (* M4 M1 M15))
               (* M4 M3 M13)
               (* M12 M1 M7)
               (- (* M12 M3 M5)) )
        I14 (+ (* (- M0) M5 M14)
               (* M0 M6 M13)
               (* M4 M1 M14)
               (- (* M4 M2 M13))
               (- (* M12 M1 M6))
               (* M12 M2 M5) )
        I3 (+ (* (- M1) M6 M11)
              (* M1 M7 M10)
              (* M5 M2 M11)
              (- (* M5 M3 M10))
              (- (* M9 M2 M7))
              (* M9 M3 M6) )
        I7 (+ (* M0 M6 M11)
              (- (* M0 M7 M10))
              (- (* M4 M2 M11))
              (* M4 M3 M10)
              (* M8 M2 M7)
              (- (* M8 M3 M6)) )
        I11 (+ (* (- M0) M5 M11)
               (* M0 M7 M9)
               (* M4 M1 M11)
               (- (* M4 M3 M9))
               (- (* M8 M1 M7))
               (* M8 M3 M5) )
        I15 (+ (* M0 M5 M10)
               (- (* M0 M6 M9))
               (- (* M4 M1 M10))
               (* M4 M2 M9)
               (* M8 M1 M6)
               (- (* M8 M2 M5)) )
        Det (+ (* M0 I0) (* M1 I4) (* M2 I8) (* M3 I12))
        Fac NIL )
      (unless (=0 Det)
         (setq Fac (/ `(* 1.0 1.0 1.0 1.0 1.0) Det))
         (mapcar '((In) (/ (* In Fac) `(* 1.0 1.0 1.0)))
            (list I0 I1 I2 I3 I4 I5 I6 I7 I8 I9 I10 I11 I12 I13 I14 I15) ) ) ) )

# Camera class - here because cameras on OpenGL are all about matrices

(class +Camera)

   # Create a new camera: X, Y, Z = camera position; ScrW, ScrH = width and
   # height of the window/screen/viewport; Near, Far = near and far planes
   # The camera's default view is aligned to the positive Z axis (looking to the
   # viewer)

   (dm T (X Y Z ScrW ScrH Near Far)
      (mapc '((K V) (put This K V))
         '(pos ang fov scrw scrh near far)
         (list (list X Y Z) (0 0) 45.0
            ScrW ScrH Near Far ) )
      (aspect> This)
      (dir> This) )

   # Displace the camera: Dir = one of up, down, right, left; D = displacement

   (dm move> (Dir D)
      (=: pos
         (case Dir
            (up (sumv (: pos) (vecxnum (: dir) D)))
            (down (subv (: pos) (vecxnum (: dir) D)))
            (right (sumv (: pos) (vecxnum (: right) D)))
            (left (subv (: pos) (vecxnum (: right) D))) ) ) )

   # Rotate the camera: DX = amount of rotation left (negative) / right
   # (positive) in degrees; DY = amount of rotation up (negative) / down
   # (positive) in degrees

   (dm rot> (DX DY)
      (=: ang (mapcar + (: ang) (list DX DY)))
      (dir> This) )

   # Calculate window/screen/viewport aspect ratio. Mostly internal use

   (dm aspect> () (=: aspect (*/ 1.0 (: scrw) (: scrh))))

   # Calculate direction vectors. Mostly internal use

   (dm dir> ()
      (let Rad (mapcar radians (: ang))
         (=: dir (list
            (*/ (cos (cadr Rad)) (sin (car Rad)) 1.0)
            (sin (cadr Rad))
            (*/ (cos (cadr Rad)) (cos (car Rad)) 1.0) ) )
         (=: right (list
            (sin (- (car Rad) `(/ 3.141593 2)))
            0
            (cos (- (car Rad) `(/ 3.141593 2))) ) )
         (=: up (crossv (: right) (: dir))) ) )

   # Calculate the view matrix of the current camera settings

   (dm view> ()
      (lookatm (: pos) (sumv (: pos) (: dir)) (: up)) )

   # Calculate the projection matrix of the current camera settings

   (dm proj> ()
      (perspm (: fov) (: aspect) (: near) (: far)) )


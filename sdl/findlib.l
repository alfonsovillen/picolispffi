# findlib uses the GNU/Linux tool ldconfig to locate a .so file

(de findlib (Lib)
   (let Matches
      (in '(ldconfig "-p") # Use '(ldconfig "-r") on FreeBSD (thanks to rick@tamos.net for testing)
         (filter
            '((F) (sub? Lib F))
            (make (while (line T) (link @))) ) )
      (and Matches
         (pack (clip (stem (chop (car Matches)) "=" ">") ) ) ) ) )


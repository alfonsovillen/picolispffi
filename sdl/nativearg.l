(de unsigned (N)
   (& `(dec (** 2 32)) (+ N `(** 2 32))) )

# Arguments for 'struct'

# uint[]
(de uints (L)
    (make
        (for V L
            (if (=0 V) (link 0 0 0 0) (link (- V))) ) ) )

# int[]
(de ints (L)
   (make
      (for V L
         (link (cons V 4)) ) ) )

# float[]
(de floats (Scale Values) (cons (- Scale) Values))

# double[]
(de doubles (Scale Values) (cons Scale Values))

# Pointer arguments for 'native'

# Allocate an array for passing the pointer via 'native' to a C
# function that fills it. The values are available in the symbol Sym.

(de getInts* (Sym Size)
    (list Sym (cons (* Size 4) 'I Size)) )

# Pass a list of numbers as unsigned bytes to a C function via 'native'

(de ubytes* (L) (cons NIL (list (length L)) L) )

# Pass a list of numbers as unsigned integers to a C function via 'native'
# If an element is 0, it is treated by native as an unsigned byte, so
# we repeat it 4 times

(de uints* (L)
    (cons NIL (make (link (list (* 4 (length L))))
      (for V L
         (if (=0 V) (link 0 0 0 0) (link (- V))) ) ) ) )
            
# Pass a list of numbers as integers (4 bytes each) to a C function via 'native'

(de ints* (L)
   (cons NIL (make (link (list (* 4 (length L))))
      (for V L (link (cons V 4))) ) ) )
         
# Pass a list of fixpoint numbers as floats to a C function via 'native'

(de floats* (Scale Values)
     (list NIL (list (* 4 (length Values))) (cons (- Scale) Values)) )

# Pass a list of fixpoint numbers as doubles to a C function via 'native'

(de doubles* (Scale Values)
     (list NIL (list (* 4 (length Values))) (cons Scale Values)) )

# allocates a list of strings and returns a list of their pointers

(de getStrings* (L)
    (mapcar '((S) (cons (%@ "strdup" 'P S) 8)) L) )

# for passing a pointer to a getStrings* Lst to a C function

(de strings* (Lst)
    (let Len (length Lst)
        (cons NIL (list (* 8 Len)) Lst) ) )

# deallocates a structure made with getStrings*

(de freeStrings* (PL)
    (mapc '((P) (%@ "free" NIL (car P))) PL) )

# for passing a pointer to an int to a C function and return its value.
# The value is available in the symbol Sym.

(de getInt* (Sym) (list Sym '(4 . I)))

# for passing a pointer to an int value to a C function.
# if N is negative, it will be an unsigned int.

(de int* (N) (list NIL '(4) N))

(de uint* (N) (int* (- N)))

# for passing a pointer to a float to a C function and return its value.
# The value is available in the symbol Sym.

(de getFloat* (Sym) (list Sym (4 . -1.0)))

# for passing a pointer to a float value to a C function.
# use Scale appropriately.

(de float* (Scale N) (list NIL '(4) (cons N (- Scale))))

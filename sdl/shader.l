# This file depends on "opengl3.l"

# A convenient class for working with OpenGL shader programs
# and the matrices used in them
# Translation from learnopengl.com

(class +Shader)

# Supply a vertex shader, a fragment shader and optionally a geometry
# shader in this order

(dm T @
   (let (A (list `GL_VERTEX_SHADER `GL_FRAGMENT_SHADER `GL_GEOMETRY_SHADER)
         S NIL )
      (while (args)
         (let C (glCreateShader (++ A))
            (push 'S C)
            (glShaderSource C (next))
            (glCompileShader C)
            (unless (compiled? C)
               (mapc glDeleteShader S)
               (quit (pack "Compilation of shader " C " failed.")) ) ) )
      (when S
         (let P (glCreateProgram)
            (for Sh S (glAttachShader P Sh))
            (glLinkProgram P)
            (unless (linked? P)
               (mapc glDeleteShader S)
               (glDeleteProgram P)
               (quit (pack "Linking of program " P " failed.")) )
            (=: shprog P)
            (mapc glDeleteShader S) ) ) ) )
  
    (dm use> () (glUseProgram (: shprog)))
    
    # stores a new property with the name SName and the number
    # of the shader uniform that corresponds to it
    
    (dm uniform> (SName)
        (put This SName (glGetUniformLocation (: shprog) SName)) )

    # utility uniform methods. Uniforms (SName) must have been got
    # using "uniform>"
    
    # setint> also accepts bools (0=false, 1=true)
    (dm setint> (SName IValue) (glUniform1i (get This SName) IValue))
    
    (dm setfloat> (SName FValue) (glUniform1f (get This SName) FValue))
    
    (dm setvec> (SName Fx Fy Fz Fw)
        (if Fw (glUniform4f (get This SName) Fx Fy Fz Fw)
            (if Fz (glUniform3f (get This SName) Fx Fy Fz)
                (if Fy (glUniform2f (get This SName) Fx Fy)
                    (glUniform1f (get This SName) Fx) ) ) ) )

    # allocate space for a new matrix of IDimension rows and columns,
    # creating a new property with the matrix's name and storing
    # its pointer in the "mats" property. Matrix elements are floats
    # (4 bytes), so the amount of bytes allocated is 4 * dimension^2.
    # Allocating and deallocating matrix buffers explicitly (and just
    # passing the buffer pointer to C functions instead of having
    # "native" do that each time) should improve performance.
    
    (dm newmat> (IDimension SName)
        (let P (%@ "malloc" 'P (* 4 IDimension IDimension))
            (put This SName P)
            (push (:: mats) P) ) )

    # matrices passed to setmat> must be in column-major
    # order. Their elements are floats.
    # IDimension specifies the size of the matrix.
    # SName is the property name of the matrix, which was previously
    # allocated using "newmat>"
    
    (dm setmat> (IDimension SUniform SMat Mat)
        (let S (get This SMat)
            (struct S NIL (cons -1.0 Mat))
            (case IDimension
                (4 (glUniformMatrix4fv (get This SUniform) 1 0 S))
                (3 (glUniformMatrix3fv (get This SUniform) 1 0 S))
                (2 (glUniformMatrix2fv (get This SUniform) 1 0 S)) ) ) )

    # allocate space for a new array
    # creating a new property with the arrays's name and storing
    # its pointer in the "mats" property. Array elements are IItemSize
    # bytes long (e.g. 12 bytes for a vec3 (3 floats), 4 for an integer)
    # and ICount elements are allocated, so the amount of bytes
    # allocated is IItemSize * ICount.
    # Allocating and deallocating array buffers explicitly (and just
    # passing the buffer pointer to C functions instead of having
    # "native" do that each time) should improve performance.
        
    (dm newarray> (IItemSize ICount SName)
        (let P (%@ "malloc" 'P (* IItemSize ICount))
            (put This SName P)
            (push (:: mats) P) ) )

    # set a whole array or a piece of an array. The array elements are floats.
    # ISize = number of floats of each array element (3 = vec3), between 1 and 4
    # IOffset = element offset starting from 0
    # Lst = elements as a single list
    
    (dm setarrayf> (SUniform SArr ISize IOffset Lst)
       (let (C (length Lst)
             A (get This SArr) )
           (if (=0 IOffset)
               (struct A NIL (cons -1.0 Lst))
               (struct (+ A (* 4 ISize IOffset)) NIL (cons -1.0 Lst)) )
           (case ISize
               (1 (glUniform1fv (get This SUniform) C A))
               (2 (glUniform2fv (get This SUniform) C A))
               (3 (glUniform3fv (get This SUniform) C A))
               (4 (glUniform4fv (get This SUniform) C A)) ) ) )

    # set a whole array or a piece of an array. The array elements are integers.
    # Integers are 4 bytes wide.
    # ISize = number of integers of each array element, between 1 and 4
    # IOffset = element offset starting from 0
    # Lst = elements as a single list
    
    (dm setarrayi> (SUniform SArr ISize IOffset Lst)
       (let (C (length Lst)
             A (get This SArr)
             L (mapcar '((K) (cons K 4)) Lst) )
           (if (=0 IOffset)
               (struct A NIL L)
               (struct (+ A (* 4 ISize IOffset)) NIL L) )
           (case ISize
               (1 (glUniform1iv (get This SUniform) C A))
               (2 (glUniform2iv (get This SUniform) C A))
               (3 (glUniform3iv (get This SUniform) C A))
               (4 (glUniform4iv (get This SUniform) C A)) ) ) )
    
    (dm destroy> ()
        (glDeleteProgram (: shprog))
        (mapc '((K) (%@ "free" NIL K)) (: mats)) )


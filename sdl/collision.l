# sscoll? : check collision of two spheres
# SphereA, SphereB = (radius x y z)

(de sscoll? (SphereA SphereB)
   (let (Ra (++ SphereA)
         Rb (++ SphereB) )
      (fully '((A B)
         (if (> A B)
            (>= (+ B Rb) (- A Ra))
            (>= (+ A Ra) (- B Rb)) ) )
         SphereA SphereB ) ) )

# spcoll? : check collision of a sphere and a box or a list of points
# Sphere = (radius x y z)
# Points = list of (x y z)

(de spcoll? (Sphere Points)
   (let Rs (** (++ Sphere) 2)
      (find '((B)
         (>= Rs
            (sum '((X S) (** (- X S) 2)) B Sphere) ) )
         Points ) ) )


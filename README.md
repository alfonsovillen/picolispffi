# README #

Picolisp bindings for SDL2 (SDL2, SDL2_image, SDL2_mixer and SDL2_ttf) and OpenGL 3.3 core profile.

This is still a work in progress!

Everything has been tested on two computers running Arch Linux and one running Linux Mint. If you want to test the demos, play the games, inspect and revise the code, I'll be glad to get some feedback from you by mail (alfonso.villen@gmail.com).

## What you can do with this ##

- Use the SDL2, SDL2_image, SDL2_mixer, SDL2_ttf and OpenGL libraries in Picolisp programs
- Develop simple graphical applications and games with the included framework
- Explore the demos, see the screenshots
- Have fun with the games!

## What you need ##

- Pil21 on Linux (get it at www.picolisp.com)
- The SDL2, SDL2_image, SDL2_mixer and SDL2_ttf runtime libraries (not the development headers)
- An OpenGL runtime library such as Mesa (the development headers are not needed)
- A graphics cards that supports OpenGL 3.3 or above (or OpenGL 2.1 for the Logo interpreter)

## How to use ##

When you download this repository, you also must either:

- Copy the contents of the `sdl/` folder into your Picolisp installation folder.
- Or make a symbolic link with the name `sdl` from your Picolisp installation folder to the `sdl` folder.

To use SDL2, type in your source file: `(load "@sdl/sdl.l" "@sdl/sdlutil.l")`.
To use SDL2 + OpenGL, type: `(load "@sdl/sdl.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l")`.
To use SDL2_image, type: `(load "@sdl/sdl.l" "@sdl/sdlimage.l")`.
To use SDL2_mixer, type: `(load "@sdl/sdl.l" "@sdl/sdlmixer.l")`.
To use SDL2_ttf, type: `(load "@sdl/sdl.l" "@sdl/sdlttf.l")`.
To use the turtle graphics framework, type : `(load "@sdl/turtle.l")`.

From version 3.0, OpenGL replaced the fixed pipeline with the programmable pipeline using shaders.
The specification encourages using the modern pipeline. `(load "@sdl/shader.l")` will provide you
a simple interface for working with OpenGL shader programs and uniforms.

## Contents ##

### sdl/ folder ###

- collision.l (two collision detection functions)
- coords.l (a class for rectangular coordinates)
- findlib.l (function for locating shared .so library files)
- glconst.l (constants for use with OpenGL, not complete)
- loadobj.l (functions for loading simple Blender .OBJ files)
- loadsym.l (function for loading source files and removing non-needed symbols)
- nativearg.l (functions for working with `native` arguments)
- nativetrans.l ( `native` bindings to `libtransform.so` in the `lib` folder)
- opengl3.l (bindings for OpenGL from version 3.3)
- sdl.l (bindings for SDL2, not complete)
- sdlconst.l (constants for use with SDL functions, not complete)
- sdlimage.l (bindings for SDL2_image, for loading image files into SDL_Surfaces)
- sdlmixer.l (bindings for SDL2_mixer)
- sdlttf.l (bindings for SDL2_ttf)
- sdlopengl.l (bindings for SDL2 OpenGL interface, not complete)
- sdl_score.l (a class for displaying game scores)
- sdlutil.l (Picolisp classes for working with surfaces and windows more comfortably)
- shader.l (class for compiling shaders and managing matrices and uniforms in shader programs)
- timing.l (two benchmarking functions)
- transform.l (functions for working with model, view and projection matrices, includes a test)
- turtle.l (tool based on Logo's turtle graphics)
- ubo.l (class for working with uniform buffer objects)

## demos/ folder ##

![demo12.l screenshot](/screenshots/demo12.png)

- demo\*.l (\* = 1 to 3 with SDL2, 4 to 13 with SDL2 and OpenGL.
Load them with `pil demo1.l` or `pil demo1.l +` (debug mode), and
start them with `(main)`.

- demo14.l (an example of what you can do with turtle.l). Load it with `pil demo14.l`or `pil demo14.l +` (debug mode), and start it with `(main)`.

- a very simple Pong game (load it with `pil sdl_pong.l` or `pil sdl_pong.l +` (debug mode)
and start it with `(main)`).

## sdl/lib/ ##

In this folder you'll find the shared library `libtransform.so`
as well as its source code (transform.c and math_3d.h), the `make`
file to build the library, and the test program `transtest.l`.

`math_3d.h` is a header library by Stephan Soller and Tobias Malmsheimer.

This library is used by `nativetrans.l` in the `sdl/` folder. It contains
some matrix arithmetics for use in OpenGL.

The file `transform.l` (also in the `sdl/` folder) is written in Picolisp and
provides (even many more) functions for working with matrices and vectors.

The demos `demo7.l` and `demo8.l` are the same except for the fact that `demo7.l`
uses the `transform.l` file and `demo8.l` uses the `nativetrans.l` and `libtransform.so` files.
Both demos show the time they spend performing matrix calculations per frame.

### Why a C library? ###

When programming real-time graphical applications, compute speed is an issue.

Based on a C library, `nativetrans.l` functions are supposed to be
faster than the equivalent Picolisp functions in `transform.l`, but
the way they handle matrices is different, because matrices in
`transform.l` are simple lists, whereas the ones in `nativetrans.l`
are `malloc`ed buffers.

However, in the tests I did, Picolisp performed its fixed-point arithmetic
calculations much faster than I had expected. In the end, I abandoned the
C alternative, but left it there for the records. Only `demo8.l` uses the C library.

## picoblocks/ folder ##

![Picoblocks screenshot](/screenshots/picoblocks.png)

Picoblocks is a falling blocks game. Load it from the `picoblocks/` folder with `pil picoblocks.l`
or `pil picoblocks.l +` (debug mode), and start it with `(main)`.
You'll need the SDL2, SDL2_image and SDL2_mixer runtime libraries installed.

### How to play ###

Press the space bar to start.

You have to place blocks of the same colour next to each other. Use the mouse for this: click on a block and it will appear at the bottom right part of the screen. Then click on a column and the block will fall there upon the other blocks in the column. When enough blocks of the same colour are next to each other, they'll disappear and your score will upgrade.

Blocks fall randomly all the time. After 50% of the level time is elapsed, you'll enter "Hurry Up!" mode, where new blocks will appear faster.

At level 1, you'll have to place 3 or more blocks next to each other to score. In levels 2 and 3, the minimum number of blocks is 4, and so on. At each level the speed increases.
When no more blocks can be placed, the game is over.

You can quit the game at any time while playing by pressing the escape key, or quit the program by pressing the escape key twice.

## breakout/ folder ##

![Breakout screenshot](/screenshots/breakout1.png)

A breakout game. Load it from this folder with `pil breakout.l` or `pil breakout.l +` (debug mode)
and start it with `(main)`.

The Breakout game is a partial translation from the C tutorial by Joey De Vries (https://twitter.com/JoeyDeVriez) found at http://learnopengl.com. Platform-dependent code (managing the game window and the keyboard events etc.) uses SDL2.
You'll need the OpenGL, SDL2, SDL2_image and SDL2_mixer libraries installed, as well as an OpenGL 3.3 compliant graphics card.

The game code is rather large and is split over several files.

### How to play ###

Control the pad with the "A" and "D" keys. Shoot the ball by pressing the space bar.

Hit all the coloured blocks to make them disappear. The gray blocks are indestructible. Collect the power pills, but be careful: some of them are evil!
You've got three lives.

You can quit the game anytime by pressing the escape key.

## Details: sdl/ folder ##

### sdl.l ###

This file contains bindings to many functions of the SDL2 C library. Because of the use of `native`, it only works on the 64-bit version of Picolisp.
It loads `sdlconst.l`, which contains named constants and enumerations from the SDL C header files.

The name of the library file is stored under `*SDLlib`. By default it's `"libSDL2.so"`.

The names of the original SDL2 functions begin with `SDL_`. In this file, the Picolisp functions have the prefix `sdl`.

In the file there are some functions that don't exist in the C library file. These are convenient functions that aim to work more easily with SDL initialization and shutdown, colours and pixel formats, among other things. They're marked as "alias" or "convenient function".

Passing structures to `native` can affect performance because they are allocated and freed each time. This is true specially in the case of the `SDL_Event` structure, that is usually checked at each animation frame (usually 60 times per second).
Therefore, `sdl.l` includes a customized `sdlInitApp` function that initializes SDL, allocates an SDL_Event structure (using `malloc`) and stores the pointer in the global variable `*SDLEvent`. Other functions such as `sdlPollEvent` use that pointer.
Finally, the `sdlQuitApp` function frees \*SDLEvent (using `free`) and shuts down SDL. This approach is used in other files, too.

Besides `*SDLEvent`, there are also other convenient functions for managing SDL events. Among those are:

- The functions `sdlEventLoop` and `sdlWaitLoop`, which can be used to render an animation frame and react to events such as key presses, mouse movement etc.
- The function `sdlHandleEvents`, which peeks at the type of the event stored in `*SDLEvent` and runs the corresponding element in `*SDLActions` (or does nothing, if no matching element is found)
- The global variable `*SDLStop`, which will make `sdlEventLoop` and `sdlWaitLoop` finish. Otherwise, `sdlEventLoop` and `sdlWaitLoop` will run an endless loop. So in order to end the event loop, you just need to do `(on *SDLStop)`.
- The global variable `*SDLActions`, which can be used to check the event queue and respond to selected events.
- The global variales `*SDLxxxxxEventStruct`, which map the bytes of an `SDL_Event` structure depending on the type of the event.
- The global variable `*SDLEventInfo`, which stores the information of an SDL event as a list.
- The functions `sdlMapxxxxxEvent`, which converts `*SDLEvent` into `*SDLEventInfo`, so the user can access the event information, such as what key was pressed, where the mouse cursor is, etc.

Using this set of functions and variables, along with the classes defined in `sdlutil.l`, `sdlimage.l` etc., can save you a lot of coding. This framework is used in all the demo files as well as in the Picoblocks and Breakout games.

Additionally, two custom functions return the data stored in the SDL_Surface and SDL_PixelFormat structures. They're called `surfstruct` and `pxfmtstruct`.

### sdlconst.l ###

This file sets a list of global variables that correspond to many of the constants defined in the SDL header files.
They're all together in a separate file so they don't clutter `sdl.l` and can be modified/completed more easily.

### sdlutil.l ###

This file contains some classes that encapsulate typical SDL usage.

- `+Window`: This class manages an SDL window.
    - Create a new window with `(new '(+Window) "Title" window_width window_height)`.
    - Methods of `+Window`:
        - `destroy>` (destroy the window as in C)
        - `framerate>` (set the refresh rate of the window in frames per second)
        - `update>` (refresh the window immediately)
        - `refresh>` (refresh the window trying to keep the frame rate)
    - Properties of `+Window`:
        - `win` (pointer to the SDL window)
        - `pxf` (pointer to the `SDL_PixelFormat` of the window)
        - `pxfname` (name of the `SDL_PixelFormat` of the window)
        - `size` (a list whose CAR is the width and its CADR the height of the window)
        - `rate` (milliseconds between frames = frame rate)
        - `ticks` (SDL ticks elapsed since the window was refreshed)


- `+RenderWindow`: This class manages an `SDL_Window` with an attached `SDL_Renderer` (so you can work with textures on it). It inherits from `+Window` and adds following methods and properties:
    - `rendercolor> (Red Green Blue Alpha)` (sets the render color)
    - `clear>` (clears the window using the render color specified with `rendercolor>`)
    - `renderer` (this property stores the pointer to the `SDL_Renderer` of the window)
    - `color` (this property stores a list with the render color; it is set by `rendercolor>`)
    

- `+Surface`: This class encapsulates an `SDL_Surface` and an `SDL_Texture`. It also allocates two SDL_Rect structures for blit and copy operations.
    - Create a new surface/texture with:
        - `(new '(+Surface) surface_width surface_height instance_of_+Window)`, this way the new surface will adapt the same pixel format as the window
        - `(new '(+Surface) surface_width surface_height NIL bits_per_pixel pixel_format)`, this way the new surface will have the pixel format that you specify (be sure to pass the "number" of the pixel format, not its pointer)
    - Methods of `+Surface`:
        - `setsrc>` (sets the source rectangle, property `src`)
        - `setdest>` (sets the destination rectangle, property `dest`)
        - `srcxy>` (sets the X and Y coordinates of the source rectangle)
        - `xy>` (sets the X and Y coordinates of the destination rectangle)
        - `getsrc>` (gets the source rectangle as a list)
        - `getdest>` (gets the destination rectangle as a list)
        - `pos>` (gets the X and Y coordinates of the destination rectangle as a list)
        - `move>` (adds or subtracts values from the X and Y coordinates of the destination rectangle)
        - `blitto>` (performs a blit on another surface using the source and destination rectangles)
        - `blitallto>` (blits the whole surface on another surface using the destination rectangle)
        - `totexture>` (converts an `SDL_Surface` into an `SDL_Texture`)
        - `copyto>` (copies the texture using the source and destination rectangles as well as an `SDL_Renderer`)
        - `copyallto>` (copies the whole texture using the destination rectangle and an `SDL_Renderer`)
        - `alpha>` (sets the value for alpha blending and stores it in the `alphamod` property)
        - `colorkey>` (sets the color key of the surface and stores it in the `colorkey` property, it also sets or clears the `hascolorkey` property)
        - `modulate>` (sets the color blending of the surface and stores it in the `colormod` property)
        - `blend>` (sets the blending mode of the surface and stores it in the `blend` property)
        - `resize>` (resizes the surface and converts it to the pixel format of a window or to the pixel format specified)
        - `destroy>` (frees the surface as well as the source and destination rectangles)
        - `collision?>` (wraps `SDL_HasIntersection`)
    - Properties of `+Surface`:
        - `surf` (the pointer to the `SDL_Surface`)
        - `pxf` (the pixel format as an integer)
        - `src` (the pointer to an `SDL_Rect` used as the source rectangle)
        - `dest` (the pointer to an `SDL_Rect` used as the destination rectangle)
        - `colorkey` (the color used as the color key)
        - `hascolorkey` (T if colorkey is set)
        - `istexture` (T if the surface was converted into a texture)
        - `colormod` (the color used to modulate the surface)
        - `alphamod` (the alpha value used to modulate the surface)
        - `blend` (the blending mode currently in use)


- `+Sprite`: This class creates a surface by loading a BMP bitmap file. It inherits from `+Surface` and adds the following property:
    - `sprid` (an optional identifier for the surface)


The file `sdlutil.l` also contains these two functions:


- `detect` (check collision of a surface against other surfaces)
- `detectall` (check collisions of some surfaces against each other)

### turtle.l and turtle-full.l ###

![Logo screenshot](/screenshots/logo.png)

- turtle.l develop a graphics framework that imitates Logo's turtle graphics.

To use it, type `(load "@sdl/turtle.l")`. 
Create a graphics window with `(startturtle)`, draw using typical turtle graphics commands such as "forward", "pendown", "setpencolor", "right", "showturtle" etc. You can also define new functions using "to". Close the graphics window with `(endturtle)`.

The demo no. 14 in the "demos/" folder shows some of the capabilities of the basic framework.


### sdlimage.l ###


This file has bindings to SDL2_image. It contains the functions needed to initialize and finalize this library. It also contains a new class.

This file relies on definitions in `sdl.l` and `sdlutil.l` and must be therefore loaded after those.

The name of the library is stored under the global variable `*SDLimg`. By default it's `"libSDL2_image.so"`.

The names of the original functions begin with `IMG_`. In this file, the Picolisp functions have the prefix `img`.

- `+SprImg`: This class creates a surface by loading an image in JPG, PNG or TIF format. It inherits from `+Surface` (defined in `sdlutil.l`).


### sdlmixer.l ###

This file has bindings to SDL2_mixer. It contains the functions needed to initialize and finalize this library. It also contains two new classes.

This file relies on definitions in `sdl.l` and must be therefore loaded after that one.

The name of the library is stored under the global variable `*SDLmix`. By default it's `"libSDL2_mixer.so"`.

The names of the original functions begin with `MIX_`. In this file, the Picolisp functions have the prefix `mix`.

- `+Chunk`: This class creates an `SDL_Chunk` (sound piece) by loading a WAV file.
    - Methods:
        - `play>` (wraps `Mix_PlayChannelTimed`)
        - `destroy>` (frees the `SDL_Chunk`)
    - Properties:
        - `chunk` (pointer to the `SDL_Chunk` structure)
        - `allocated, abuf, alen, volume` (fields of the `SDL_Chunk` structure)


- `+Music`: This class creates an `SDL_Music` by loading an audio file.
    - Methods:
        - `play>` (wraps `Mix_PlayMusic`)
        - `destroy>` (frees the `SDL_Music`)

### sdlttf.l ###

This file has plain bindings to the library.

The name of the library is stored under the global variable `*SDLttf`. By default it's `"libSDL2_ttf.so"`.

The names of the original functions begin with `TTF_`. In this file, the Picolisp functions have the prefix `ttf`.

### sdlopengl.l ###

This file has bindings to the OpenGL part of SDL2. It contains relevant constants and functions. It also contains a new class.

This file relies on definitions in `sdl.l` and must be therefore loaded after that one.
This file doesn't depend on `sdlutil.l`. For OpenGL rendering through SDL, you don't create `SDL_Surface`s or `SDL_Texture`s,
but only a `SDL_Window` and an OpenGL rendering context. All subsequent graphics processing is done using the OpenGL API. You can, however, load an image file using `sdlimage.l`, and using the returned `SDL_Surface` to create an OpenGL texture from it.
Being an SDL window, you can use the event handling functions defined in `sdl.l` and `sdlutil.`.

- `+OpenGLWindow`: This class makes it easy to create an `SDL_Window`, attach an OpenGL rendering context to it, and update the window contents at each frame.
It is very similar to the `+Window` class of `sdlutil.l`, but it is adapted to OpenGL. These are the differences.
    - Methods:
        - The class constructor `T` accepts an optional minimum OpenGL API version (if not specified, it defaults to OpenGL 2.1. It also accepts an optional list of window attributes and their values as cons pairs.
        - `swap>` (updates the window contents immediately)
        - `refresh>` (updates the window according to the frame rate specified using `framerate>`)
    - Properties:
        - `context` (the pointer to the OpenGL context of the window)
        - `vsync` (T if OpenGL rendering supports vertical synchronization)

### opengl3.l ###

This file has bindings to the OpenGL API, including the latest specification (shaders etc.).
Only the core profile functionality is there, but no extensions (yet).

The name of the library is stored under the global variable `*GLlib`. By default it's `"libGL.so"`.

#### A pointer problem ####

Many OpenGL functions use pointer arguments (including pointers to lists of pointers and such). That may be fine in C, but in Picolisp,
`native` allocates and releases the memory needed for those pointer arguments at every call.
When this is performed very frequently (for instance at every frame or 60 times per second!) it can cause an unwanted overhead and isn't convenient.
Therefore, some `native` calls to OpenGL functions that aren't time-critical (for instance `glShaderSource`) make use of `native`-style arguments instead of raw pointers. `glShaderSource` does not
take a pointer, but just a transient symbol, and lets `native` do the work of allocating a string buffer and passing the pointer to the OpenGL function.

The calls to other, time-critical functions (for instance `glGetUniformLocation` or `glUniform4fv`) use another approach that is closer to C than to Picolisp:
A buffer must be allocated before making the `native` call, for instance at the beginning
of the program. The buffer can be filled at every frame (using `struct`) and the pointer to the buffer is passed to the
`native` function, so that `native` doesn't have to allocate and free memory every time. Finally, the buffer must be freed before the program ends. This is how the `+Shader` class works.

All this means that, in the `opengl3.l` file, you'll find some functions that take their arguments exactly as the original OpenGL functions expect them, but for others,
you'll pass a normal transient symbol or a list and the function binding will take care of allocating the memory for the data, passing the pointer to the OpenGL function, and releasing the
memory afterwards. And in a few cases, you can find both alternatives. For instance,
`glBufferData` uses a `native` argument specification, but `glBufferDataP` takes a pointer to a buffer.

Work is being made to adapt as many functions as possible, so that they take their arguments in the most comfortable and time-efficient way, depending on their typical use.

See the file `nativearg.l` for more information.

### nativearg.l ###

In this file there are several convenient functions that create appropriate argument specifications for `native`, so that OpenGL functions that take pointers to numbers, lists and structures or
store values in pointer variables can work with Picolisp lists and symbols in a more
comfortable manner.

Some functions such as `glBufferData` are adapted to take arguments made with the
following functions:

- `int*` (converts a number into a pointer to an integer)
- `uint*` (converts a number into a pointer to an unsigned integer)
- `float*` (converts a fixed-point number into a pointer to a float)
- `ubytes*` (converts a list of numbers into a buffer of unsigned bytes)
- `uints*` (converts a list of numbers into a buffer of unsigned integers)
- `ints*` (converts a list of numbers into a buffer of signed integers)
- `floats*` (converts a list of fixed-point numbers into a buffer of floats)
- `doubles*` (converts a list of fixed-point numbers into a buffer of doubles)
- `strings*` (converts a list of pointers to strings (\*\* char), allocated using `getStrings*`, into a buffer of strings)
- `getInt*` (will return a number whose pointer (\* int) was passed to an OpenGL function which filled it)
- `getFloat*` (will return a fixed-point number whose pointer (\* float) was passed to an OpenGL function which filled it)
- `getInts*` (will return a buffer, filled by an OpenGL function, as a list)
- `getStrings*` (will return a list of pointers to strings, converted from transient symbols)

Some functions such as `glCreateShaderProgramv` and `glShaderSource` are adapted to directly accept strings. These are allocated using `getStrings*`, then passed
to the OpenGL function using `strings*`, and finally freed with `freeStrings*`, all inside the definition of the function binding.

Since `getStrings*` is an intermediate step before passing the string buffer via `native`,
you must call `freeStrings*` on the return value of `getStrings*` to de-allocate the strings.

### glconst.l ###

This file sets a list of global variables that correspond to some of the constants defined in the `glcorearb.h` header file.
They're all together in a separate file so they don't clutter `opengl3.l` and can be modified/completed more easily.

### shader.l ###

This file implements a new class for working with OpenGL shader programs.
It uses functions from `opengl3.l`, so it must always be loaded after loading `opengl3.l`.

- `+Shader`: This class wraps some methods for creating, compiling and using vertex and fragment shader programs.
- Create a shader with `(new '(+Shader) Vertex_Program Fragment_Program)`. Both arguments are transient symbols. The class constructor creates and tries to compile a shader program and quits the program with an error if it can't.
- Methods:
    - `use>` (tell OpenGL to use this shader program)
    - `newmat>` (allocate a new matrix structure and gives it a name)
    - `newarray>` (allocate a new array and gives it a name)
    - `setmat>` (set the contents of a matrix with the elements of a list of fixed-point numbers (floats))
    - `uniform>` (get a named `uniform` from the shader program)
    - `setint>, setfloat>, setvec>` (set the value of a `uniform`)
    - `setarrayf>`, `setarrayi>` (set the contents of an array with fixed-point numbers (floats) or integers, respectively)
    - `destroy>` (free all allocated matrices and arrays)

### transform.l ###

This file implements matrix and vector operations that are needed when working with OpenGL shaders and 3D geometry in general.

Matrices are 4 rows high and 4 columns wide.
Matrices are lists. Picolisp only has lists.
Matrix elements are stored in column major order (the first 4 elements are the first column,
elements 5-8 are the second column, elements 9-12 are the third column, and elements 13-16 are the fourth column).

- `identm` (return the identity matrix)
- `transm` (return a translation matrix)
- `remtransm` (return a matrix without the translation component)
- `scalem` (return a scale matrix)
- `rotxm, rotym, rotzm` (return a rotation matrix for X, Y and Z axes)
- `xy2yx` (return a transposed matrix)
- `matxmat` (return the multiplication of two matrices)
- `matxvec` (return the multiplication of a matrix by a vector)
- `sumv` (return the addition of two vectors)
- `subv` (return the subtraction two vectors)
- `dotv` (return the dot product of two vectors)
- `crossv` (return the cross product of two vectors)
- `normv` (return a normalized vector)
- `vecxnum` (return the multiplication of a vector by a number)
- `transv` (return the translation of a vector)
- `scalev` (return the scaling of a vector)
- `rotxv, rotyv, rotzv` (return the rotation of a vector on the X, Y and Z axes)
- `orthom` (return an orthographic projection matrix)
- `lookatm` (return a look-at view matrix)
- `perspm` (return a perspective projection matrix)
- `mvpm` (return the multiplication of a model, a view and a projection matrix)
- `radians` (convert degrees to radians)
- `degrees` (convert radians to degrees)
- `dirv` (return the direction of a vector)
- `lenv` (return the length of a vector)
- `invm` (return the inverse of a matrix)
- `+Camera` class that implements an FPS camera and calculates the needed view and projection matrices

### nativetrans.l ###

This file defines `native` bindings to the C library `libtransform.so` in
the `lib` folder.

In order to avoid the pointer stuff that is typical in C, `nativetrans.l`
operates on a buffer of indexed matrices (index starts at 0).
The functions take two arguments (a symbol holding the pointer to a matrix buffer
and an integer that is the index of the matrix or
matrices affected by the function), plus other necessary parameters.

As the matrix buffer is `malloc`ed when creating it with `newm`, don't forget to `freem` it before
quitting Picolisp.

### coords.l ###

This file implements a class for working with 2D rectangular coordinates.
The coordinates can be of any size, and they can be scaled down so they adapt
to e.g. the size of a graphics window.

Every instance holds the X and Y coordinates of the rectangle, as well as its width and height and the internal scale (number of decimal digits as in `*Scl`).

- `+Rect`
    - Create an instance with `(new '(+Rect) X Y Width Height Scale)`. `X` and `Y` are the top left corner of the rectangle. `Scale` is optional and defaults to 1.
    - Methods:
        - `set>` (sets the coordinates of the top left corner and updates the coordinates of the other corners)
        - `delta>` (increases or decreases the coordinates of the top left corner and updates the coordinates of the other corners)
        - `zoom>` (increases or decreases the coordinates of the top left corner and decreases or increases the coordinates of the other corners, thus increasing or reducing the size of the rectangle)
        - `randomX>, randomY>` (sets a random X or Y coordinate for the top left corner and updates the coordinates of the other corners)
        - `scalexywh>` (scales the coordinates down by the amount of the internal scale and returns the top left corner as well as the width and height of the rectangle in a list)
        - `enclose>` (resizes the coordinates so that the rectangle encloses a list of points)
        - `intersect>` (returns the position and dimensions of the overlapping area of two rectangles)
    - Properties:
        - `top, left, right, bottom, width, height` (the coordinates of the rectangle)
        - `scale` (the internal scale)

### loadsym.l ###

The files containing the bindings (sdl.l, sdlutil.l, opengl3.l etc.) are fairly large
and many symbols aren't needed in every program.

With the `loadsym` function you can take rid of the new symbols that are loaded
in a file but not used in your program, by removing them from the global namespace.

`loadsym` returns a list of four numbers: the number of initial symbols in the global
namespace, the number of newly loaded symbols, the number of new used symbols and the
number of stripped new symbols.

#### An example ####

If you load the `demo12.l` file, for example, you'll end up with more than 1500 new symbols in
the global namespace: lots of SDL and OpenGL functions and constants that the demo doesn't
really need. It might be useful to not clutter the namespace that much.

If you load the `loadsym.l` file and then use `(loadsym "demo12.l" main)`, you'll
see four numbers such as (965 1552 387 1166). 965 is the number of symbols in the
`'pico` namespace _before_ loading the demo file. 1552 is the number of new symbols
after loading the demo file (because `demo12.l` loads `transform.l`, `math.l`, `loadobj.l`,
`sdl.l`, `sdlopengl.l`, `opengl3.l`, `sdlimage.l` and `shader.l`). 387 is the number
of new symbols _actually_ referenced from the `main` function, and 1166 is the number
of new symbols _removed_ from the namespace because they're not used.

The program will run without problems.

### timing.l ###

The file defines to functions:

- `mbench` is like Picolisp's `bench` but it returns the number of elapsed microseconds.
- `secbench` is like `bench` but it is made to work in loops; it displays the number
of elapsed microseconds but only once every second.

### loadobj.l ###

The file defines the following functions:

- `loadobj` parses a simple Blender OBJ file and returns a list of three `native` arguments:
the coordinates of vertices, textures and normals, the indices of the vertices that build the
triangles of the mesh, and the amount of indices.
- `obj2bin` uses `loadobj` and saves its result in a file.
- `loadbin` reads a file created with `obj2bin`, so the calculations performed by `loadobj`
are already stored in the file and needn't be done again.

### ubo.l ###

- `std140` and `std140-array`: These functions make it easier to work with uniform blocks inside shaders and uniform buffer objects.

- `+UBO` : This class manages a uniform buffer object. It handles a `malloc`ed buffer for the data as well as the binding points of the shader programs. The data are stored using the
std-140 pattern. The +UBO class hasn't been tested yet.

### findlib.l ###

The file defines the function `findlib`, which uses the Linux utility `ldconfig` to locate a shared library in a Linux system, for instance `(findlib "libSDL2-2")`.

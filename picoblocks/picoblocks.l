# Always load this file after "sdl.l", "sdlutil.l", "sdlimage.l" and "sdlmixer.l"!!
 
(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/sdlmixer.l")
   
# GAME CONSTANTS

(de steps (Start N Step)
    (make (for (I 0 (<= I N) (inc I)) (link (+ Start (* I Step))))) )

(de gameDef ()
    (setq "Cols" 7      # 0..7=8 number of columns
          "Rows" 7      # 0..7=8 number of rows (but only 7 will be filled
          "NrStars" 5   # number of stars per block
          "Colors" 6    # 0..6=7 number of block sprites
          "NrSSpr" 3    # 0..3=4 number of star sprites
          "BSize" 48    # block sprite size
          "DSize" 24    # digit sprite size
          "ScrW" 640    # 640 window width
          "ScrH" 480    # 480 window height
          "Border" 16   # 16 width of the border around the playing area
          "BlockX" "Border" # X position of the left border of the playing area
          "BlockY" (- "ScrH" "Border" (* (inc "Rows") "BSize")) # Y position of the top border of the playing area
          "RightX" (+ (dec "BlockX") (* (inc "Cols") "BSize")) # right border of the playing area
          "BottomY" (+ (dec "BlockY") (* (inc "Rows") "BSize")) # bottom border of the playing area
          "BotY" (+ "BlockY" (* "Rows" "BSize")) # Y position of any block at the bottom of a column
          "InfoX" (+ "BlockX" "Border" (* (inc "Cols") "BSize") "Border") # X position of the left border of the information display
          "LevelY" (+ "BlockY" "BSize")
          "ScoreY" (+ "LevelY" (* 3 "BSize"))
          "BarY" (+ "ScoreY" (* 2 "BSize"))
          "BarW" (* 4 "BSize")
          "BarH" (/ "BSize" 2)
          "Xs" (steps "BlockX" "Cols" "BSize")      # list of column positions
          "MXs" (steps "BlockX" (inc "Cols") "BSize") # list of column positions for mouse pointer localization
          "MYs" (steps "BlockY" (inc "Rows") "BSize")      # list of row positions for mouse pointer localization
          "StarDir" (-5 -4 -3 -2 -1 1 2 3 4 5)      # star direction speed
          "StarVel" 1                               # star fading speed
          "SSize" 24                                # star sprite size
          "Level" 1                                 # first level
          "GMode" start
          *SDLStop NIL
          "FRate" 60 ) )                            # frame rate

# GAME LOGIC

# These functions use symbols defined in (gameDef) and (level N)

# a block is an anonymous symbol
# properties: x, y=position, c=color, t=target y position, s=speed,
# f=fall status (1 if it must fall, otherwise f isn't present)

# is there enough room for a block at C R?

(de colfree (C R) (> (cadr (assoc C "Coltop")) R))

# what columns have some free room?

(de freecol () (filter '((C) (> (cadr C) "BlockY") ) "Coltop"))

# free room on top of column C

(de target (C) (cadr (assoc C "Coltop")))

# decrease free room on top of column C

(de uptarg (C)
    (let R (cdr (assoc C "Coltop"))
        (when (> (car R) "BlockY") (dec R "BSize")) ) )

# updates falling target at column C so that holes are filled
# blocks over the hole must be set to fall

(de falltrg (C Y)
    (let R (cdr (assoc C "Coltop"))
        (when (< (car R) Y) (set R Y)) ) )

# move blocks that are set to fall

(de moveb ()
    (use Ny
        (for B "Blocks"
            (when (get B 'f)
                (setq Ny (+ (get B 'y) (get B 's)))
                (if (>= Ny (get B 't))              # block reaches its target
                    (prog                           # don't move it anymore
                        (put B 'y (get B 't))
                        (put B 'f NIL)
                        (matches B) )               # check for matches
                    (put B 'y Ny)                   # block keeps falling
                    (inc (prop B 's)) ) ) ) ) )     # with increasing speed

# get adjacent blocks of the same color

(de matches (A)
    (off "Checked")                                 # blocks are checked only once
    (let (M (make (domatch A)) L (length M))
        (when (>= L "NrMatch")                      # nr. of matches must be >= "NrMatch"
            (score+ L)                              # add score
            (stars M)                               # make star effect
            (or
                (setq "Blocks" (diff "Blocks" M))   # delete matching blocks
                (setq "GMode" lvlDone) )            # if no more blocks, change to "level complete"
            # update target positions so that blocks over the matched
            # blocks will fill the holes
            (for X M (falltrg (get X 'x) (get X 'y)))
            # find the blocks over the matched blocks and make them fall
            (mapc dofall (bot2top (uniq (mapcan over M)))) ) ) )

# check recursively for blocks with the same color as block X
# either left, right, on or under

(de domatch (X)
    (when (and X (not (member X "Checked")))
        (push '"Checked" X)
        (link X)
        (domatch (getb X (- "BSize") 0))
        (domatch (getb X "BSize" 0))
        (domatch (getb X 0 (- "BSize")))
        (domatch (getb X 0 "BSize")) ) )

# get the block at position C R and return it only if its color
# is the same as G's

(de getb (G X Y)
    (find '((B)
        (and
            (not (get B 'f))
            (= (+ (get G 'x) X) (get B 'x))
            (= (+ (get G 'y) Y) (get B 'y))
            (= (get G 'c) (get B 'c)) ) )
        "Blocks" ) )

# order blocks from lowest to highest

(de bot2top (L) (sort L '((A B) (> (get A 'y) (get B 'y)))))

# get blocks over B, ordered from bottom up

(de over (B)
    (filter '((N)
        (and (= (get B 'x) (get N 'x)) (> (get B 'y) (get N 'y))) )
        "Blocks" ) )

# set block to fall

(de dofall (A)
    (unless (get A 'f)              # block is already falling
        (put A 'f 1)                # make block fall
        (put A 's 0) )
    (put A 't (target (get A 'x)))  # set its target position
    (uptarg (get A 'x)) )           # decrease target position for the column

# get a random item from a list
        
(de random (L) (get L (rand 1 (length L))))

# create a new block at position C R

(de newb (C R)
    (let B (box)
        (put B 'f 1)
        (put B 'x C)
        (put B 'y R)
        (put B 'c (* "BSize" (rand 0 "Colors")))
        (put B 't (target C))
        (put B 's 0)
        (uptarg C)
        (push '"Blocks" B) ) )

# calculates the score based on the amount of matched blocks

(de score+ (N)
    (play> "ScSnd")
    (inc '"Score" (* "Level" 10 N))
    (setq "Infosco" (str2spr "Score")) )

# turns a number into a sequence of digit sprites

(de str2spr (N)
    (flip
        (make
            (while (gt0 N)
                (link (* "DSize" (% N 10)))
                (setq N (/ N 10)) ) ) ) )

# create N new blocks at random positions

(de startb (N)
    (use (S X Y)
        (do N
            (until (and X Y (not (member (cons X Y) S)))
                (setq X (+ "BlockX" (* "BSize" (rand 0 "Cols")))
                      Y (+ "BlockY" (* "BSize" (rand 0 "Rows"))) ) )
                (push 'S (cons X Y)) )
        (mapcan '((B) (newb (car B) (cdr B))) (reverse (by cdr sort S))) ) )

# put a new random block at the top of a free column
# in "and", @ holds the result of the previous expression

(de putnewb ()
    (let? F (freecol)
        # never put the new block at the column where the mouse pointer is
        (if (cdr F)
            (newb (car (random (filter '((C) (<> (car C) "MouseX")) F))) "BlockY")
            (newb (caar F) "BlockY") ) ) )
    
# pick a drop using the mouse

(de pickb ()
    (when
        (and "MouseX"
             "MouseY"
             (find '((B)
                (and
                    (= "MouseX" (get B 'x))
                    (= "MouseY" (get B 'y)) ) )
                "Blocks" ) )
        (setq "Picked" @)
        (del @ '"Blocks")
        (falltrg (get @ 'x) (get @ 'y))
        (mapc dofall (bot2top (over @))) ) )

# drop the picked block at the column where the mouse cursor is

(de dropb ()
    (and "MouseX"
        (colfree "MouseX" "BlockY")
        (put "Picked" 'x "MouseX")
        (put "Picked" 'y "BlockY")
        (put "Picked" 't (target "MouseX"))
        (put "Picked" 'f 1)
        (put "Picked" 's 0)
        (push '"Blocks" "Picked")
        (uptarg "MouseX")
        (off "Picked") ) )

# create stars from the blocks in the list
# a star has the following properties: x and y=position,
# n=offset in the spritesheet, vx and vy=horizontal and vertical velocity,
# alpha=alpha value

(de stars (L)
    (use (S X Y)
        (for F L
            (setq X (+ (get F 'x) (/ "SSize" 2))
                  Y (+ (get F 'y) (/ "SSize" 2)) )
            (do "NrStars"
                (setq S (box))
                (put S 'x X)
                (put S 'y Y)
                (put S 'n (* "SSize" (rand 0 "NrSSpr")))
                (put S 'vx (random "StarDir"))
                (put S 'vy (random "StarDir"))
                (put S 'alpha (rand 100 200))
                (push '"Stars" S) ) ) ) )

(de moves ()
    # do something only if there are stars visible
    (and "Stars"
        # remove the stars that are no longer visible
        # (out of the window bounds or with 0 alpha value)
        (setq "Stars" (filter '((S) (gt0 (get S 'alpha))) "Stars")) )
    (for S "Stars"
        (inc (prop S 'x) (get S 'vx))       # update the position
        (inc (prop S 'y) (get S 'vy))       
        (dec (prop S 'alpha) "StarVel") ) ) # update the alpha value
    
# LEVEL PARAMS

# initialize a list of (column_position target_position)

(de tops () (mapcar '((V) (list V "BotY")) "Xs"))

# prepare a level

(de level (N)
    (setq "Level" N
          "Infolvl" (str2spr N)
          "Score" (if (= 1 N) 0 "Score")
          "NrMatch" (+ 3 (/ N 2))       # minimum amount of matching blocks to score
          "NBDelay" (- 95 (* 5 N))      # frames between new blocks
          "LvlTime" (+ 40 (* 40 N))  # number of new blocks until level complete
          "HUTime"  (/ "LvlTime" 2)      # number of new blocks until hurry up mode
          "HUDelay" (/ "NBDelay" 2)     # frames between new blocks in hurry up mode
          "Blocks" NIL                  # list of fixed blocks
          "Coltop" (tops)               # list of positions to fall onto at each column
          "Fallcol" NIL
          "Stars" NIL                   # list of stars
          "Picked" NIL                  # block picked with the mouse
          "MouseX" NIL
          "MouseY" NIL
          "BarStep" (*/ 100 "BarW" "LvlTime")
          "Count" 0                     # frame counter
          "GMode" newLvl )              # loop function
    (startb (+ 5 (* 5 (% N 3)))) )      # starting blocks
      
# GRAPHICS FUNCTIONS

(de graphic (S) (pack "gfx/blocks_" S ".png"))

(de sound (S) (pack "music/blocks_" S))
        
# convert mouse coordinates to block column and row

(de m2w (X Y)
    (if (and
            (<= "BlockX" X "RightX")
            (<= "BlockY" Y "BottomY") )
        (setq "MouseX"
            (find '((A B) (<= A X (inc B))) "MXs" (cdr "MXs"))
              "MouseY"
            (find '((A B) (<= A Y (inc B))) "MYs" (cdr "MYs")) )
        (off "MouseX" "MouseY") ) )
        
# draw all the blocks

(de drawb ()
    (with "BImg"
        (for B "Blocks"
            (srcxy> This (get B 'c) 0)
            (xy> This (get B 'x) (get B 'y))
            (copyto> This "Ren") ) ) )
            
# draw all the stars

(de draws ()
    (with "SImg"
        (for S "Stars"
            (srcxy> This (get S 'n) 0)
            (xy> This (get S 'x) (get S 'y))
            (alpha> This (get S 'alpha))
            (copyto> This "Ren") ) ) )

# draw the picked block

(de drawp ()
    (with "BImg"
        (srcxy> This (get "Picked" 'c) 0)
        (xy> This "InfoX" "BotY")
        (copyto> This "Ren") ) )

# draw the level and the score

(de drawi ()
    (let X "InfoX"
        (with "Digits"
            (for D "Infolvl"
                (srcxy> This D 0)
                (xy> This X "LevelY")
                (copyto> This "Ren")
                (inc 'X "DSize") )
            (setq X "InfoX")
            (for D "Infosco"
                (srcxy> This D 0)
                (xy> This X "ScoreY")
                (copyto> This "Ren")
                (inc 'X "DSize") ) ) )
    (copyallto> "Bar" "Ren") )

# update bar width

(de barw ()
    (setdest> "Bar" "InfoX" "BarY" (*/ "BarStep" "LvlTime" 100) "BarH") )
    
# INITIALIZATION AND SHUTDOWN

(de makeBackground ()
    (let (B2 (new '(+SprImg) (graphic 'background))         # background picture
          LL (new '(+SprImg) (graphic 'level))              # level logo
          LS (new '(+SprImg) (graphic 'score))              # score logo
          B (new '(+Surface) "ScrW" "ScrH" NIL              # screen background
                (sdlGetSurfacePixelFormatVal (get B2 'surf)) )
          S (new '(+Surface) 1 1 NIL                        # block field
                (sdlGetSurfacePixelFormatVal (get B 'surf)) ) )
        (with B2
            # The background picture is 1920x1080 pixels big
            # Select a random rectangle as big as the window
            (setsrc> This (rand 0 (- 1920 "ScrW"))
                (rand 0 (- 1080 "ScrH")) "ScrW" "ScrH" )
            # and copy to the background
            (setdest> This 0 0 "ScrW" "ScrH")
            (blitto> This (get B 'surf))
            # we don't need the original picture anymore
            (destroy> This) )
        (with S
            (let (Wi (inc "Cols") He (inc "Rows"))
                # put two rectangles on the background to enclose the block field
                (sdlFillSurfaceRGB (: surf) 200 200 200)
                (blend> This `SDL_BLENDMODE_MOD)
                (alpha> This 200)
                (setsrc> This 0 0 1 1)
                (setdest> This (- "BlockX" 16) (- "BlockY" 16)
                    (+ (* Wi "BSize") 32) (+ (* He "BSize") 32) )
                (sdlBlitScaled (: surf) (: src) (get B 'surf) (: dest))
                (alpha> This 140)
                (setdest> This "BlockX" "BlockY" (* Wi "BSize")
                    (* He "BSize") )
                (sdlBlitScaled (: surf) (: src) (get B 'surf) (: dest))
                (destroy> This) ) )
        (with LL
            # paint the Level logo
            (setdest> This "InfoX" "BlockY" (* 4 "BSize") "BSize")
            (blitallto> This (get B 'surf))
            (destroy> This) )
        (with LS
            # paint the Score logo
            (setdest> This "InfoX" (+ "BlockY" (* 3 "BSize"))
                (* 4 "BSize") "BSize" )
            (blitallto> This (get B 'surf))
            (destroy> This) )
        (with B
            # prepare the background
            (setdest> This 0 0 "ScrW" "ScrH")
            (totexture> This "Win") )
        B ) )
        
(de setup ()
    (seed (time))
    (sdlInitApp (| SDL_INIT_VIDEO SDL_INIT_AUDIO))
    (imgInit IMG_INIT_PNG)
    (setq
        "Win" (new '(+RenderWindow) "PicoBlocks" "ScrW" "ScrH")
        "Ren" (get "Win" 'renderer)
        "Bgnd" (makeBackground)
        "Bar" (new '(+Surface) 1 1 "Win") )
    (mapc '((K V)
        (set K (new '(+SprImg) (graphic V)))
        (totexture> (val K) "Win") )
        '("HULogo" "GoLogo" "GaOLogo" "StLogo"
          "WDLogo" "Digits" "BImg" "SImg")
        '(hurryup go gameover start welldone digits blocks stars) )
    (with "BImg" # block spritesheet
        (setsrc> This 0 0 "BSize" "BSize")
        (setdest> This 0 0 "BSize" "BSize") )
    (with "Bar" # level time bar
        (sdlFillSurfaceRGB (: surf) 255 255 0)
        (totexture> This "Win")
        (alpha> This 200)
        (blend> This SDL_BLENDMODE_BLEND)
        (setdest> This "InfoX" "BarY" "BarW" "BarH") )
    (with "HULogo" # hurry up logo sprite
        (setdest> This "BlockX" 0 (* 8 "BSize") (* 2 "BSize")) )
    (mapc '((K) (setdest> K "BlockX" (- (/ "ScrH" 2) "BSize")
        (* 8 "BSize") (* 2 "BSize") ) )
        (list "GoLogo" "GaOLogo" "WDLogo") )
    (with "StLogo" # start logo sprite
        (setdest> This (- (/ "ScrW" 2) (* 4 "BSize"))
            (- (/ "ScrH" 2) "BSize") (* 8 "BSize")
            (* 2 "BSize") ) )
    (with "Digits" # digit spritesheet
        (setsrc> This 0 0 "DSize" "BSize")
        (setdest> This 0 0 "DSize" "BSize") )
    (with "SImg" # star spritesheet
        (setsrc> This 0 0 "SSize" "SSize")
        (setdest> This 0 0 "SSize" "SSize")
        (blend> This SDL_BLENDMODE_BLEND) )
    (with "Win" # window
        (framerate> This "FRate")
        (rendercolor> This 0 0 0 0) )
    (mixInit MIX_INIT_MP3)
    (mixOpenAudio)
    (setq "WDSnd" (new '(+Chunk) (sound "levelup.wav"))      # level complete sound
          "ScSnd"  (new '(+Chunk) (sound "match.wav"))       # match sound
          "Music" (new '(+Music) (sound "music1.mp3"))       # music in normal mode
          "HUMusic" (new '(+Music) (sound "music2.mp3")) ) ) # music in hurry up mode

(de cleanup ()
    (mixHaltMusic)
    (mapc '((K) (destroy> K))
        (list "Music" "ScSnd" "WDSnd" "HUMusic"
          "HULogo" "GoLogo" "GaOLogo" "StLogo"
          "WDLogo" "Digits" "BImg" "Bar" "Bgnd" "SImg"
          "Win" ) )
    (mixCloseAudio)
    (imgQuit)
    (sdlQuitApp) )

# GAME LOOP

(de sdlEventLoop ()
    # Main application loop:
    (until *SDLStop
        # Take events from the event queue, until it is empty
        # (i.e. sdlPollEvent returns 0)
        (until (=0 (sdlPollEvent))
            # Respond to an event.
            # sdlHandleEvents depends on the user
            # having defined *SDLActions.
            (sdlHandleEvents) )
        # Animate a frame.
        (run "GMode") ) ) # run a frame animation function

(def '*SDLActions
    (quote
        (`SDL_QUIT
            (on *SDLStop) )
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            # The 8th field of *SDLEventInfo holds now the key code
            # 41 = Escape key
            (case (get *SDLEventInfo 8)
                (41 (if (= "GMode" start)   # Pressing Escape start mode will quit the program
                        (on *SDLStop)
                        (mixHaltMusic)      # otherwise, it will return to start mode
                        (mixRewindMusic)
                        (setq "GMode" start) ) )
                (T  (when (= "GMode" start) # Pressing any other key in start mode will begin the game
                        (level 1) ) ) ) )
        (`SDL_MOUSEBUTTONDOWN
            (sdlMapMouseButtonEvent)
            # the 9th and 10th fields of *SDLEventInfo hold the
            # X and Y position of the mouse
            (if "Picked" (dropb) (pickb)) )
        (`SDL_MOUSEMOTION
            (sdlMapMouseMotionEvent)
            # the 6th and 7th fields of *SDLEventInfo hold the
            # X and Y position of the mouse
            (apply m2w (head 2 (nth *SDLEventInfo 6))) ) ) )

# checks for game state changes

(de gamest ()
    (inc '"Count")
    (when (>= "Count" "NBDelay")
        (zero "Count")
        (ifn (putnewb)
            (setq "GMode" gameOvr)
            (dec '"LvlTime")
            (barw)
            (cond
                ((le0 "LvlTime") (setq "GMode" lvlDone))
                ((= "LvlTime" "HUTime")
                    (mixHaltMusic)
                    (play> "HUMusic")
                    (setq "GMode" hurryup) ) ) ) ) )

# animation loop in normal mode

(de play ()
    # (matches) checks if there are no more blocks on the screen
    # and sets the loop mode to lvlDone
    (gamest)
    (moveb)
    (moves)
    (copyallto> "Bgnd" "Ren")
    (drawb)
    (drawi)
    (and "Stars" (draws))
    (and "Picked" (drawp))
    (refresh> "Win") )

# checks for game state changes in hurry up mode

(de hupst ()
    (inc '"Count")
    (when (>= "Count" "HUDelay")
        (zero "Count")
        (ifn (putnewb)
            (setq "GMode" gameOvr)
            (dec '"LvlTime")
            (barw)
            (and (le0 "LvlTime") (setq "GMode" lvlDone)) ) ) )

# animation loop in hurry up mode

(de hurryup ()
    # (matches) checks if there are no more blocks on the screen
    # and sets the loop mode to lvlDone
    (hupst)
    (moveb)
    (moves)
    (copyallto> "Bgnd" "Ren")
    (drawb)
    (drawi)
    (and "Stars" (draws))
    (and "Picked" (drawp))
    (copyallto> "HULogo" "Ren")
    (refresh> "Win") )
    
# new level screen

(de newLvl ()
    (play> "Music" -1)
    (do 4
        (do (/ "FRate" 2)
            (copyallto> "Bgnd" "Ren")
            (copyallto> "GoLogo" "Ren")
            (refresh> "Win") )
        (do (/ "FRate" 2)
            (copyallto> "Bgnd" "Ren")
            (refresh> "Win") ) )
    (setq "GMode" play) )

# "well done" (level complete) screen

(de lvlDone ()
    (mixHaltMusic)
    (play> "WDSnd")
    (do (* "FRate" 5)
        (moveb)
        (moves)
        (copyallto> "Bgnd" "Ren")
        (drawb)
        (drawi)
        (and "Stars" (draws))
        (and "Picked" (drawp))
        (copyallto> "WDLogo" "Ren")
        (refresh> "Win") )
    (level (inc "Level")) )

# game over screen

(de gameOvr ()
    (blend> "Bgnd" `SDL_BLENDMODE_BLEND)
    (for (I 255 (ge0 I) (dec I))
        (clear> "Win")
        (alpha> "Bgnd" I)
        (copyallto> "Bgnd" "Ren")
        (drawi)
        (copyallto> "GaOLogo" "Ren")
        (refresh> "Win") )
    (alpha> "Bgnd" 255)
    (blend> "Bgnd" `SDL_BLENDMODE_NONE)
    (mixFadeOutMusic 2000)
    (sdlDelay 2000)
    (setq "GMode" start) )

# game start screen

(de start ()
    (clear> "Win")
    (copyallto> "StLogo" "Ren")
    (refresh> "Win") )

# MAIN FUNCTION

(de main ()
    (gameDef)
    (setup)
    (sdlEventLoop)
    (cleanup) )


# Display a square (two triangles) on a SDL2 window, using
# OpenGl 3.3 shaders

(scl 3)

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l")

(de main ()
    (sdlInitApp SDL_INIT_VIDEO)
    (setq
        W (new '(+OpenGLWindow) "SDL Test 4" 400 400 3 3)
        Vertices (floats* 10
            (-5 -5 0 10  0  0
              5 -5 0  0 10  0
              5  5 0  0  0 10
             -5  5 0 10 10 10 ) )
        Elements (ubytes* (0 1 3 1 2 3))
        Program (glCreateProgram)
        VShader (glCreateShader GL_VERTEX_SHADER)
        FShader (glCreateShader GL_FRAGMENT_SHADER)
        VAO (glGenVertexArrays 1)
        VBO (glGenBuffers 1)
        EBO (glGenBuffers 1)
        VShaderProg
            "#version 330 core
            layout (location = 0) in vec3 aPos;
            layout (location = 1) in vec3 aColor;
            out vec3 ourColor;
            void main()
            {
                gl_Position = vec4(aPos, 1.0);
                ourColor = aColor;
            }"
        FShaderProg
            "#version 330 core
            out vec4 FragColor;
            in vec3 ourColor;
            void main()
            {
                FragColor = vec4(ourColor, 1.0f);
            }" )
    (glShaderSource VShader VShaderProg)
    (glCompileShader VShader)
    (unless (compiled? VShader) (quit "Vertex shader not compiled"))
    (glShaderSource FShader FShaderProg)
    (glCompileShader FShader)
    (unless (compiled? FShader) (quit "Fragment shader not compiled"))
    (glAttachShader Program VShader)
    (glAttachShader Program FShader)
    (glLinkProgram Program)
    (unless (linked? Program) (quit "Shader program not linked"))
    (glDeleteShader VShader)
    (glDeleteShader FShader)
    (glBindVertexArray VAO)
    (glBindBuffer GL_ARRAY_BUFFER VBO)
    (glBufferData GL_ARRAY_BUFFER Vertices GL_STATIC_DRAW)
    (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
    (glBufferData GL_ELEMENT_ARRAY_BUFFER Elements GL_STATIC_DRAW)
    (glVertexAttribPointer 0 3 GL_FLOAT 0 24 0)
    (glEnableVertexAttribArray 0)
    (glVertexAttribPointer 1 3 GL_FLOAT 0 24 12)
    (glEnableVertexAttribArray 1)
    (glClearColor 0.2 0.3 0.3 1.0)
    (glClear GL_COLOR_BUFFER_BIT)        
    (glUseProgram Program)
    (glBindVertexArray VAO)
    (glDrawElements GL_TRIANGLES 6 GL_UNSIGNED_BYTE 0)
    (swap> W)
    (sdlDelay 3000)
    (destroy> W)
    (sdlQuitApp) )


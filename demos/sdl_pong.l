# Always load this file after loading
# "sdl.l", "sdlutil.l", "sdlimage.l", "sdl_score.l" and "coords.l"!

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/sdl_score.l"
   "@sdl/coords.l" )

(class +Game +RenderWindow)

    (dm T (IW IH)
        (default IW 1000 IH 500)
        (super "Pong" IW IH)
        (=: coords (new '(+Rect) 0 0 (* IW 10) (* IH 10) 10))
        (=: pause T)
        (=: score 0)
        (=: lives 10)

        (let (PadW (/ IH 20) PadH (/ IH 8))
            (=: pad (new '(+Surface) PadW PadH This))
            (sdlFillSurfaceRGB (: pad surf) 255 255 255)
            (setsrc> (: pad) 0 0 PadW PadH)
            (setdest> (: pad) 0 0 PadW PadH)
            (=: pad coords (new '(+Rect) 0 0 (* 10 PadW) (* 10 PadH) 10)) )
            (=: pad dir 0)
        (totexture> (: pad) This)
        
        (let (BalW (/ IH 25) S (new '(+Sprite) "gfx/ball.bmp" This))
            (=: ball (new '(+Surface) BalW BalW This))
            (setdest> (: ball) 0 0 BalW BalW)
            (sdlBlitScaled (get S 'surf) 0 (: ball surf) (: ball dest))
            (=: ball coords (new '(+Rect) 10 0 (* 10 BalW) (* 10 BalW) 10))
            (destroy> S) )
        (colorkey> (: ball) T 0 0 0)
        (totexture> (: ball) This)
        (blend> (: ball) SDL_BLENDMODE_BLEND)
        
        (=: panel (new '(+Score) "gfx/digitmap.png" 32 64 This)) )
    
    (dm startpos> ()
        (randomY> (: pad coords) (: coords bottom) (* 8 (/ (: coords width) 10)))
        (=: pad speed 40)
        (randomY> (: ball coords) (: coords bottom) 10)
        (=: ball dx 40)
        (=: ball dy (rand -40 40))
        (=: points (* (: ball dx) (abs (: ball dy)))) )

    (dm translate> ()
        (apply send (cons 'setdest> (: pad) (scalexywh> (: pad coords))))
        (apply send (cons 'setdest> (: ball) (scalexywh> (: ball coords)))) )
    
    (dm reactcoll> ()
        (cond
            ((and
                (>= (: ball coords bottom) (: pad coords top) (: ball coords top))
                (>= (: ball coords right) (: pad coords left) (: ball coords left)) )
                (=: ball dx (- (abs (: ball dx))))
                (=: ball dy (- (abs (: ball dy))))
                (inc (:: score) (: points)) )
            ((and
                (>= (: ball coords bottom) (: pad coords bottom) (: ball coords top))
                (>= (: ball coords right) (: pad coords left) (: ball coords left)) )
                (=: ball dx (- (abs (: ball dx))))
                (=: ball dy (abs (: ball dy)))
                (inc (:: score) (: points)) )
            ((and
                (<= (: pad coords top) (: ball coords top) (: ball coords bottom) (: pad coords bottom))
                (>= (: pad coords right) (: ball coords right) (: pad coords left) (: ball coords left)) )
                (=: set> (: ball) (- (: pad coords left) (: ball coords width)) (: ball coords top))
                (=: ball dx (- (abs (: ball dx))))
                (inc (:: score) (: points)) )
            ((>= (: ball coords bottom) (: coords bottom))
                (=: ball dy (- (abs (: ball dy)))) )
            ((<= (: ball coords top) (: coords top))
                (=: ball dy (abs (: ball dy))) )
            ((> (: ball coords right) (: coords right))
                (dec (:: lives))
                (startpos> This) )
            ((le0 (: ball coords left))
                (=: ball dx (abs (: ball dx))) ) ) )
    
    (dm padup> ()
        (if (le0 (: pad coords top))
            (=: pad dir 0)
            (delta> (: pad coords) 0 (- (: pad speed))) ) )

    (dm paddown> ()
        (if (>= (: pad coords bottom) (: coords bottom))
            (=: pad dir 0)
            (delta> (: pad coords) 0 (: pad speed))) )
    
    (dm moveball> () (delta> (: ball coords) (: ball dx) (: ball dy)))

    (dm destroy> ()
        (destroy> (: pad))
        (destroy> (: ball))
        (destroy> (: panel))
        (super) )

(de main ()
    (sdlInitApp SDL_INIT_VIDEO)
    (setq *G (new '(+Game) 500 500))
    (framerate> *G 60)
    (startpos> *G)
    (sdlEventLoop frame)
    (prinl "Game over. Total score: " (get *G 'score))
    (destroy> *G)
    (sdlQuitApp) )

(de frame ()
    (with *G
        (if (=0 (: lives))
            (on *SDLStop) )
            (case (: pad dir)
                (1 (padup> This))
                (2 (paddown> This)) )
            (reactcoll> This)
            (moveball> This)
            (translate> This)
            (clear> This)
            (number> (: panel) (: score) 0 0)
            (copyallto> (: pad) (: renderer))
            (copyallto> (: ball) (: renderer))
            (refresh> This) ) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop))
                (82 (put *G 'pad 'dir 1))
                (81 (put *G 'pad 'dir 2)) ) ) ) )


# OpenGL app template

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/opengl3.l"
   "@sdl/sdlopengl.l" "@sdl/transform.l" "@sdl/shader.l" )
      
(de startup (Width Height)
   (default Width 640 Height 480)
   (sdlInitApp SDL_INIT_VIDEO)
    # Create an 800x800 pixel SDL window with an attached OpenGL context
   (setq *Window (new '(+OpenGLWindow) "SDL + OpenGL App" Width Height 3 3
      '((SDL_GL_DOUBLEBUFFER . 1)) ) )
   
   # You can pass GL attributes with a list of cons pairs, like so:
   #   '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
   #    (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 8)
   #    (SDL_GL_DOUBLEBUFFER . 1))
)

(de shutdown ()
   (destroy> *Window)
)

(de frame ()
   (swap> *Window)
)

# Respond to events
(def '*SDLActions
   (quote
      # If a key was pressed
      (`SDL_KEYDOWN
         # Get the event information
         (sdlMapKeyboardEvent)
         # If the key code is Escape (#41), then stop the event loop
         (case (get *SDLEventInfo 8)
            (41 (on *SDLStop)) ) )
   )
)

(de main ()
   (startup)
   (sdlEventLoop frame)
   (shutdown) )


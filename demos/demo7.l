# Display a cube (12 triangles) on a SDL2 window, using
# OpenGl 3.3 shaders, and rotating it around all axis
# by applying a model-view-projection matrix with an
# OpenGl uniform.

# It uses the +Shader class and the transform.l file

(scl 3)

(load "@sdl/timing.l" "@sdl/sdl.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l"
   "@sdl/transform.l" "@sdl/shader.l" )

(de main ()

    (sdlInitApp SDL_INIT_VIDEO)
    
    (let (W (new '(+OpenGLWindow) "SDL Test 7" 600 600 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 16)
              (SDL_GL_DOUBLEBUFFER . 1) ) )
              
          Shader (new '(+Shader)
          
            "#version 330 core
            layout (location = 0) in vec3 aPos;
            layout (location = 1) in vec3 aColor;
            out vec3 ourColor;
            uniform mat4 trf;
            void main()
            {
                gl_Position = trf * vec4(aPos, 1.0f);
                ourColor = aColor;
            }"
            
            "#version 330 core
            out vec4 FragColor;
            in vec3 ourColor;
            void main()
            {
                FragColor = vec4(ourColor, 1.0f);
            }" )
            
          Vertices (floats* 10 # This 10 is the fixpoint scale of the elements
            (-5 -5  5 10  0  0
              5 -5  5  0 10  0
              5  5  5  0  0 10
             -5  5  5 10 10 10
              5 -5 -5 10 10  0
             -5 -5 -5  0 10 10
             -5  5 -5 10  0 10
              5  5 -5 10 10 10 ) )
              
          Elements (ubytes*
            (0 1 3 1 2 3 4 5 6 4 6 7 1 4 2 4 7 2
             5 3 6 5 0 3 0 1 5 1 4 5 7 6 2 6 3 2 ) )
            
          Angles (0 0 0) # Z Y X
          
          View (transm 0 0 -2.5)
          Persp (perspm 45.0 1.0 0.1 100.0)
          VP (matxmat Persp View)

          Clear (| GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT)

          VAO (glGenVertexArrays 1)
          VBO (glGenBuffers 1)
          EBO (glGenBuffers 1) )
          
        (glBindVertexArray VAO)
        
        (glBindBuffer GL_ARRAY_BUFFER VBO)
        (glBufferData GL_ARRAY_BUFFER Vertices GL_STATIC_DRAW)
        (glVertexAttribPointer 0 3 GL_FLOAT 0 24 0) # float = 4 bytes
        (glEnableVertexAttribArray 0)
        (glVertexAttribPointer 1 3 GL_FLOAT 0 24 12)
        (glEnableVertexAttribArray 1)
        
        (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
        (glBufferData GL_ELEMENT_ARRAY_BUFFER Elements GL_STATIC_DRAW)
        
        (glEnable GL_DEPTH_TEST)
        (glClearColor 0.2 0.3 0.3 1.0)
        
        (newmat> Shader 4 'matrix)
        (uniform> Shader "trf")
        (use> Shader)
        
        (sdlEventLoop frame)
        
        (destroy> Shader)
        (destroy> W)
        
        (sdlQuitApp) ) )
    
(de frame ()
    (glClear Clear)
    (secbench
       (setq Angles (mapcar '((X V) (inc 'X V)) Angles '(0.005 0.011 0.016)))
       (setq Model (multm (rotzm (car Angles)) (rotym (cadr Angles))
           (rotxm (caddr Angles)) ) ) )
    (setmat> Shader 4 "trf" 'matrix (matxmat VP Model))
    (glDrawElements `GL_TRIANGLES 36 `GL_UNSIGNED_BYTE 0)
    (swap> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )


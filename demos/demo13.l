# demo13.l: a landscape with a height map

(scl 5)

(once (load "@sdl/transform.l" "@sdl/loadobj.l"
   "@sdl/sdl.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l"
   "@sdl/sdlimage.l" "@sdl/shader.l" ) )

# Create a flat terrain mesh, from (x = -1, y = -1) to (x = 1, y = 1)
# with NumX x NumY triangles. The values can be passed to functions such as
# glBufferData using the appropriate 'native' argument specification or
# with the help of the functions in the file 'nativearg.l', such as 'floats*'.

(de terrain-mesh (NumX NumY)
   (let (SX (/ 2.0 NumX) SY (/ 2.0 NumY))
      (make
         (for Y (range 1.0 -1.0 SY)
            (for X (range -1.0 1.0 SX)
               (link X Y) ) ) ) ) )

# Take a mesh made with 'terrain-mesh' as well as the number of triangle rows
# and columns, and create a list of indices that can be uploaded to an element
# buffer object and renderer with a single draw call as a GL_TRIANGLE_STRIP.

(de terrain-indices (Mesh NumX NumY)
   (let O 0
      (make
         (for Y NumY
            (for X (range 0 NumX)
               (link (+ O X) (+ O X NumX 1)) )
            (unless (= Y NumY)
               (link (+ O NumX NumX 1)
                     (+ O NumX 1) ) )
            (inc 'O (inc NumX)) ) ) ) )

# Update the +Camera parameters according to mouse position and buttons

(de set-camera (DX DY Button)
   (case Button
      (1                                              # left mouse button is pressed: move the camera
         (if (> (abs DX) (abs DY))
            (move> *Camera 'left (* *Vel DX))         # depending on the sign of DX or DY,
            (move> *Camera 'down (* *Vel DY)) ) )     # the movement will be in the opposite direction
      (4                                              # right mouse button: rotate the camera
         (rot> *Camera (* *Vel 10 DX) (* *Vel 10 DY)) ) )
         # update the projection-view matrix of the camera:
         # multiply the projection matrix by the view matrix
   (setq *CamVP (matxmat (proj> *Camera) (view> *Camera))) )

# Load a 2D texture, generate mip maps if specified and bind it to a texture unit in
# a shader program
# The internal texture format can be specified with Format
# and the format of the image can be given with Type

(de load-texture (Unit ID File Shader Uniform Format Type Unpack Mips)
   (default Format GL_RGBA8
            Type GL_RGB
            Unpack 4
            Mips 0 )
   (let (Sur (imgLoad File)                                    # Use SDL2_image to load the image
         Inf (surfstruct Sur) )                                # Get the SDL_SURFACE structure
      (glActiveTexture Unit)
      (glBindTexture GL_TEXTURE_2D ID)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER
         (if (n0 Mips) GL_NEAREST_MIPMAP_NEAREST GL_LINEAR) )
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_BASE_LEVEL 0)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAX_LEVEL Mips)
      (glPixelStorei GL_UNPACK_ALIGNMENT Unpack)
      (glTexImage2D GL_TEXTURE_2D 0 Format (; Inf w) (; Inf h) 0 Type
         GL_UNSIGNED_BYTE (; Inf pixels) )
      (when (n0 Mips) (glGenerateMipmap GL_TEXTURE_2D))
      (sdlFreeSurface Sur) )
   (use> Shader)
   (setint> Shader Uniform Unit) )

(de setup-shaders ()
   (setq *Shader (new '(+Shader) *VShader *FShader))  # Create a new +Shader object
   (with *Shader
      (use> This)                                     # Activate the shader
      (uniform> This "vp")                            # Create the access to the uniform "vp"
      (newmat> This 4 'vpm)                           # Create a new matrix and name it 'vpm
      (uniform> This "heightmap")                     # Create the access to the uniform "heightmap"
      (uniform> This "terrain_texture") ) )           # Create the access to the uniform "terrain_texture"
      
(de create-window ()
   # Create an SDL window with an attached OpenGL context
   (setq *Win (new '(+OpenGLWindow) "SDL Test 13" 800 600 3 3
           '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
            (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 24)
            (SDL_GL_DOUBLEBUFFER . 1) ) ) ) )

(de initialize ()
   # Initialize SDL2 and the event handling functionality
   (sdlInitApp SDL_INIT_VIDEO)
   # Initialize the SDL2_image library
   (imgInit (| IMG_INIT_JPG IMG_INIT_PNG))
   # Create window
   (create-window)
   # Set shaders up
   (setup-shaders)
   # Set terrain up
   (setup-terrain 512 512)
   # Set textures up
   (setup-textures)
   # Set up the camera
   (setq *Camera (new '(+Camera) 0 0.2 2.0 (car (; *Win size)) (cadr (; *Win size))
            0.01 10.0)
         *Vel 0.01 )
   (rot> *Camera 180.0 0)
   (set-camera 0 0 0)
   (glClearColor 0.5 0.8 0.9 1.0)
   (glEnable GL_DEPTH_TEST)
   (glDepthFunc GL_LESS)
   (glEnable GL_CULL_FACE) )

(de shutdown ()
   (destroy> *Shader)
   (mapc glDeleteBuffers (list (; *Terrain vbo) (; *Terrain ebo)))
   (glDeleteVertexArrays (; *Terrain vao))
   (glDeleteTextures (list *Heightmap *TerrTex))
   (destroy> *Win)
   (sdlQuitApp) )

(de main ()
   (initialize)
   (sdlWaitLoop render-scene)
   (shutdown) )

# Actions responding to SDL events

(def '*SDLActions
   (quote
      (`SDL_KEYDOWN
         (sdlMapKeyboardEvent)
         (case (get *SDLEventInfo 8)
            (41 (on *SDLStop)) ) )        # escape key
      (`SDL_MOUSEMOTION
         (sdlMapMouseMotionEvent)
         (set-camera
            (get *SDLEventInfo 8)         # horiz. amount
            (get *SDLEventInfo 9)         # vert. amount
            (get *SDLEventInfo 5) ) ) ) ) # buttons pressed

(setq *VShader
   "#version 330 core
   
    layout (location = 0) in vec2 pos;
    
    out vec3 Normal;
    out vec3 FragPos;
    
    uniform mat4 vp;
    uniform sampler2D heightmap;
    
    void main () {
      vec4 height;
      vec2 uv;
      
      uv = (pos + 1.0f) / 2.0f;
      height = texture(heightmap, uv);
      gl_Position = vp * vec4(pos, height.a / 2.0f, 1.0f);
      Normal = vec3(height.rgb);
      FragPos = vec3(pos, height.a);
    }"
    
   *FShader
   "#version 330 core
   
      out vec4 FragColor;
      
      in vec3 FragPos;
      in vec3 Normal;
      
      uniform sampler2D terrain_texture;
      
      void main()
      {
         vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
         vec3 lightPos = vec3(1.0f, 2.0f, 2.0f);
         vec3 ambientLight = vec3(0.1f, 0.1f, 0.1f);
         
         vec3 lightDir = normalize(lightPos - FragPos);
         float diff = max(dot(Normal, lightDir), 0.0f);
         vec3 diffuse = diff * lightColor;

         vec2 uv = vec2(clamp(FragPos.z, 0.01f, 0.99f), 0.5f);
         vec3 texture_color = texture(terrain_texture, uv).rgb;
         
         vec3 result = (ambientLight + diffuse) * texture_color;
         FragColor = vec4(result, 1.0f);
      }" )

(de render-scene ()
   (glClear `(| GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT))
   # In the +Shader object *Shader we have the location of the unifom "vp".
   # Set it with the contents of the 16-float-buffer 'vpm, which is used to
   # store a 4x4 matrix. The contents of 'vpm are the list contained in the
   # CDR of the cons pair whose CAR is 'model, in the value of *Terrain
   (setmat> *Shader 4 "vp" 'vpm (matxmat *CamVP (; *Terrain model)))
   (glDrawElements `GL_TRIANGLE_STRIP (; *Terrain nume) `GL_UNSIGNED_INT 0)
   (swap> *Win) )

# The value of the global symbol *Terrain is a list of cons pairs storing
# the number of indices to render, the name of the 3 buffer objects
# and the model matrix

(de setup-terrain (NumX NumY)
   (let (Vao (glGenVertexArrays 1)
         (Vbo Ebo) (glGenBuffers 2)
         Verts (terrain-mesh NumX NumY)
         Inds (terrain-indices Verts NumX NumY) )
      (glBindVertexArray Vao)
      (glBindBuffer GL_ARRAY_BUFFER Vbo)
      (glBufferData GL_ARRAY_BUFFER (floats* 1.0 Verts) GL_STATIC_DRAW)
      (glVertexAttribPointer 0 2 GL_FLOAT 0 8 0)
      (glEnableVertexAttribArray 0)
      (glBindBuffer GL_ELEMENT_ARRAY_BUFFER Ebo)
      (glBufferData GL_ELEMENT_ARRAY_BUFFER (uints* Inds) GL_STATIC_DRAW)
      (setq *Terrain (mapcar cons '(nume vao vbo ebo model)
         (list (length Inds) Vao Vbo Ebo
            (matxmat (rotym (radians 90.0)) (rotxm (radians -90.0))) ) ) ) ) )

(de setup-textures ()
   (setq *Heightmap (glGenTextures 1)
         *TerrTex (glGenTextures 1) )
   (load-texture 0 *Heightmap "textures/heightmap.png" *Shader "hmap"
      NIL GL_RGBA )
   (load-texture 1 *TerrTex "textures/terrain.png" *Shader "terrain_texture"
      NIL GL_RGBA ) )


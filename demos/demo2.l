# SDL test 2
# Press Escape to quit

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l")

(de main ()
    (sdlInitApp SDL_INIT_VIDEO)
    (imgInit IMG_INIT_PNG)
    (let (W (new '(+RenderWindow) "SDL Test 2" 128 128)
          S (new '(+SprImg) "gfx/frog.png" "frog") )
        (put S 'sheet
            '((0 0 64 64) (64 0 64 64)
              (128 0 64 64) (192 0 64 64)
              (0 64 64 64) (64 64 64 64)
              (128 64 64 64) (192 64 64 64) ) )
        (put S 'curr 1)
        (totexture> S W)
        (rendercolor> W 150 80 122 0)
        (framerate> W 4)
        (setdest> S 32 32 64 64)
        (show W)
        (show S)
        (sdlEventLoop frame)
        (destroy> S)
        (destroy> W) )
    (imgQuit)
    (sdlQuitApp) )

(de frame ()
    (with S
        (apply send (cons 'setsrc> This (get (: sheet) (: curr))))
        (if (= 8 (: curr))
            (=: curr 1)
            (inc (:: curr)) ) )
    (clear> W)
    (copyto> S (get W 'renderer))
    (refresh> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )


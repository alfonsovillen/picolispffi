# Display a textured cube (12 triangles) on a SDL2 window, using
# OpenGl 3.3 shaders, and rotating it around all axis
# by applying a model-view-projection matrix with an
# OpenGl uniform.

# It uses the +Shader class and the transform.l file
# It also uses sdlimage.l for loading the texture

(scl 3)

(load "@sdl/timing.l" "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l"
   "@sdl/opengl3.l" "@sdl/sdlopengl.l" "@sdl/transform.l" "@sdl/shader.l" )

(de main ()

    (sdlInitApp SDL_INIT_VIDEO)
    (imgInit IMG_INIT_PNG)
    
    (let (W (new '(+OpenGLWindow) "SDL Test 9" 800 800 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 16)
              (SDL_GL_DOUBLEBUFFER . 1) ) )
              
          Shader (new '(+Shader)
          
            "#version 330 core
            layout (location = 0) in vec3 vertexPos;
            layout (location = 1) in vec2 vertexUV;
            out vec2 UV;
            uniform mat4 MVP;
            void main()
            {
                gl_Position = MVP * vec4(vertexPos, 1.0f);
                UV = vertexUV;
            }"
            
            "#version 330 core
            out vec4 color;
            in vec2 UV;
            uniform sampler2D myTex;
            void main()
            {
               color = texture(myTex, UV);
            }" )
          
          Vertices (floats* 10 # This 10 is the fixpoint scale of the elements
            (
               # front
               -5 -5  5 0 0
                5 -5  5 10 0
                5  5  5 10 10
               -5  5  5 0 10
               # top
               -5  5  5 0 0
                5  5  5 10 0
                5  5 -5 10 10
               -5  5 -5 0 10
               # back
                5 -5 -5 0 0
               -5 -5 -5 10 0
               -5  5 -5 10 10
                5  5 -5 0 10
               # bottom
               -5 -5 -5 0 0
                5 -5 -5 10 0
                5 -5  5 10 10
               -5 -5  5 0 10
               # left
               -5 -5 -5  0  0
               -5 -5  5 10  0
               -5  5  5 10 10
               -5  5 -5  0 10
               # right
                5 -5  5 0 0
                5 -5 -5 10 0
                5  5 -5 10 10
                5  5  5 0 10
               ) )
              
          Elements (ubytes*
            (0 1 2 2 3 0 4 5 6 6 7 4 8 9 10 10 11 8 12 13 14
             14 15 12 16 17 18 18 19 16 20 21 22 22 23 20 ) )
          
          Texture (chdir "textures"
            (imgLoad "checker.png") )
         
          TexInfo (surfstruct Texture)
            
          Angles (0 0 0) # Z Y X
          
          View (transm 0 0 -2.5)
          Persp (perspm 45.0 1.0 0.1 100.0)
          VP (matxmat Persp View)

          Clear (| GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT)

          VAO (glGenVertexArrays 1)
          VBO (glGenBuffers 1)
          EBO (glGenBuffers 1)
          TEX (glGenTextures 1) )
        
        (imgQuit)

        (glPixelStorei GL_UNPACK_ROW_LENGTH 256)
        (glPixelStorei GL_UNPACK_ALIGNMENT 1)
        (glBindTexture GL_TEXTURE_2D TEX)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_BASE_LEVEL 0)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAX_LEVEL 0)
        (prinl "Making texture " TEX " of size " (; TexInfo w) "x"
            (; TexInfo h) " with data at..." (; TexInfo pixels))
        (glTexImage2D GL_TEXTURE_2D 0 GL_RGBA8 (; TexInfo w) (; TexInfo h)
            0 GL_RGB GL_UNSIGNED_BYTE (; TexInfo pixels) )
        (and (=0 (glGetError)) (prinl "Made texture"))
        
        (sdlFreeSurface Texture)

        (glActiveTexture 0)
        (glBindVertexArray VAO)
        
        (glBindBuffer GL_ARRAY_BUFFER VBO)
        (glBufferData GL_ARRAY_BUFFER Vertices GL_STATIC_DRAW)
        (glVertexAttribPointer 0 3 GL_FLOAT 0 20 0) # float = 4 bytes
        (glEnableVertexAttribArray 0)
        (glVertexAttribPointer 1 2 GL_FLOAT 0 20 12)
        (glEnableVertexAttribArray 1)
        
        (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
        (glBufferData GL_ELEMENT_ARRAY_BUFFER Elements GL_STATIC_DRAW)
        
        (glEnable GL_DEPTH_TEST)
        (glClearColor 0 0 0 1.0)
        
        (newmat> Shader 4 'matrix)
        (uniform> Shader "MVP")
        (uniform> Shader "myTex")
        (use> Shader)

        (setint> Shader "myTex" 0)

        (sdlEventLoop frame)
        
        (glDeleteTextures TEX)
        
        (destroy> Shader)
        (destroy> W)
        
        (sdlQuitApp) ) )
    
(de frame ()
    (glClear Clear)
(secbench
    (setq Angles (mapcar '((X V) (inc 'X V)) Angles '(0.005 0.011 0.016)))
    (setq Model (multm (rotzm (car Angles)) (rotym (cadr Angles))
        (rotxm (caddr Angles)) ) )
    (setmat> Shader 4 "MVP" 'matrix (matxmat VP Model))
    (glDrawElements `GL_TRIANGLES 36 `GL_UNSIGNED_BYTE 0)
)
    (swap> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )


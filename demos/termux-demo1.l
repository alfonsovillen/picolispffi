# This demo works in termux, too.
# It doesn't use any form of hardware acceleration.
# To play it in termux, follow the instructions in 
# https://wiki.termux.com/wiki/Graphical_Environment
# and install the termux packages sdl2 and sdl2-image.
# findlib.l doesn't work in termux, but you can make *SDLlib and
# *SDLimg point to the exact location of the libraries libSDL2.so and
# libSDL2_image.so.
# After starting the VNC server and viewer, load the demo and start it
# with (main).

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l")

(de frame ()
   (with *Win
      (xy> *Spr
         (rand 0 (dec (car (: size))))
         (rand 0 (dec (cadr (: size)))) )
      (blitallto> *Spr (: surf))
      (refresh> This) ) )

(def '*SDLActions
   (quote
      (`SDL_QUIT (on *SDLStop))
      (`SDL_KEYDOWN
         (sdlMapKeyboardEvent)
         (and (= 41 (get *SDLEventInfo 8)) (on *SDLStop)) ) ) )

(de main ()
   (sdlInitApp SDL_INIT_VIDEO)
   (imgInit IMG_INIT_PNG)

   (setq *Win (new '(+Window) "Hello!" 1000 600)
         *Spr (new '(+SprImg) "gfx/linux.png" 'linux) )

   (with *Win
      (framerate> This 60)
      (=: bgd (sdlMapRGBAlloc (: pxf) 255 255 255))
      (sdlFillSurfaceRGB (: surf) 255 255 255) )

   (with *Spr
      (alpha> This 255)
      (setsrc> This 0 0 128 128)
      (setdest> This 0 0 128 128) )

   (sdlEventLoop frame)

   (destroy> *Spr)
   (destroy> *Win)
   (imgQuit)
   (sdlQuitApp) )


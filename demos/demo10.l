# Display a textured monkey head on a SDL2 window, using
# OpenGl 3.3 shaders, and rotating it around all axis
# by applying a model-view-projection matrix with an
# OpenGl uniform.

# It uses:
# - the event handling framework (*SDLActions, *SDLStop, *SDLEvent,
#   sdlWaitLoop, sdlMapKeyboardEvent, sdlMapMouseMotionEvent,
#   sdlInitApp, sdlQuitApp) in sdl.l
# - the +OpenGLWindow class in sdlopengl.l
# - the +Shader class in shader.l
# - the transform.l file for operations on vertex geometry
# - sdlimage.l for loading the texture
# - a BIN file created by "obj2bin" in the loadobj.l file (the original
#   file is an .OBJ file exported from Blender)
# Hover with the mouse to rotate the monkey head
# Press escape to quit

(scl 3)

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/opengl3.l"
   "@sdl/sdlopengl.l" "@sdl/transform.l" "@sdl/shader.l" )

(de main ()

    # Initialize SDL2 and the event handling functionality
    (sdlInitApp SDL_INIT_VIDEO)
    
    # Initialize the SDL2_image library
    (imgInit IMG_INIT_JPG)
    
    # Create an SDL window with an attached OpenGL context
    (let (W (new '(+OpenGLWindow) "SDL Test 10" 800 800 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 8)
              (SDL_GL_DOUBLEBUFFER . 1) ) )
        
          # Create and compile an OpenGL shader program
          Shader (new '(+Shader)
          
            # the vertex shader code
            
            "#version 330 core
            layout (location = 0) in vec3 vertexPos;
            layout (location = 1) in vec2 vertexUV;
            layout (location = 2) in vec3 vertexNormal;
            out vec3 FragPos;
            out vec3 Normal;
            out vec2 UV;
            uniform mat4 VP;
            uniform mat4 model;
            uniform mat4 normat;
            void main()
            {
                FragPos = vec3(model * vec4(vertexPos, 1.0f));
                gl_Position = VP * vec4(FragPos, 1.0f);
                UV = vertexUV;
                Normal = vec3(normat * vec4(vertexNormal, 1.0f));
            }"
            
            # the fragment shader code
            
            "#version 330 core
            out vec4 FragColor;
            in vec2 UV;
            in vec3 Normal;
            in vec3 FragPos;
            uniform sampler2D myTex;
            uniform vec3 ambientLight;
            uniform vec3 lightPos;
            void main()
            {
               vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
               vec3 lightDir = normalize(lightPos - FragPos);
               float diff = max(dot(Normal, lightDir), 0.0f);
               vec3 diffuse = diff * lightColor;
               vec3 textureColor = texture(myTex, UV).rgb;
               vec3 result = (ambientLight + diffuse) * textureColor;
               FragColor = vec4(result, 1.0f);
            }" )
          
          # Load a mesh file
          Mesh (chdir "meshes" (in "susi.bin" (read)))
          # Get the number of vertices of the mesh
          NumElems (; Mesh numelems)
          
          # Load a texture
          Texture (chdir "textures"
            (imgLoad "susi.jpg") ) # load the image into an SDL_Surface
         
          TexInfo (surfstruct Texture) # get the SDL_Surface structure
            
          Angles (0 0 0) # Z Y X
          
          # Create the matrices for projecting the vertices
          Model (identm)
          Norm (identm)
          View (transm 0 0 -5.0)
          Persp (perspm 45.0 1.0 0.1 100.0)
          VP (matxmat Persp View)

          Clear (| GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT)

          # Create the needed buffers
          VAO (glGenVertexArrays 1)
          VBO (glGenBuffers 1)
          EBO (glGenBuffers 1)
          TEX (glGenTextures 1)
          
          MX 1 MY 1 DX 0 DY 0 ) # for mouse motion events
        
        (imgQuit)

        # Upload the texture to the GPU using usual OpenGL
        (glActiveTexture 0)
        (glBindTexture GL_TEXTURE_2D TEX)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_BASE_LEVEL 0)
        (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAX_LEVEL 0)
        (glTexStorage2D GL_TEXTURE_2D 1 GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
           (; TexInfo w) (; TexInfo h) )
        (glTexSubImage2D GL_TEXTURE_2D 0 0 0 (; TexInfo w) (; TexInfo h)
           GL_RGB GL_UNSIGNED_BYTE (; TexInfo pixels) )
        
        (sdlFreeSurface Texture)

        (glBindVertexArray VAO)
        
        # Set up the vertex array buffer attributes using usual OpenGL
        (glBindBuffer GL_ARRAY_BUFFER VBO)
        (glBufferData GL_ARRAY_BUFFER (; Mesh vertices) GL_STATIC_DRAW)
        (glVertexAttribPointer 0 3 GL_FLOAT 0 32 0) # Vertex coordinates: float = 4 bytes
        (glEnableVertexAttribArray 0)
        (glVertexAttribPointer 1 2 GL_FLOAT 0 32 12) # Texture UV coordinates
        (glEnableVertexAttribArray 1)
        (glVertexAttribPointer 2 3 GL_FLOAT 0 32 20) # Normal coordinates
        (glEnableVertexAttribArray 2)
        
        (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
        (glBufferData GL_ELEMENT_ARRAY_BUFFER (; Mesh elems) GL_STATIC_DRAW)
        
        (glEnable GL_DEPTH_TEST)
        (glClearColor 0 0 0 1.0)
        
        (glEnable GL_CULL_FACE)
        
        (newmat> Shader 4 'vp)      # allocate matrices
        (newmat> Shader 4 'model)
        (newmat> Shader 4 'normat)
        (uniform> Shader "VP")      # get uniform locations
        (uniform> Shader "myTex")
        (uniform> Shader "model")
        (uniform> Shader "normat")
        (uniform> Shader "ambientLight")
        (uniform> Shader "lightPos")
        
        (use> Shader)

        (setmat> Shader 4 "VP" 'vp VP)  # set uniforms with a matrix
        (setmat> Shader 4 "model" 'model (identm))
        (setmat> Shader 4 "normat" 'normat (identm))
        (setvec> Shader "ambientLight" 0.2 0.2 0.2) # set uniforms with vec3
        (setvec> Shader "lightPos" 1.0 1.0 2.0)
        (setint> Shader "myTex" 0)  # set uniform with an integer

        (sdlWaitLoop frame) # the rendering and event handling loop
        
        (glDeleteTextures TEX)
        
        (destroy> Shader) # deallocate the matrices 'vp and 'model
        (destroy> W) # close the window
        
        # Finalize SDL and the event handling
        (sdlQuitApp) ) )

# Frame rendering (called every time from sdlWaitLoop)
(de frame ()
    # Only if the current mouse position is different from the previous one
    (unless (and (= MX DX) (= MY DY))
       (glClear Clear)
       # Set the uniform "model" in the shader program
       (setmat> Shader 4 "model" 'model Model)
       (setmat> Shader 4 "normat" 'normat Norm)
       # Draw the mesh
       (glDrawElements `GL_TRIANGLES NumElems `GL_UNSIGNED_INT 0)
       # Update the window
       (swap> W)
       # Store the new mouse position
       (setq MX DX
             MY DY
             # Calculate the geometry rotation
             Angles (mapcar '((X V) (inc 'X V)) Angles 
                (list 0 (* 0.02 MX) (* 0.02 MY)) ) )
       # Calculate the model-view-projection matrix
       (setq Model (multm (rotzm (car Angles)) (rotym (cadr Angles))
           (rotxm (caddr Angles)) )
       # and the normal matrix (inverse transpose of the model matrix)
           Norm (xy2yx (invm Model)) ) ) )

# Respond to events
(def '*SDLActions
    (quote
        # If a key was pressed
        (`SDL_KEYDOWN
            # Get the event information
            (sdlMapKeyboardEvent)
            # If the key code is Escape (#41), then stop the event loop
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) )
        # If the mouse cursor has moved
        (`SDL_MOUSEMOTION
            # Get the event information
            (sdlMapMouseMotionEvent)
            # Store the mouse position in DX and DY
            (setq DX (get *SDLEventInfo 8)
                  DY (get *SDLEventInfo 9) ) ) ) )


# demo12.l: show a rotating planet with 10000 asteroids around it
# on a spatial background
# Code and textures partially translated/borrowed from learnopengl.com

(scl 5)

(once (load "@sdl/transform.l" "@sdl/loadobj.l"
   "@sdl/sdl.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l"
   "@sdl/sdlimage.l" "@sdl/shader.l" ) )

# Shader program code

(setq *AsteroidVertexShader
   "#version 330 core
    layout (location = 0) in vec3 pos;
    layout (location = 1) in vec2 uv;
    layout (location = 2) in mat4 model;
    out vec2 texcoords;
    uniform mat4 vp;
    void main()
    {
      gl_Position = vp * model * vec4(pos, 1.0f);
      texcoords = uv;
    }"
    
   *AsteroidFragmentShader
   "#version 330 core
    in vec2 texcoords;
    out vec4 fragmentColor;
    uniform sampler2D image;
    void main()
    {
      vec3 tex = texture(image, texcoords).rgb;
      fragmentColor = vec4(tex, 1.0f);
    }"
    
   *PlanetVertexShader
   "#version 330 core
    layout (location = 0) in vec3 pos;
    layout (location = 1) in vec2 uv;
    out vec2 texcoords;
    uniform mat4 vp;
    void main()
    {
      gl_Position = vp * vec4(pos, 1.0f);
      texcoords = uv;
    }"
    
   *PlanetFragmentShader
   "#version 330 core
    in vec2 texcoords;
    out vec4 fragmentColor;
    uniform sampler2D image;
    const vec4 planetColor = vec4(0.7f, 1.0f, 0.8f, 1.0f);
    void main()
    {
      vec3 tex = texture(image, texcoords).rgb;
      fragmentColor = planetColor * vec4(tex, 1.0f);
    }"

   *SkyboxVertexShader
   "#version 330 core
    layout (location = 0) in vec3 pos;
    out vec3 texcoords;
    uniform mat4 vp;
    void main()
    {
      texcoords = pos;
      vec4 far = vp * vec4(pos, 1.0f);
      gl_Position = far.xyww;
    }"

   *SkyboxFragmentShader
   "#version 330 core
    in vec3 texcoords;
    out vec4 fragmentColor;
    uniform samplerCube image;
    void main()
    {
      vec3 tex = texture(image, texcoords).rgb;      
      fragmentColor = vec4(tex, 1.0f);
    }" )

# Update the +Camera parameters according to mouse position and buttons

(de set-camera (DX DY Button)
   (case Button
      (1                                              # left mouse button is pressed: move the camera
         (if (> (abs DX) (abs DY))
            (move> *Camera 'left (* *Vel DX))         # depending on the sign of DX or DY,
            (move> *Camera 'down (* *Vel DY)) ) )     # the movement will be in the opposite direction
      (4                                              # right mouse button: rotate the camera
         (rot> *Camera (* *Vel DX) (* *Vel DY)) ) )
   (let (View (view> *Camera) Proj (proj> *Camera))
            # update the projection-view matrix of the camera:
            # multiply the projection matrix by the view matrix
      (setq *CamVP (matxmat Proj View)
            # update the projection-view matrix of the skybox:
            # remove the translation off the view matrix and multiply
            # the projection matrix by that
            *SkyVP (matxmat Proj (remtransm View)) ) ) )

# Create model matrices for the asteroids and return them as a list

(de displacement (X) (- (rand 0 (* X 2)) X))

(de asteroid-matrices (Amount Radius Offset)
   (use (Angle Scale)
      (seed (time))
      (make
         (for I Amount
            (setq Angle (*/ (*/ 1.0 I Amount) 360.0 1.0)
                  Scale (rand 0.05 0.25) )
            (chain
               # multm multiplies matrices in sequence, not as OpenGL multiplies them
               (multm
                  (scalem Scale Scale Scale)
                  (rotxm (radians (rand 0 360.0)))
                  (rotym (radians (rand 0 360.0)))
                  (rotzm (radians (rand 0 360.0)))
                  (transm
                     (+ (displacement Offset) (*/ (sin Angle) Radius 1.0))
                     (*/ (displacement Offset) 0.4 1.0)
                     (+ (displacement Offset) (*/ (cos Angle) Radius 1.0)) ) ) ) ) ) ) )

# Load a 2D texture, generate mip maps for it and bind it to a texture unit in
# a shader program

(de load-texture (Unit ID File Shader Uniform)
   (let (Sur (imgLoad File)                                    # Use SDL2_image to load the image
         Inf (surfstruct Sur) )                                # Get the SDL_SURFACE structure
      (glActiveTexture Unit)
      (glBindTexture GL_TEXTURE_2D ID)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST_MIPMAP_NEAREST)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_BASE_LEVEL 0)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAX_LEVEL 11)
      (glTexStorage2D GL_TEXTURE_2D 1 GL_RGBA8
         (; Inf w) (; Inf h) )
      (glTexSubImage2D GL_TEXTURE_2D 0 0 0 (; Inf w) (; Inf h)
         GL_RGB GL_UNSIGNED_BYTE (; Inf pixels) )
      (glGenerateMipmap GL_TEXTURE_2D)
      (sdlFreeSurface Sur) )
   (use> Shader)
   (setint> Shader Uniform Unit) )

(de create-asteroids ()
   (let A (loadbin "meshes/rock.bin")
      (setq *AShader (new '(+Shader) *AsteroidVertexShader *AsteroidFragmentShader)
	         *AVAO (glGenVertexArrays 1)
            *AVBO (glGenBuffers 1)
            *AEBO (glGenBuffers 1)
            *AMAT (glGenBuffers 1)
            *ATEX (glGenTextures 1)
            *AElems (; A numelems) )
      (uniform> *AShader "image")
      (uniform> *AShader "vp")
      (newmat> *AShader 4 'vpmat)
      (glBindVertexArray *AVAO)
      (glBindBuffer GL_ARRAY_BUFFER *AVBO)
      (glBufferData GL_ARRAY_BUFFER (; A vertices) GL_STATIC_DRAW)
      (glVertexAttribPointer 0 3 GL_FLOAT 0 32 0) # Vertex coordinates: float = 4 bytes
      (glEnableVertexAttribArray 0)
      (glVertexAttribPointer 1 2 GL_FLOAT 0 32 12) # Texture UV coordinates
      (glEnableVertexAttribArray 1)
      (glBindBuffer GL_ELEMENT_ARRAY_BUFFER *AEBO)
      (glBufferData GL_ELEMENT_ARRAY_BUFFER (; A elems) GL_STATIC_DRAW)
      (glBindBuffer GL_ARRAY_BUFFER *AMAT)
      (glBufferData GL_ARRAY_BUFFER
         (floats* 1.0 (asteroid-matrices *ANum 150.0 25.0))
            GL_STATIC_DRAW )
      (glVertexAttribPointer 2 4 GL_FLOAT 0 64 0)
      (glEnableVertexAttribArray 2)
      (glVertexAttribPointer 3 4 GL_FLOAT 0 64 16)
      (glEnableVertexAttribArray 3)
      (glVertexAttribPointer 4 4 GL_FLOAT 0 64 32)
      (glEnableVertexAttribArray 4)
      (glVertexAttribPointer 5 4 GL_FLOAT 0 64 48)
      (glEnableVertexAttribArray 5)
      (glVertexAttribDivisor 2 1)
      (glVertexAttribDivisor 3 1)
      (glVertexAttribDivisor 4 1)
      (glVertexAttribDivisor 5 1) )
   (load-texture 0 *ATEX "textures/Rock-Texture-Surface.jpg" *AShader "image") )

(de create-planet ()
   (let A (loadbin "meshes/planet.bin")
      (setq *PShader (new '(+Shader) *PlanetVertexShader *PlanetFragmentShader)
	         *PVAO (glGenVertexArrays 1)
            *PVBO (glGenBuffers 1)
            *PEBO (glGenBuffers 1)
            *PTEX (glGenTextures 1)
            *PElems (; A numelems)
            *PScale (matxmat (transm 0 -20.0 0) (scalem 20.0 20.0 20.0))
            *PlanetRot 0
            *Planet (rotym 0) )
      (uniform> *PShader "image")
      (uniform> *PShader "vp")
      (newmat> *PShader 4 'vpmat)
      (glBindVertexArray *PVAO)
      (glBindBuffer GL_ARRAY_BUFFER *PVBO)
      (glBufferData GL_ARRAY_BUFFER (; A vertices) GL_STATIC_DRAW)
      (glVertexAttribPointer 0 3 GL_FLOAT 0 32 0) # Vertex coordinates: float = 4 bytes
      (glEnableVertexAttribArray 0)
      (glVertexAttribPointer 1 2 GL_FLOAT 0 32 12) # Texture UV coordinates
      (glEnableVertexAttribArray 1)
      (glBindBuffer GL_ELEMENT_ARRAY_BUFFER *PEBO)
      (glBufferData GL_ELEMENT_ARRAY_BUFFER (; A elems) GL_STATIC_DRAW) )
   (load-texture 1 *PTEX "textures/planet_Quom1200.jpg" *PShader "image") )

# Load a cubemap texture (6 files). The width and height of the images must be specified.
# Assign the texture to a texture unit and a shader program

(de load-cubemap (Unit ID Files Width Height Shader Uniform)
   (glActiveTexture Unit)
   (glBindTexture GL_TEXTURE_CUBE_MAP ID)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_MIN_FILTER GL_LINEAR)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_MAG_FILTER GL_LINEAR)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_WRAP_R GL_CLAMP_TO_EDGE)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_BASE_LEVEL 0)
   (glTexParameteri GL_TEXTURE_CUBE_MAP GL_TEXTURE_MAX_LEVEL 0)
   (glTexStorage2D GL_TEXTURE_CUBE_MAP 1 GL_RGBA8
      Width Height )
   (use (Sur Inf Face)
      (setq Face GL_TEXTURE_CUBE_MAP_POSITIVE_X)
      (for File Files
         (setq Sur (imgLoad File)
               Inf (surfstruct Sur) )
         (glTexSubImage2D Face 0 0 0 Width Height
            GL_RGB GL_UNSIGNED_BYTE (; Inf pixels) )
         (sdlFreeSurface Sur)
         (inc 'Face) ) )
   (use> Shader)
   (setint> Shader Uniform Unit) )

# Create the skybox

(de create-skybox ()
   (setq *SShader (new '(+Shader) *SkyboxVertexShader *SkyboxFragmentShader)
         *SVAO (glGenVertexArrays 1)
         *SVBO (glGenBuffers 1)
         *STEX (glGenTextures 1) )
   (uniform> *SShader "image")
   (uniform> *SShader "vp")
   (newmat> *SShader 4 'vpmat)
   (glBindVertexArray *SVAO)
   (glBindBuffer GL_ARRAY_BUFFER *SVBO)
   (glBufferData GL_ARRAY_BUFFER
      (floats* 1.0
         (-1.0  1.0 -1.0 -1.0 -1.0 -1.0  1.0 -1.0 -1.0
           1.0 -1.0 -1.0  1.0  1.0 -1.0 -1.0  1.0 -1.0
          -1.0 -1.0  1.0 -1.0 -1.0 -1.0 -1.0  1.0 -1.0
          -1.0  1.0 -1.0 -1.0  1.0  1.0 -1.0 -1.0  1.0
           1.0 -1.0 -1.0  1.0 -1.0  1.0  1.0  1.0  1.0
           1.0  1.0  1.0  1.0  1.0 -1.0  1.0 -1.0 -1.0
          -1.0 -1.0  1.0 -1.0  1.0  1.0  1.0  1.0  1.0
           1.0  1.0  1.0  1.0 -1.0  1.0 -1.0 -1.0  1.0
          -1.0  1.0 -1.0  1.0  1.0 -1.0  1.0  1.0  1.0
           1.0  1.0  1.0 -1.0  1.0  1.0 -1.0  1.0 -1.0
          -1.0 -1.0 -1.0 -1.0 -1.0  1.0  1.0 -1.0 -1.0
           1.0 -1.0 -1.0 -1.0 -1.0  1.0  1.0 -1.0  1.0 ) )
      GL_STATIC_DRAW )
   (glVertexAttribPointer 0 3 GL_FLOAT 0 12 0) # Vertex coordinates: float = 4 bytes
   (glEnableVertexAttribArray 0)
   (load-cubemap 2 *STEX
      (list "textures/skybox/right.jpg" "textures/skybox/left.jpg"
       "textures/skybox/top.jpg" "textures/skybox/bottom.jpg"
       "textures/skybox/front.jpg" "textures/skybox/back.jpg" )
      1024 1024 *SShader "image" ) )

(de render-asteroids ()
   (use> *AShader)
   # set the uniform "vp" with the matrix stored as 'vpmat, whose contents
   # will be the multiplication of the projection-view matrix of the camera
   # by the model matrix of the planet
   (setmat> *AShader 4 "vp" 'vpmat (matxmat *CamVP *Planet))
   (glBindVertexArray *AVAO)
   (glDrawElementsInstanced `GL_TRIANGLES *AElems `GL_UNSIGNED_INT 0 *ANum) )

(de render-planet ()
   (use> *PShader)
   (setmat> *PShader 4 "vp" 'vpmat (multm *PScale *Planet *CamVP))
   (glBindVertexArray *PVAO)
   (glDrawElements `GL_TRIANGLES *PElems `GL_UNSIGNED_INT 0) )

(de render-skybox ()
   (glDepthFunc GL_LEQUAL)
   (use> *SShader)
   (setmat> *SShader 4 "vp" 'vpmat *SkyVP)
   (glBindVertexArray *SVAO)
   (glDrawArrays `GL_TRIANGLES 0 36)
   (glDepthFunc GL_LESS) )

(de render-scene ()
   (inc '*PlanetRot 0.002)
   (setq *Planet (rotym *PlanetRot))
   (glClear `(| GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT))
   (render-planet)
   (render-asteroids)
   (render-skybox)
   (swap> *Win) )

(de create-window ()
   # Create an SDL window with an attached OpenGL context
   (setq *Win (new '(+OpenGLWindow) "SDL Test 12" 800 600 3 3
           '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
            (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 24)
            (SDL_GL_DOUBLEBUFFER . 1) ) ) )
   (glClearColor 0.1 0.1 0.1 1.0)
   (glEnable GL_DEPTH_TEST)
   (glDepthFunc GL_LESS)
   (glEnable GL_CULL_FACE) )

(de initialize ()
   # Initialize SDL2 and the event handling functionality
   (sdlInitApp SDL_INIT_VIDEO)
   # Initialize the SDL2_image library
   (imgInit IMG_INIT_JPG)
   # Create window
   (create-window)
   (setq *ANum 10000
         *Camera (new '(+Camera) 0 0 -300.0 (car (; *Win size)) (cadr (; *Win size))
            0.1 1000.0)
         *Vel 0.2 )
   # Set up asteroids
   (create-asteroids)
   # Set up planet
   (create-planet)
   # Set up sky box
   (create-skybox)
   # Set up the camera
   (set-camera 0 0 0) )

(de shutdown ()
   (mapc '((S) (and S (destroy> S))) (list *AShader *PShader *SShader))
   (mapc '((B) (and B (glDeleteBuffers B)))
      (list *AVBO *AEBO *AMAT *PVBO *PEBO *SVBO) )
   (mapc '((A) (and A (glDeleteVertexArrays A)))
      (list *AVAO *PVAO *SVAO) )
   (mapc '((X) (and X (glDeleteTextures X)))
      (list *ATEX *PTEX *STEX) )
   (destroy> *Win)
   (sdlQuitApp) )

(de main ()
   (show-info)
   (initialize)
   (sdlEventLoop render-scene)
   (shutdown) )

(def '*SDLActions
   (quote
      (`SDL_KEYDOWN
         (sdlMapKeyboardEvent)
         (case (get *SDLEventInfo 8)
            (41 (on *SDLStop))            # escape key
            (44 (show-params)) ) )        # space bar
      (`SDL_MOUSEMOTION
         (sdlMapMouseMotionEvent)
         (set-camera
            (get *SDLEventInfo 8)         # horiz. amount
            (get *SDLEventInfo 9)         # vert. amount
            (get *SDLEventInfo 5) ) ) ) ) # buttons pressed

(de show-info ()
   (prinl
      "Turn the camera with the right mouse button.^J"
      "Move around with the left mouse button.^J"
      "Press the space bar to show the parameters." ) )

(de show-params ()
   (prin "Camera view-projection matrix ")
   (println *CamVP)
   (prin "Skybox view-projection matrix ")
   (println *SkyVP)
   (prinl "Camera parameters")
   (show *Camera) )

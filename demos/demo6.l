# Display a square (two triangles) on a SDL2 window, using
# OpenGl 3.3 shaders, and rotating it around the Z axis
# by applying a matrix transformation with an
# OpenGl uniform. This version does the same as demo5.l,
# but it uses the +Shader class in "shader.l"

(scl 3)

(load "@sdl/sdl.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l" "@sdl/transform.l"
   "@sdl/shader.l" )

(de main ()
    (sdlInitApp SDL_INIT_VIDEO)
    (let (W (new '(+OpenGLWindow) "SDL Test 6" 400 400 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 16)
              (SDL_GL_DOUBLEBUFFER . 1) ) )
          Shader (new '(+Shader)
            "#version 330 core
            layout (location = 0) in vec3 aPos;
            layout (location = 1) in vec3 aColor;
            out vec3 ourColor;
            uniform mat4 transform;
            void main()
            {
                gl_Position = transform * vec4(aPos, 1.0f);
                ourColor = aColor;
            }"
            "#version 330 core
            out vec4 FragColor;
            in vec3 ourColor;
            void main()
            {
                FragColor = vec4(ourColor, 1.0f);
            }" )
          Vertices (floats* 10
            (-5 -5 0 10  0  0
              5 -5 0  0 10  0
              5  5 0  0  0 10
             -5  5 0 10 10 10 ) )
          Elements (ubytes* (0 1 3 1 2 3))
          Angle 0
          VAO (glGenVertexArrays 1)
          VBO (glGenBuffers 1)
          EBO (glGenBuffers 1) )
        (glBindVertexArray VAO)
        (glBindBuffer GL_ARRAY_BUFFER VBO)
        (glBufferData GL_ARRAY_BUFFER Vertices GL_STATIC_DRAW)
        (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
        (glBufferData GL_ELEMENT_ARRAY_BUFFER Elements GL_STATIC_DRAW)
        (glVertexAttribPointer 0 3 GL_FLOAT 0 24 0) # float = 4 bytes
        (glEnableVertexAttribArray 0)
        (glVertexAttribPointer 1 3 GL_FLOAT 0 24 12)
        (glEnableVertexAttribArray 1)
        (newmat> Shader 4 'matrix)
        (uniform> Shader "transform")
        (show Shader)
        (sdlEventLoop frame)
        (destroy> Shader)
        (destroy> W)
        (sdlQuitApp) ) )
    
(de frame ()
    (glClearColor 0.2 0.3 0.3 1.0)
    (glClear `GL_COLOR_BUFFER_BIT)
    (use> Shader)
    (if (>= Angle 360.0) (zero Angle) (inc 'Angle 1.0))
    (setmat> Shader 4 "transform" 'matrix (rotxm (radians Angle)))
    (glBindVertexArray VAO)
    (glDrawElements `GL_TRIANGLES 6 `GL_UNSIGNED_BYTE 0)
    (swap> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )


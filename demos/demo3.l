# SDL Test 3
# Press Escape to quit

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlimage.l" "@sdl/coords.l")

(de main ()
    # initialize SDL video and event handling
    (sdlInitApp SDL_INIT_VIDEO)
    # initialize SDL_image
    (imgInit IMG_INIT_PNG)
            # create a +RenderWindow
    (let (W (new '(+RenderWindow) "SDL Test 3" 256 256)
            # load a spritesheet
          S (new '(+SprImg) "gfx/frog.png" "frog")
            # create a coordinates object
          R (new '(+Rect) 0 0 256 256) )
        # initial zoom: zoom out
        (put R 'dir -1)
        # convert the sprite to a texture for faster rendering
        (totexture> S W)
        # define sprite animation steps (pixel offsets in the
        # spritesheet bitmap)
        (put S 'sheet
                '((0 0 64 64) (64 0 64 64)
                  (128 0 64 64) (192 0 64 64)
                  (0 64 64 64) (64 64 64 64)
                  (128 64 64 64) (192 64 64 64) ) )
        # set the animation step
        (put S 'curr 1)
        # the sprite will animate at every 8th frame, set the
        # frame counter to 0
        (put S 'fcount 0)
        # window background color
        (rendercolor> W 150 80 122 0)
        # set the framerate to 30 fps
        (framerate> W 30)
        # animate and handle events
        (sdlEventLoop frame)
        # clean up
        (destroy> S)
        (destroy> W) )
    (imgQuit)
    (sdlQuitApp) )

(de frame ()
    # resize the sprite
    (with R
        (zoom> This (: dir))
        (setdest> S (: left) (: top) (: width) (: height))
        (when (or (<= (: width) 2) (>= (: width) 256))
            (=: dir (- (: dir))) ) )
    (clear> W)
    # animate the sprite
    (with S
        (inc (:: fcount))
        (when (=0 (% (: fcount) 8)) (inc (:: curr)))
        (when (< 8 (: curr)) (=: curr 1))
        (apply send (cons 'setsrc> This (get (: sheet) (: curr))))
        # render the sprite
        (sdlRenderCopy (get W 'renderer) (: surf) (: src) (: dest) ) )
    # update window
    (refresh> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )


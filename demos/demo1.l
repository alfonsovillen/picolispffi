# SDL test 1
# Close the window or press Escape to quit

# *SDLActions:
# Actions to be executed when an event is retrieved and stored in
# *SDLEvent (a malloc'ed structure initialized in sdlInitApp
# and freed in sdlQuitApp).
# It is defined globally so that it must not be passed everytime
# a new event is retrieved from the SDL event queue.

(load "@sdl/sdl.l" "@sdl/sdlutil.l")

(def '*SDLActions
    (quote
        # In the list you put pairs of numbers and a list of executable
        # expressions. The numbers match the SDL event types.
        (`SDL_QUIT
            # if *SDLStop is not NIL, the function sdlEventLoop will terminate
            (on *SDLStop) )
        (`SDL_MOUSEBUTTONDOWN
            # *SDLEvent contains a SDL_MouseButtonEvent structure. Get
            # the structure and store it as a list in *SDLEventInfo
            (sdlMapMouseButtonEvent)
            # *SDLEventInfo contains now the mouse button event info
            (println 'SDL_MOUSEBUTTONDOWN! *SDLEventInfo) )
        (`SDL_KEYDOWN
            # *SDLEvent contains a SDL_MouseButtonEvent structure. Get
            # the structure and store it as a list in *SDLEventInfo
            (sdlMapKeyboardEvent)
            # The 8th field of *SDLEventInfo holds now the key code
            # 41 = Escape key
            (when (= (get *SDLEventInfo 8) 41)
                (on *SDLStop) ) ) ) )
            
(de main ()
    (seed (stamp))
    (sdlInitApp SDL_INIT_VIDEO)
          # Create a window with a renderer
    (setq Win (new '(+RenderWindow) "SDL Test 1" 800 600)
          Ren (get Win 'renderer)
          # Create 10 sprites (the image is a sprite sheet with the
          # same image in different colours)
          Spr (make (do 20 (link (new '(+Sprite) "gfx/image.bmp")))) )
          # Customize each sprite
    (mapc '((This)
        # Always apply colorkey> before totexture>!
        (colorkey> This T 0 0 0)
        (totexture> This Win)
        (=: dx (rand 1 5))
        (=: dy (rand 1 5))
        (setsrc> This (+ 4 (* 64 (rand 1 3))) 0 64 64)
        (setdest> This (rand 1 500) (rand 1 500) 64 64)
        (blend> This SDL_BLENDMODE_BLEND)
        (alpha> This (rand 50 255)) ) Spr )
        # window background colour: black
        (rendercolor> Win 0 0 0 0)
        # set the animation framerate to 60 fps
        (framerate> Win 60)
        # sdlEventLoop will run the "frame" function at every frame, according
        # to the value set above, until *SDLStop is not NIL. It will also
        # check for SDL events and run the matching action in *SDLActions
        (sdlEventLoop frame)
        # clean up
        (mapc '((This) (destroy> This)) Spr)
        (destroy> Win)
        (sdlQuitApp) )

(de frame ()
    (clear> Win)
    (mapc '((This)
        (let Pos (pos> This)
            (when (or (>= (car Pos) 736) (<= (car Pos) 0)) (=: dx (- (: dx))))
            (when (or (>= (cadr Pos) 536) (<= (cadr Pos) 0)) (=: dy (- (: dy)))) )
        (move> This (: dx) (: dy))
        (copyto> This Ren) ) Spr )
    (refresh> Win) )


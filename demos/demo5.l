# Display a square (two triangles) on a SDL2 window, using
# OpenGl 3.3 shaders, and rotating it around the Z axis
# by applying a matrix transformation with an
# OpenGl uniform

(scl 3)

(load "@sdl/sdl.l" "@sdl/sdlutil.l" "@sdl/sdlopengl.l" "@sdl/opengl3.l"
   "@sdl/transform.l" )

(de main ()
    
    # Initialize SDL and the event handling loop
    
    (sdlInitApp SDL_INIT_VIDEO)
    
    # Further setup
    
    (setq
    
        # allocate a transformation matrix, M holds the pointer
    
        M (%@ "malloc" 'P 64)
    
        # rotation angle
    
        Angle 0
    
        # a +OpenGLWindo object
    
        W (new '(+OpenGLWindow) "SDL Test 5" 400 400 3 3
            '((SDL_GL_RED_SIZE . 8) (SDL_GL_GREEN_SIZE . 8)
              (SDL_GL_BLUE_SIZE . 8) (SDL_GL_DEPTH_SIZE . 16)
              (SDL_GL_DOUBLEBUFFER . 1) ) )
              
        # vertex data (floats* prepares them for use with 'native')
        
        Vertices (floats* 10
            (-5 -5 0 10  0  0
              5 -5 0  0 10  0
              5  5 0  0  0 10
             -5  5 0 10 10 10 ) )
             
        # element data (ubytes* prepares them for use with 'native')
        
        Elements (ubytes* (0 1 3 1 2 3))
        
        # shader programs (plain strings)
        
        VShaderProg
            "#version 330 core
            layout (location = 0) in vec3 aPos;
            layout (location = 1) in vec3 aColor;
            out vec3 ourColor;
            uniform mat4 transform;
            void main()
            {
                gl_Position = transform * vec4(aPos, 1.0f);
                ourColor = aColor;
            }"
            
        FShaderProg
            "#version 330 core
            out vec4 FragColor;
            in vec3 ourColor;
            void main()
            {
                FragColor = vec4(ourColor, 1.0f);
            }"
            
        # new OpenGL program, vertex shader, fragment shader,
        # vertex array and 2 buffers
        
        Program (glCreateProgram)
        VShader (glCreateShader GL_VERTEX_SHADER)
        FShader (glCreateShader GL_FRAGMENT_SHADER)
        VAO (glGenVertexArrays 1)
        VBO (glGenBuffers 1)
        EBO (glGenBuffers 1) )

    # compile the vertex shader
    
    (glShaderSource VShader VShaderProg)
    (glCompileShader VShader)
    (unless (compiled? VShader) (quit "Vertex shader not compiled"))
    
    # compile the fragment shader
    
    (glShaderSource FShader FShaderProg)
    (glCompileShader FShader)
    (unless (compiled? FShader) (quit "Fragment shader not compiled"))
    
    # build program
    
    (glAttachShader Program VShader)
    (glAttachShader Program FShader)
    (glLinkProgram Program)
    (unless (linked? Program) (quit "Shader program not linked"))
    
    # delete what's not needed anymore
    
    (glDeleteShader VShader)
    (glDeleteShader FShader)
    
    # configure vertex and element buffers
    
    (glBindVertexArray VAO)
    (glBindBuffer GL_ARRAY_BUFFER VBO)
    (glBufferData GL_ARRAY_BUFFER Vertices GL_STATIC_DRAW)
    (glBindBuffer GL_ELEMENT_ARRAY_BUFFER EBO)
    (glBufferData GL_ELEMENT_ARRAY_BUFFER Elements GL_STATIC_DRAW)
    (glVertexAttribPointer 0 3 GL_FLOAT 0 24 0) # float = 4 bytes
    (glEnableVertexAttribArray 0)
    (glVertexAttribPointer 1 3 GL_FLOAT 0 24 12)
    (glEnableVertexAttribArray 1)
    
    # store the ID of the uniform "transform"
    
    (setq Trans (glGetUniformLocation Program "transform"))
    
    # run the main loop, at every run the function "frame" will be
    # called and the event handling configured under *SDLActions will be
    # checked

    (sdlEventLoop frame)

    # program will end: first clean up everything that has allocated
    # memory with "malloc" manually

    (destroy> W)
    (%@ "free" NIL M)
    
    # clean up everything that sdlInitApp initialized, and quit.
    
    (sdlQuitApp) )

# frame is called from sdlEventLoop until *SDLStop is not NIL

(de frame ()

    (glClearColor 0.2 0.3 0.3 1.0)
    (glClear `GL_COLOR_BUFFER_BIT)        
    (glUseProgram Program)

    (when (>= Angle 360.0) (zero Angle))
    (struct M 'P (cons -1.0 (rotzm (radians Angle))))
    (inc 'Angle 1.0)

    (glUniformMatrix4fv Trans 1 0 M)

    (glBindVertexArray `VAO)
    (glDrawElements `GL_TRIANGLES 6 `GL_UNSIGNED_BYTE 0)

    (refresh> W) )

(def '*SDLActions
    (quote
        (`SDL_KEYDOWN
            (sdlMapKeyboardEvent)
            (case (get *SDLEventInfo 8)
                (41 (on *SDLStop)) ) ) ) )

